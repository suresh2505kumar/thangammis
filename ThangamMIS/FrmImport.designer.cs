﻿namespace ThangamMIS
{
    partial class FrmImport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dtp = new System.Windows.Forms.DateTimePicker();
            this.GBImp = new System.Windows.Forms.GroupBox();
            this.grProgress = new System.Windows.Forms.GroupBox();
            this.progressBar = new CircularProgressBar.CircularProgressBar();
            this.button4 = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.txtloctot = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtservtot = new System.Windows.Forms.TextBox();
            this.btnserv = new System.Windows.Forms.Button();
            this.DGVSer = new System.Windows.Forms.DataGridView();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.DGVLoc = new ThangamMIS.DataGridViewUC();
            this.lblFile = new System.Windows.Forms.Label();
            this.btnExcel = new System.Windows.Forms.Button();
            this.GBEmp = new System.Windows.Forms.GroupBox();
            this.button3 = new System.Windows.Forms.Button();
            this.DGVServI = new System.Windows.Forms.DataGridView();
            this.btnexp = new System.Windows.Forms.Button();
            this.btnimp = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.btnDownload = new System.Windows.Forms.Button();
            this.btnUpdateAmt = new System.Windows.Forms.Button();
            this.GBImp.SuspendLayout();
            this.grProgress.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DGVSer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DGVLoc)).BeginInit();
            this.GBEmp.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DGVServI)).BeginInit();
            this.SuspendLayout();
            // 
            // dtp
            // 
            this.dtp.CustomFormat = "";
            this.dtp.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtp.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtp.Location = new System.Drawing.Point(384, 7);
            this.dtp.Name = "dtp";
            this.dtp.Size = new System.Drawing.Size(129, 26);
            this.dtp.TabIndex = 23;
            this.dtp.ValueChanged += new System.EventHandler(this.dtp_ValueChanged_1);
            // 
            // GBImp
            // 
            this.GBImp.Controls.Add(this.btnUpdateAmt);
            this.GBImp.Controls.Add(this.grProgress);
            this.GBImp.Controls.Add(this.button4);
            this.GBImp.Controls.Add(this.label2);
            this.GBImp.Controls.Add(this.txtloctot);
            this.GBImp.Controls.Add(this.label1);
            this.GBImp.Controls.Add(this.txtservtot);
            this.GBImp.Controls.Add(this.btnserv);
            this.GBImp.Controls.Add(this.DGVSer);
            this.GBImp.Controls.Add(this.button2);
            this.GBImp.Controls.Add(this.button1);
            this.GBImp.Controls.Add(this.DGVLoc);
            this.GBImp.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GBImp.Location = new System.Drawing.Point(2, 38);
            this.GBImp.Name = "GBImp";
            this.GBImp.Size = new System.Drawing.Size(932, 474);
            this.GBImp.TabIndex = 25;
            this.GBImp.TabStop = false;
            this.GBImp.Visible = false;
            // 
            // grProgress
            // 
            this.grProgress.BackColor = System.Drawing.Color.White;
            this.grProgress.Controls.Add(this.progressBar);
            this.grProgress.Location = new System.Drawing.Point(370, 131);
            this.grProgress.Name = "grProgress";
            this.grProgress.Size = new System.Drawing.Size(192, 196);
            this.grProgress.TabIndex = 29;
            this.grProgress.TabStop = false;
            this.grProgress.Visible = false;
            // 
            // progressBar
            // 
            this.progressBar.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.progressBar.AnimationFunction = WinFormAnimation.KnownAnimationFunctions.Liner;
            this.progressBar.AnimationSpeed = 500;
            this.progressBar.BackColor = System.Drawing.Color.White;
            this.progressBar.Font = new System.Drawing.Font("Arial", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.progressBar.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.progressBar.InnerColor = System.Drawing.Color.White;
            this.progressBar.InnerMargin = 5;
            this.progressBar.InnerWidth = 5;
            this.progressBar.Location = new System.Drawing.Point(6, 8);
            this.progressBar.MarqueeAnimationSpeed = 2000;
            this.progressBar.Name = "progressBar";
            this.progressBar.OuterColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(73)))), ((int)(((byte)(94)))));
            this.progressBar.OuterMargin = -11;
            this.progressBar.OuterWidth = 10;
            this.progressBar.ProgressColor = System.Drawing.Color.FromArgb(((int)(((byte)(194)))), ((int)(((byte)(251)))), ((int)(((byte)(50)))));
            this.progressBar.ProgressWidth = 10;
            this.progressBar.SecondaryFont = new System.Drawing.Font("Microsoft Sans Serif", 4.125F);
            this.progressBar.Size = new System.Drawing.Size(180, 180);
            this.progressBar.StartAngle = 270;
            this.progressBar.Style = System.Windows.Forms.ProgressBarStyle.Marquee;
            this.progressBar.SubscriptColor = System.Drawing.Color.Silver;
            this.progressBar.SubscriptMargin = new System.Windows.Forms.Padding(0);
            this.progressBar.SubscriptText = "";
            this.progressBar.SuperscriptColor = System.Drawing.Color.Silver;
            this.progressBar.SuperscriptMargin = new System.Windows.Forms.Padding(0);
            this.progressBar.SuperscriptText = "";
            this.progressBar.TabIndex = 10;
            this.progressBar.Text = "Please Wait";
            this.progressBar.TextMargin = new System.Windows.Forms.Padding(0, 2, 0, 0);
            this.progressBar.Value = 80;
            // 
            // button4
            // 
            this.button4.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button4.Location = new System.Drawing.Point(677, 14);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(128, 34);
            this.button4.TabIndex = 22;
            this.button4.Text = "Process Data";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(698, 426);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(38, 18);
            this.label2.TabIndex = 21;
            this.label2.Text = "Total";
            this.label2.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // txtloctot
            // 
            this.txtloctot.Enabled = false;
            this.txtloctot.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtloctot.Location = new System.Drawing.Point(743, 422);
            this.txtloctot.Name = "txtloctot";
            this.txtloctot.Size = new System.Drawing.Size(85, 26);
            this.txtloctot.TabIndex = 20;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(238, 429);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(38, 18);
            this.label1.TabIndex = 19;
            this.label1.Text = "Total";
            this.label1.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // txtservtot
            // 
            this.txtservtot.Enabled = false;
            this.txtservtot.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtservtot.Location = new System.Drawing.Point(283, 425);
            this.txtservtot.Name = "txtservtot";
            this.txtservtot.Size = new System.Drawing.Size(85, 26);
            this.txtservtot.TabIndex = 18;
            // 
            // btnserv
            // 
            this.btnserv.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnserv.Location = new System.Drawing.Point(163, 12);
            this.btnserv.Name = "btnserv";
            this.btnserv.Size = new System.Drawing.Size(128, 34);
            this.btnserv.TabIndex = 17;
            this.btnserv.Text = "Get Details";
            this.btnserv.UseVisualStyleBackColor = true;
            this.btnserv.Click += new System.EventHandler(this.btnserv_Click_1);
            // 
            // DGVSer
            // 
            this.DGVSer.BackgroundColor = System.Drawing.Color.MistyRose;
            this.DGVSer.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DGVSer.Location = new System.Drawing.Point(13, 50);
            this.DGVSer.Name = "DGVSer";
            this.DGVSer.ReadOnly = true;
            this.DGVSer.RowHeadersVisible = false;
            this.DGVSer.Size = new System.Drawing.Size(436, 364);
            this.DGVSer.TabIndex = 15;
            // 
            // button2
            // 
            this.button2.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.Location = new System.Drawing.Point(840, 419);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 32);
            this.button2.TabIndex = 13;
            this.button2.Text = "Exit";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click_2);
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(471, 13);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(128, 34);
            this.button1.TabIndex = 12;
            this.button1.Text = "Download Data";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // DGVLoc
            // 
            this.DGVLoc.BackgroundColor = System.Drawing.Color.MistyRose;
            this.DGVLoc.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DGVLoc.Location = new System.Drawing.Point(471, 50);
            this.DGVLoc.Name = "DGVLoc";
            this.DGVLoc.RowHeadersVisible = false;
            this.DGVLoc.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DGVLoc.Size = new System.Drawing.Size(444, 365);
            this.DGVLoc.TabIndex = 23;
            // 
            // lblFile
            // 
            this.lblFile.AutoSize = true;
            this.lblFile.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFile.Location = new System.Drawing.Point(588, 528);
            this.lblFile.Name = "lblFile";
            this.lblFile.Size = new System.Drawing.Size(13, 18);
            this.lblFile.TabIndex = 33;
            this.lblFile.Text = "-";
            this.lblFile.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.lblFile.Visible = false;
            // 
            // btnExcel
            // 
            this.btnExcel.BackColor = System.Drawing.Color.LightGray;
            this.btnExcel.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExcel.ForeColor = System.Drawing.Color.Blue;
            this.btnExcel.Location = new System.Drawing.Point(427, 518);
            this.btnExcel.Name = "btnExcel";
            this.btnExcel.Size = new System.Drawing.Size(135, 34);
            this.btnExcel.TabIndex = 33;
            this.btnExcel.Text = "Upload From Excel";
            this.btnExcel.UseVisualStyleBackColor = false;
            this.btnExcel.Visible = false;
            this.btnExcel.Click += new System.EventHandler(this.btnExcel_Click);
            // 
            // GBEmp
            // 
            this.GBEmp.Controls.Add(this.button3);
            this.GBEmp.Controls.Add(this.DGVServI);
            this.GBEmp.Location = new System.Drawing.Point(2, 38);
            this.GBEmp.Name = "GBEmp";
            this.GBEmp.Size = new System.Drawing.Size(932, 474);
            this.GBEmp.TabIndex = 28;
            this.GBEmp.TabStop = false;
            this.GBEmp.Visible = false;
            // 
            // button3
            // 
            this.button3.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button3.Location = new System.Drawing.Point(857, 419);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 32);
            this.button3.TabIndex = 17;
            this.button3.Text = "Exit";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // DGVServI
            // 
            this.DGVServI.BackgroundColor = System.Drawing.Color.MistyRose;
            this.DGVServI.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DGVServI.Location = new System.Drawing.Point(380, 62);
            this.DGVServI.Name = "DGVServI";
            this.DGVServI.ReadOnly = true;
            this.DGVServI.RowHeadersVisible = false;
            this.DGVServI.Size = new System.Drawing.Size(256, 340);
            this.DGVServI.TabIndex = 16;
            // 
            // btnexp
            // 
            this.btnexp.BackColor = System.Drawing.Color.LightGray;
            this.btnexp.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnexp.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnexp.Location = new System.Drawing.Point(750, 5);
            this.btnexp.Name = "btnexp";
            this.btnexp.Size = new System.Drawing.Size(184, 34);
            this.btnexp.TabIndex = 29;
            this.btnexp.Text = "Export Data to Webserver";
            this.btnexp.UseVisualStyleBackColor = false;
            this.btnexp.Click += new System.EventHandler(this.btnexp_Click);
            // 
            // btnimp
            // 
            this.btnimp.BackColor = System.Drawing.Color.LightGray;
            this.btnimp.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnimp.ForeColor = System.Drawing.Color.Blue;
            this.btnimp.Location = new System.Drawing.Point(15, 5);
            this.btnimp.Name = "btnimp";
            this.btnimp.Size = new System.Drawing.Size(104, 34);
            this.btnimp.TabIndex = 30;
            this.btnimp.Text = "Import Local";
            this.btnimp.UseVisualStyleBackColor = false;
            this.btnimp.Visible = false;
            this.btnimp.Click += new System.EventHandler(this.btnimp_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(325, 13);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(37, 18);
            this.label3.TabIndex = 31;
            this.label3.Text = "Date";
            this.label3.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // btnDownload
            // 
            this.btnDownload.BackColor = System.Drawing.Color.LightGray;
            this.btnDownload.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDownload.ForeColor = System.Drawing.Color.Blue;
            this.btnDownload.Location = new System.Drawing.Point(260, 518);
            this.btnDownload.Name = "btnDownload";
            this.btnDownload.Size = new System.Drawing.Size(135, 34);
            this.btnDownload.TabIndex = 32;
            this.btnDownload.Text = "Download CSV File";
            this.btnDownload.UseVisualStyleBackColor = false;
            this.btnDownload.Visible = false;
            this.btnDownload.Click += new System.EventHandler(this.btnDownload_Click);
            // 
            // btnUpdateAmt
            // 
            this.btnUpdateAmt.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUpdateAmt.Location = new System.Drawing.Point(583, 419);
            this.btnUpdateAmt.Name = "btnUpdateAmt";
            this.btnUpdateAmt.Size = new System.Drawing.Size(109, 34);
            this.btnUpdateAmt.TabIndex = 30;
            this.btnUpdateAmt.Text = "Update Amout";
            this.btnUpdateAmt.UseVisualStyleBackColor = true;
            this.btnUpdateAmt.Click += new System.EventHandler(this.btnUpdateAmt_Click);
            // 
            // FrmImport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.ClientSize = new System.Drawing.Size(952, 555);
            this.Controls.Add(this.lblFile);
            this.Controls.Add(this.btnDownload);
            this.Controls.Add(this.btnExcel);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.btnimp);
            this.Controls.Add(this.btnexp);
            this.Controls.Add(this.dtp);
            this.Controls.Add(this.GBImp);
            this.Controls.Add(this.GBEmp);
            this.Name = "FrmImport";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Import and Export";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.GBImp.ResumeLayout(false);
            this.GBImp.PerformLayout();
            this.grProgress.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DGVSer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DGVLoc)).EndInit();
            this.GBEmp.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DGVServI)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.DateTimePicker dtp;
        private System.Windows.Forms.GroupBox GBImp;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtloctot;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtservtot;
        private System.Windows.Forms.Button btnserv;
        private System.Windows.Forms.DataGridView DGVSer;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.GroupBox GBEmp;
        private System.Windows.Forms.DataGridView DGVServI;
        private System.Windows.Forms.Button btnexp;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button btnimp;
        private System.Windows.Forms.Button button4;
        private ThangamMIS.DataGridViewUC DGVLoc;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox grProgress;
        private CircularProgressBar.CircularProgressBar progressBar;
        private System.Windows.Forms.Button btnDownload;
        private System.Windows.Forms.Label lblFile;
        private System.Windows.Forms.Button btnExcel;
        private System.Windows.Forms.Button btnUpdateAmt;
    }
}