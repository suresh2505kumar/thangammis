﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.ComponentModel.DataAnnotations;
using System.Threading;
using System.IO;
using System.Data.OleDb;

namespace ThangamMIS
{
    public partial class FrmImport : Form
    {
        public string _Sourcecon = ConfigurationManager.ConnectionStrings["ConnstrWeb"].ConnectionString;
        public string _Destinationcon = ConfigurationManager.ConnectionStrings["ConnStr"].ConnectionString;
        SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnStr"].ConnectionString);
        SqlConnection connWeb = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnstrWeb"].ConnectionString);
        SqlCommand cmnd = new SqlCommand();
        public bool stats;
        public FrmImport()
        {
            InitializeComponent();
        }
        private void button2_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void loadgrid()
        {
            try
            {
                using (SqlConnection Srccon = new SqlConnection(_Sourcecon))
                {
                    DGVSer.Refresh();
                    DGVSer.AutoGenerateColumns = false;
                    DGVSer.DataSource = null;
                    DGVSer.ColumnCount = 5;
                    DGVSer.Columns[0].Name = "AgentId";
                    DGVSer.Columns[0].Visible = false;

                    DGVSer.Columns[1].Name = "agentname";
                    DGVSer.Columns[1].HeaderText = "AgentName";
                    DGVSer.Columns[1].DataPropertyName = "agentname";
                    DGVSer.Columns[1].Width = 175;

                    DGVSer.Columns[2].Name = "AgentCode";
                    DGVSer.Columns[2].HeaderText = "AgentCode";
                    DGVSer.Columns[2].DataPropertyName = "AgentCode";
                    DGVSer.Columns[2].Width = 95;

                    DGVSer.Columns[3].Name = "amt";
                    DGVSer.Columns[3].HeaderText = "Amount";
                    DGVSer.Columns[3].DataPropertyName = "amt";
                    DGVSer.Columns[3].Width = 85;
                    DGVSer.Columns[4].Name = "Noofrecords";
                    DGVSer.Columns[4].HeaderText = "Records";
                    DGVSer.Columns[4].DataPropertyName = "Noofrecords";
                    DGVSer.Columns[4].Width = 75;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private DataTable GetLocalSever()
        {
            DataTable dt = new DataTable();
            SqlCommand com = new SqlCommand();
            com.CommandType = CommandType.StoredProcedure;
            com.CommandText = "Prc_ExportTOGridLoc";
            com.Connection = conn;
            string year = DateTime.Parse(dtp.Value.ToString()).Year.ToString();
            string month = DateTime.Parse(dtp.Value.ToString()).Month.ToString();
            string date = DateTime.Parse(dtp.Value.ToString()).Day.ToString();
            int day = Convert.ToInt16(date);
            int mon = Convert.ToInt16(month);
            int yr = Convert.ToInt16(year);
            com.Parameters.AddWithValue("@Day", day);
            com.Parameters.AddWithValue("@Mon", mon);
            com.Parameters.AddWithValue("@Yr", yr);
            SqlDataAdapter adpt = new SqlDataAdapter(com);
            adpt.Fill(dt);
            return dt;

        }
        protected void FillLocalGrid(DataTable dt)
        {
            try
            {
                if (dt.Rows.Count > 0)
                {
                    DGVLoc.ColumnCount = 5;

                    DGVLoc.Columns[0].Name = "AgentId";
                    DGVLoc.Columns[0].HeaderText = "AgentId";
                    DGVLoc.Columns[0].DataPropertyName = "AgentId";
                    DGVLoc.Columns[0].Visible = false;

                    DGVLoc.Columns[1].Name = "agentname";
                    DGVLoc.Columns[1].HeaderText = "AgentName";
                    DGVLoc.Columns[1].DataPropertyName = "agentname";
                    DGVLoc.Columns[1].Width = 175;

                    DGVLoc.Columns[2].Name = "AgentCode";
                    DGVLoc.Columns[2].HeaderText = "AgentCode";
                    DGVLoc.Columns[2].DataPropertyName = "AgentCode";
                    DGVLoc.Columns[2].Width = 95;

                    DGVLoc.Columns[3].Name = "Amount";
                    DGVLoc.Columns[3].HeaderText = "Amount";
                    DGVLoc.Columns[3].DataPropertyName = "amt";
                    DGVLoc.Columns[3].Width = 85;

                    DGVLoc.Columns[4].Name = "Noofrecords";
                    DGVLoc.Columns[4].HeaderText = "Records";
                    DGVLoc.Columns[4].DataPropertyName = "Noofrecords";
                    DGVLoc.Columns[4].Width = 75;
                    DGVLoc.DataSource = dt;
                    double sum = 0;
                    for (int i = 0; i < DGVLoc.Rows.Count; ++i)
                    {
                        if (DGVLoc.Rows[i].Cells[3].Value != null)
                        {
                            sum += Convert.ToDouble(DGVLoc.Rows[i].Cells[3].Value.ToString());
                            txtloctot.Text = sum.ToString();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }
        private void loadServgrid()
        {
            DataTable dt = new DataTable();
            DGVServI.Refresh();
            DGVServI.DataSource = null;
            using (SqlConnection con = new SqlConnection(_Sourcecon))
            {
                SqlCommand com = new SqlCommand("Prc_ExportTOGridServ", con);
                string year = DateTime.Parse(dtp.Value.ToString()).Year.ToString();
                string month = DateTime.Parse(dtp.Value.ToString()).Month.ToString();
                string date = DateTime.Parse(dtp.Value.ToString()).Day.ToString();
                int day = Convert.ToInt16(date);
                int mon = Convert.ToInt16(month);
                int yr = Convert.ToInt16(year);
                com.Parameters.AddWithValue("@Day", day);
                com.Parameters.AddWithValue("@Mon", mon);
                com.Parameters.AddWithValue("@Yr", yr);
                com.CommandType = CommandType.StoredProcedure;

                SqlDataAdapter adpt = new SqlDataAdapter(com);
                adpt.Fill(dt);
                try
                {
                    if (dt.Rows.Count > 0)
                    {
                        DGVServI.AutoGenerateColumns = false;
                        DGVServI.ColumnCount = 3;
                        DGVServI.Columns[0].Name = "agentcode";
                        DGVServI.Columns[0].HeaderText = "Agentcode";
                        DGVServI.Columns[0].DataPropertyName = "agentcode";
                        DGVServI.Columns[0].Width = 165;

                        DGVServI.Columns[1].Name = "agentid";
                        DGVServI.Columns[1].HeaderText = "agentid";
                        DGVServI.Columns[1].DataPropertyName = "agentid";
                        DGVServI.Columns[1].Visible = false;

                        DGVServI.Columns[2].Name = "Noofrecords";
                        DGVServI.Columns[2].HeaderText = "Records";
                        DGVServI.Columns[2].DataPropertyName = "Noofrecords";
                        DGVServI.Columns[2].Width = 85;
                        DGVServI.DataSource = dt;
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
            }
        }
        private void Form1_Load(object sender, EventArgs e)
        {
            grProgress.Visible = false;
            progressBar.Visible = false;
            GBImp.Visible = true;
        }
        private void btnserv_Click(object sender, EventArgs e)
        {
        }
        private void dtp_ValueChanged(object sender, EventArgs e)
        {
            DGVLoc.DataSource = null;
            DGVSer.DataSource = null;
            txtloctot.Text = string.Empty;
            txtservtot.Text = string.Empty;
        }
        private bool CheckNet()
        {
            if (System.Net.NetworkInformation.NetworkInterface.GetIsNetworkAvailable() == true)
            {
                stats = true;
            }
            else
            {
                MessageBox.Show("Check The Internet Connection");
                stats = false;
            }
            return stats;
        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void button2_Click_2(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void btnexp_Click(object sender, EventArgs e)
        {
            try
            {
                GetDataInLocalServer();
                //grProgress.Visible = true;
                //progressBar.Visible = true;
                //Thread thread = new Thread(GetDataInLocalServer);
                //thread.Start();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }
        protected DataTable getServerData()
        {
            DataTable dt = new DataTable();
            try
            {
                SqlCommand com = new SqlCommand();
                com.Connection = connWeb;
                com.CommandType = CommandType.StoredProcedure;
                com.CommandText = "Prc_ExportTOGrid";
                string year = DateTime.Parse(dtp.Value.ToString()).Year.ToString();
                string month = DateTime.Parse(dtp.Value.ToString()).Month.ToString();
                string date = DateTime.Parse(dtp.Value.ToString()).Day.ToString();
                int day = Convert.ToInt16(date);
                int mon = Convert.ToInt16(month);
                int yr = Convert.ToInt16(year);
                com.Parameters.AddWithValue("@Day", day);
                com.Parameters.AddWithValue("@Mon", mon);
                com.Parameters.AddWithValue("@Yr", yr);
                com.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(com);
                da.Fill(dt);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            return dt;
        }
        protected void FillWebServerDataGrid(DataTable dt)
        {
            DGVSer.DataSource = null;
            DGVSer.AutoGenerateColumns = false;
            DGVSer.ColumnCount = 5;
            DGVSer.Columns[0].HeaderText = "AgentId";
            DGVSer.Columns[0].DataPropertyName = "AgentId";
            DGVSer.Columns[0].Visible = false;

            DGVSer.Columns[1].Name = "agentname";
            DGVSer.Columns[1].HeaderText = "AgentName";
            DGVSer.Columns[1].DataPropertyName = "agentname";
            DGVSer.Columns[1].Width = 175;

            DGVSer.Columns[2].Name = "AgentCode";
            DGVSer.Columns[2].HeaderText = "AgentCode";
            DGVSer.Columns[2].DataPropertyName = "AgentCode";
            DGVSer.Columns[2].Width = 95;

            DGVSer.Columns[3].Name = "amt";
            DGVSer.Columns[3].HeaderText = "Amount";
            DGVSer.Columns[3].DataPropertyName = "amt";
            DGVSer.Columns[3].Width = 85;

            DGVSer.Columns[4].Name = "Noofrecords";
            DGVSer.Columns[4].HeaderText = "Records";
            DGVSer.Columns[4].DataPropertyName = "Noofrecords";
            DGVSer.Columns[4].Width = 75;
            DGVSer.DataSource = dt;
            double sum = 0;
            for (int i = 0; i < DGVSer.Rows.Count; ++i)
            {
                sum += Convert.ToDouble(DGVSer.Rows[i].Cells[3].Value);
            }
            txtservtot.Text = sum.ToString();
        }
        private void btnserv_Click_1(object sender, EventArgs e)
        {
            DGVSer.DataSource = null;
            DGVLoc.DataSource = null;
            grProgress.Visible = true;
            progressBar.Visible = true;
            Thread thread = new Thread(LoadTable);
            thread.Start();
            Thread threadLocal = new Thread(LoadLocalTable);
            threadLocal.Start();
        }
        private void button1_Click_1(object sender, EventArgs e)
        {
            List<ReqBulkUploadCollection> BulkUploadCollectionList = new List<ReqBulkUploadCollection>();
            using (SqlConnection Srccon = new SqlConnection(_Sourcecon))
            {
                DynamicParameters param = new DynamicParameters();
                string year = DateTime.Parse(dtp.Value.ToString()).Year.ToString();
                string month = DateTime.Parse(dtp.Value.ToString()).Month.ToString();
                string date = DateTime.Parse(dtp.Value.ToString()).Day.ToString();
                int day = Convert.ToInt32(date);
                int Mon = Convert.ToInt32(month);
                int yr = Convert.ToInt32(year);
                param.Add("@Day", day);
                param.Add("@Mon", Mon);
                param.Add("@Yr", yr);
                BulkUploadCollectionList = Srccon.Query<ReqBulkUploadCollection>("Prc_ExportCollectionToLocalDatabaseNew", param, null, true, 0, CommandType.StoredProcedure).ToList();
            }
            ResUploadCollectionDetails objResUploadCollectionDetails = new ResUploadCollectionDetails();
            if (BulkUploadCollectionList.Count > 0)
            {
                using (SqlConnection Descon = new SqlConnection(_Destinationcon))
                {
                    DataTable DtCollection = new DataTable();
                    DtCollection.Columns.Add(new DataColumn { ColumnName = "AgentID", DataType = typeof(Int32) });
                    DtCollection.Columns.Add(new DataColumn { ColumnName = "ColDate", DataType = typeof(DateTime) });
                    DtCollection.Columns.Add(new DataColumn { ColumnName = "CollAmt", DataType = typeof(decimal) });
                    DtCollection.Columns.Add(new DataColumn { ColumnName = "ChitId", DataType = typeof(Int32) });
                    DtCollection.Columns.Add(new DataColumn { ColumnName = "SlNo", DataType = typeof(Int32) });
                    DtCollection.Columns.Add(new DataColumn { ColumnName = "ChitDDid", DataType = typeof(Int32) });
                    DtCollection.Columns.Add(new DataColumn { ColumnName = "CnclTag", DataType = typeof(Int32) });
                    DtCollection.Columns.Add(new DataColumn { ColumnName = "EntryType", DataType = typeof(string) });
                    DtCollection.Columns.Add(new DataColumn { ColumnName = "SyncUID", DataType = typeof(string) });
                    DtCollection.Columns.Add(new DataColumn { ColumnName = "ChitTag", DataType = typeof(string) });
                    DtCollection.Columns.Add(new DataColumn { ColumnName = "NewBalanceDate", DataType = typeof(DateTime) });
                    
                    foreach (ReqBulkUploadCollection ucd in BulkUploadCollectionList)
                    {
                        DataRow row = DtCollection.NewRow();
                        row["AgentID"] = ucd.AgentID;
                        row["ColDate"] = ucd.ColDate;
                        row["ChitId"] = ucd.ChitId;
                        row["SlNo"] = ucd.SlNo;
                        row["CollAmt"] = ucd.CollAmt;
                        row["ChitDDid"] = 0;
                        row["CnclTag"] = ucd.CnclTag;
                        row["EntryType"] = ucd.EntryType;
                        row["SyncUID"] = ucd.SyncUID;
                        row["ChitTag"] = ucd.ChitTag;
                       
                        if (ucd.NewBalanceDate == null)
                        {
                            row["NewBalanceDate"] = DBNull.Value;
                        }
                        else
                        {
                            row["NewBalanceDate"] = ucd.NewBalanceDate;
                        }

                        DtCollection.Rows.Add(row);
                    }

                    using (SqlCommand cmd = new SqlCommand("Prc_Test_Bulk_UploadCollectionDetails"))
                    {
                        DataTable dt = new DataTable();
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Connection = Descon;
                        cmd.Parameters.AddWithValue("@Colland", DtCollection);
                        try
                        {
                            Descon.Open();
                            var dataReader = cmd.ExecuteReader();
                            dt.Load(dataReader);
                            objResUploadCollectionDetails.ResponseCode = Convert.ToInt32(dt.Rows[0]["ResponseCode"]);
                            objResUploadCollectionDetails.ResponseMessage = Convert.ToString(dt.Rows[0]["ResponseMessage"]);
                            MessageBox.Show("Download Sucessfully");
                            DGVSer.DataSource = null;
                            DGVLoc.DataSource = null;
                            grProgress.Visible = true;
                            progressBar.Visible = true;
                            Thread thread = new Thread(LoadTable);
                            thread.Start();
                            Thread threadLocal = new Thread(LoadLocalTable);
                            threadLocal.Start();
                        }
                        catch (Exception)
                        {
                            objResUploadCollectionDetails.ResponseCode = 101;//Convert.ToInt32(dt.Rows[0]["ResponseCode"]);
                            objResUploadCollectionDetails.ResponseMessage = "Upload Fails";//Convert.ToString(dt.Rows[0]["ResponseMessage"]);
                        }
                        finally
                        {
                            Descon.Close();
                            cmd.Dispose();
                        }
                    }
                }
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void dtp_ValueChanged_1(object sender, EventArgs e)
        {
            DGVLoc.DataSource = null;
            DGVSer.DataSource = null;
            DGVServI.DataSource = null;
            txtloctot.Text = string.Empty;
            txtservtot.Text = string.Empty;
        }
        private void btnimp_Click(object sender, EventArgs e)
        {
            GBImp.Visible = true;
            GBEmp.Visible = false;
            btnimp.Visible = false;
            DGVLoc.DataSource = null;
            DGVSer.DataSource = null;
            DGVServI.DataSource = null;
            txtloctot.Text = string.Empty;
            txtservtot.Text = string.Empty;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            try
            {
                button4.Enabled = false;
                Cursor = Cursors.WaitCursor;
                SqlConnection conn = new SqlConnection(_Destinationcon);
                for (int i = 0; i < DGVLoc.SelectedRows.Count; i++)
                {
                    DataGridViewRow row = DGVLoc.SelectedRows[i];
                    conn.Open();
                    string val = row.Cells["AgentId"].Value.ToString();
                    int AgentId = Convert.ToInt32(val);
                    SqlCommand cmd = new SqlCommand();
                    cmd.CommandText = "SP_BalancedteProcess";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = conn;
                    cmd.Parameters.AddWithValue("@Dt", Convert.ToDateTime(dtp.Text).ToString("yyyy-MM-dd"));
                    cmd.Parameters.AddWithValue("@Agentid", AgentId);
                    cmd.CommandTimeout = 360;
                    cmd.ExecuteNonQuery();
                    conn.Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                button4.Enabled = true;
                Cursor = Cursors.Default;
                return;
            }
            conn.Open();
            SqlCommand cmdMonth = new SqlCommand();
            cmdMonth.CommandText = "SP_BalancedteProcessMonthly";
            cmdMonth.CommandType = CommandType.StoredProcedure;
            cmdMonth.Connection = conn;
            cmdMonth.Parameters.AddWithValue("@Dt", Convert.ToDateTime(dtp.Text).ToString("yyyy-MM-dd"));
            cmdMonth.Parameters.AddWithValue("@Agentid", 0);
            cmdMonth.CommandTimeout = 240;
            cmdMonth.ExecuteNonQuery();
            conn.Close();
            button4.Enabled = true;
            Cursor = Cursors.Default;
            MessageBox.Show("Process Completed Succesfully", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
        private void LoadTable()
        {
            DataTable dtWeb = getServerData();
            setDataSource(dtWeb);
        }
        internal delegate void SetDataSourceDelegate(DataTable table);
        private void setDataSource(DataTable table)
        {
            if (this.InvokeRequired)
            {
                this.Invoke(new SetDataSourceDelegate(setDataSource), table);
            }
            else
            {
                FillWebServerDataGrid(table);
                grProgress.Visible = false;
                progressBar.Visible = false;
            }
        }
        private void LoadLocalTable()
        {
            DataTable dtLocal = GetLocalSever();
            SetDataSourceLocalDelegate(dtLocal);
        }
        internal delegate void SetLocalDataSourceDelegate(DataTable table);
        private void SetDataSourceLocalDelegate(DataTable table)
        {
            // Invoke method if required:
            if (this.InvokeRequired)
            {
                this.Invoke(new SetLocalDataSourceDelegate(SetDataSourceLocalDelegate), table);
            }
            else
            {
                FillLocalGrid(table);
            }
        }
        private void DownloadCSV()
        {
            DataTable dtLocal = GetDatatcsv();
            DownloadDataSourceCSV(dtLocal);
        }
        internal delegate void DownloadDataSourceDelegate(DataTable table);
        private void DownloadDataSourceCSV(DataTable table)
        {
            if (this.InvokeRequired)
            {
                this.Invoke(new SetLocalDataSourceDelegate(DownloadDataSourceCSV), table);
            }
            else
            {
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < table.Columns.Count; i++)
                {
                    if (i == table.Columns.Count - 1)
                    {
                        sb.Append(table.Columns[i].ColumnName);
                    }
                    else
                    {
                        sb.Append(table.Columns[i].ColumnName + ',');
                    }
                }
                sb.Append(Environment.NewLine);

                for (int j = 0; j < table.Rows.Count; j++)
                {
                    for (int k = 0; k < table.Columns.Count; k++)
                    {
                        if (k == table.Columns.Count - 1)
                        {
                            sb.Append(table.Rows[j][k].ToString());
                        }
                        else
                        {
                            sb.Append(table.Rows[j][k].ToString() + ',');
                        }
                    }
                    sb.Append(Environment.NewLine);
                }
                FolderBrowserDialog dialog = new FolderBrowserDialog();
                dialog.ShowDialog();
                File.WriteAllText(dialog.SelectedPath + "" + dtp.Text + " Coll.csv", sb.ToString());
                grProgress.Visible = false;
                progressBar.Visible = false;
                MessageBox.Show("Collection Downloaded successfully to this path," + dialog.SelectedPath + "", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
        private void btnDownload_Click(object sender, EventArgs e)
        {
            Thread thre = new Thread(DownloadCSV);
            thre.Start();
            grProgress.Visible = true;
            progressBar.Visible = true;
        }

        protected DataTable GetDatatcsv()
        {
            DataTable dt = new DataTable();
            try
            {
                connWeb.Open();
                DateTime date = Convert.ToDateTime(dtp.Text);
                SqlCommand cmdpro = new SqlCommand();
                cmdpro.CommandType = CommandType.StoredProcedure;
                cmdpro.CommandText = "proc_UpdateCnclTag";
                cmdpro.Parameters.AddWithValue("@date", date.ToString("yyyy-MM-dd"));
                cmdpro.Connection = connWeb;
                SqlDataAdapter da = new SqlDataAdapter(cmdpro);
                da.Fill(dt);
                int count = dt.Rows.Count;
                connWeb.Close();
            }
            catch (Exception)
            {

            }
            return dt;
        }
        private void GetDataInLocalServer()
        {
            DataTable dtLocal = GetLocalExportdata();
            setLocalexport(dtLocal);
        }
        internal delegate void SetLocalexportDataDelegate(DataTable table);
        private void setLocalexport(DataTable table)
        {
            if (this.InvokeRequired)
            {
                this.Invoke(new SetLocalexportDataDelegate(setLocalexport), table);
            }
            else
            {
                BulkUploadtoSeverfromLoacl(table);
                //grProgress.Visible = false;
                //progressBar.Visible = false;
                MessageBox.Show("Data uploaded Successfully", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
        protected DataTable GetLocalExportdata()
        {
            DataTable dt = new DataTable();
            try
            {
                DateTime date = Convert.ToDateTime(dtp.Text.ToString());
                SqlCommand com = new SqlCommand();
                com.CommandType = CommandType.StoredProcedure;
                com.CommandText = "Prc_ExportCollectionToServerDatabase";
                com.Parameters.AddWithValue("@Date", date.ToString("yyyy-MM-dd"));
                com.Connection = conn;
                SqlDataAdapter da = new SqlDataAdapter(com);
                da.Fill(dt);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            return dt;
        }
        protected void BulkUploadtoSeverfromLoacl(DataTable dt)
        {
            try
            {
                connWeb.Open();
                DateTime date = Convert.ToDateTime(dtp.Text.ToString());
                SqlCommand com = new SqlCommand();
                com.CommandType = CommandType.StoredProcedure;
                com.CommandText = "SP_DeletegentUpldData";
                com.Parameters.AddWithValue("@Date", date.ToString("yyyy-MM-dd"));
                com.Connection = connWeb;
                com.ExecuteNonQuery();
                connWeb.Close();
                using (SqlBulkCopy copy = new SqlBulkCopy(connWeb))
                {
                    connWeb.Open();
                    copy.DestinationTableName = "Agent_upld";
                    copy.ColumnMappings.Add("DT", "DT");
                    copy.ColumnMappings.Add("AgentId", "AgentId");
                    copy.ColumnMappings.Add("AgentCode", "AgentCode");
                    copy.ColumnMappings.Add("AgentName", "AgentName");
                    copy.ColumnMappings.Add("CustId", "CustId");
                    copy.ColumnMappings.Add("CustCode", "CustCode");
                    copy.ColumnMappings.Add("CustName", "CustName");
                    copy.ColumnMappings.Add("ChitID", "ChitID");
                    copy.ColumnMappings.Add("ChitSName", "ChitSName");
                    copy.ColumnMappings.Add("ChitName", "ChitName");
                    copy.ColumnMappings.Add("SlNo", "SlNo");
                    copy.ColumnMappings.Add("BarCode", "Barcode");
                    copy.ColumnMappings.Add("ChitAmt", "ChitAmt");
                    copy.ColumnMappings.Add("AmtPChit", "AmtPChit");
                    copy.ColumnMappings.Add("BalDate", "BalDate");
                    copy.ColumnMappings.Add("DueAmt", "DueAmt");
                    copy.ColumnMappings.Add("TenureType", "TenureType");
                    copy.ColumnMappings.Add("ChitDid", "ChitDid");
                    copy.ColumnMappings.Add("StartDt", "StartDt");
                    copy.ColumnMappings.Add("FatherName", "Fathername");
                    copy.ColumnMappings.Add("Street", "Street");
                    copy.ColumnMappings.Add("City", "City");
                    copy.ColumnMappings.Add("ComDt", "ComDt");
                    copy.ColumnMappings.Add("BalDue", "BalDue");
                    copy.ColumnMappings.Add("ChitTag", "ChitTag");
                    copy.WriteToServer(dt);
                    connWeb.Close();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void btnExcel_Click(object sender, EventArgs e)
        {
            string filePath = string.Empty;
            string fileExt = string.Empty;
            OpenFileDialog file = new OpenFileDialog(); //open dialog to choose file  
            if (file.ShowDialog() == DialogResult.OK)
            {
                filePath = file.FileName;
                lblFile.Text = filePath;
                fileExt = Path.GetExtension(filePath);
                if (fileExt.CompareTo(".xls") == 0 || fileExt.CompareTo(".xlsx") == 0)
                {
                    try
                    {
                        string excelpath = lblFile.Text;
                        string connstring = string.Empty;
                        string extensionpath = Path.GetExtension(file.FileName);
                        switch (extensionpath)
                        {
                            case ".xls":
                                connstring = ConfigurationManager.ConnectionStrings["Excel03ConString"].ConnectionString;
                                break;
                            case ".xlsx":
                                //connstring = ConfigurationManager.ConnectionStrings["Excel07+ConString"].ConnectionString;                              
                                return;
                        }

                        connstring = string.Format(connstring, excelpath);
                        using (OleDbConnection excel_Conn = new OleDbConnection(connstring))
                        {
                            excel_Conn.Open();
                            string sheet1 = excel_Conn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null).Rows[0]["TABLE_NAME"].ToString();
                            DataTable dtexcelData = new DataTable();
                            new DataColumn("Dt", typeof(DateTime));
                            new DataColumn("AgentId", typeof(int));
                            new DataColumn("AgentCode", typeof(string));
                            new DataColumn("AgentName", typeof(string));
                            new DataColumn("CustId", typeof(int));
                            new DataColumn("CustCode", typeof(string));
                            new DataColumn("CustName", typeof(string));
                            new DataColumn("ChitID", typeof(int));
                            new DataColumn("ChitSName", typeof(string));
                            new DataColumn("ChitName", typeof(string));
                            new DataColumn("SlNo", typeof(int));
                            new DataColumn("BarCode", typeof(string));
                            new DataColumn("ChitAmt", typeof(decimal));
                            new DataColumn("AmtPChit", typeof(decimal));
                            new DataColumn("BalDate", typeof(DateTime));
                            new DataColumn("DueAmt", typeof(decimal));
                            new DataColumn("TenureType", typeof(string));
                            new DataColumn("ChitDid", typeof(int));
                            new DataColumn("StartDt", typeof(DateTime));
                            new DataColumn("Fathername", typeof(string));
                            new DataColumn("Street", typeof(string));
                            new DataColumn("City", typeof(string));
                            new DataColumn("ComDt", typeof(DateTime));
                            new DataColumn("BalDue", typeof(int));
                            new DataColumn("ChitTag", typeof(string));

                            using (OleDbDataAdapter oda = new OleDbDataAdapter("SELECT * FROM [" + sheet1 + "]", excel_Conn))
                            {
                                oda.Fill(dtexcelData);
                            }

                            excel_Conn.Close();
                            string connString = ConfigurationManager.ConnectionStrings["ConnstrWeb"].ConnectionString;
                            using (SqlConnection conn = new SqlConnection(connString))
                            {
                                TimeZoneInfo INDIAN_ZONE = TimeZoneInfo.FindSystemTimeZoneById("India Standard Time");
                                DateTime indianTime = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, INDIAN_ZONE);
                                string query = "Delete from Agent_Upld where DT='" + indianTime.ToString("yyyy-MM-dd") + "'";
                                conn.Open();
                                SqlCommand cmd = new SqlCommand(query, conn);
                                cmd.ExecuteNonQuery();
                                cmd.Dispose();
                                conn.Close();
                                using (SqlBulkCopy sqlbulkcopy = new SqlBulkCopy(conn))
                                {
                                    sqlbulkcopy.DestinationTableName = "dbo.Agent_upld";
                                    sqlbulkcopy.ColumnMappings.Add("Dt", "DT");
                                    sqlbulkcopy.ColumnMappings.Add("AgentId", "AgentId");
                                    sqlbulkcopy.ColumnMappings.Add("AgentCode", "AgentCode");
                                    sqlbulkcopy.ColumnMappings.Add("AgentName", "AgentName");
                                    sqlbulkcopy.ColumnMappings.Add("CustId", "CustId");
                                    sqlbulkcopy.ColumnMappings.Add("CustCode", "CustCode");
                                    sqlbulkcopy.ColumnMappings.Add("CustName", "CustName");
                                    sqlbulkcopy.ColumnMappings.Add("ChitID", "ChitID");
                                    sqlbulkcopy.ColumnMappings.Add("ChitSName", "ChitSName");
                                    sqlbulkcopy.ColumnMappings.Add("ChitName", "ChitName");
                                    sqlbulkcopy.ColumnMappings.Add("SINo", "SlNo");
                                    sqlbulkcopy.ColumnMappings.Add("BarCode", "Barcode");
                                    sqlbulkcopy.ColumnMappings.Add("ChitAmt", "ChitAmt");
                                    sqlbulkcopy.ColumnMappings.Add("AmtPChit", "AmtPChit");
                                    sqlbulkcopy.ColumnMappings.Add("BalDate", "BalDate");
                                    sqlbulkcopy.ColumnMappings.Add("DueAmt", "DueAmt");
                                    sqlbulkcopy.ColumnMappings.Add("TenureType", "TenureType");
                                    sqlbulkcopy.ColumnMappings.Add("ChitDid", "ChitDid");
                                    sqlbulkcopy.ColumnMappings.Add("StartDt", "StartDt");
                                    sqlbulkcopy.ColumnMappings.Add("FatherName", "Fathername");
                                    sqlbulkcopy.ColumnMappings.Add("Street", "Street");
                                    sqlbulkcopy.ColumnMappings.Add("City", "City");
                                    sqlbulkcopy.ColumnMappings.Add("ComDt", "ComDt");
                                    sqlbulkcopy.ColumnMappings.Add("BalDue", "BalDue");
                                    sqlbulkcopy.ColumnMappings.Add("ChitTag", "ChitTag");
                                    conn.Open();
                                    sqlbulkcopy.BulkCopyTimeout = 600;
                                    sqlbulkcopy.WriteToServer(dtexcelData);
                                    conn.Close();
                                    MessageBox.Show("Uploaded Successfully", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information); ;
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message.ToString());
                    }
                }
                else
                {
                    MessageBox.Show("Please choose .xls or .xlsx file only.", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void btnUpdateAmt_Click(object sender, EventArgs e)
        {
            SQLDBHelper db = new SQLDBHelper();
            string Quer = @"
                            select CAST(coldate as date) as dt, chitid, slno, chitddid
                            from coll_and
                            where CHITDDID > 0 and MONTH(ColDate) = 12 group by chitid, CAST(coldate as date), chitid, slno, chitddid
                            having COUNT(chitid) > 1";
            DataSet ds = db.GetDatasWithPara(CommandType.Text, Quer);
            DataTable dt = ds.Tables[0];
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                int ChitDDid =Convert.ToInt32(dt.Rows[i]["Chitddid"].ToString());
                SqlParameter[] para = { new SqlParameter("@ChitDDid", ChitDDid) };
                db.ExecuteQuery(CommandType.StoredProcedure, "SP_TempData", para);
            }
            MessageBox.Show("Update sucessfully");
        }
    }
    public class ReqBulkUploadCollection
    {
        public int AgentID { get; set; }
        public DateTime CollectionDate { get; set; }
        public string ApiAccessToken { get; set; }
        public int ChitId { get; set; }
        public decimal CollectionAmount { get; set; }
        public int SlNo { get; set; }
        public int ChitDID { get; set; }
        public int CnclTag { get; set; }
        public string EntryType { get; set; }
        public string SyncUID { get; set; }
        public string ChitTag { get; set; }
        public DateTime ColDate { get; set; }
        public decimal CollAmt { get; set; }
       
        public DateTime? NewBalanceDate { get; set; }
        [DisplayFormat(DataFormatString = "yyyy-MM-dd hh:mm:ss")]
        public Nullable<System.DateTime> dtps { get; set; }
    }
    public class ReaBulkColleServer
    {
        public Nullable<System.DateTime> DT { get; set; }
        public int AgentID { get; set; }
        public string AgentCode { get; set; }
        public string AgentName { get; set; }
        public int Custid { get; set; }
        public string CustCode { get; set; }
        public string CustName { get; set; }
        public int ChitId { get; set; }
        public string ChitSName { get; set; }
        public string ChitName { get; set; }
        public int SlNo { get; set; }
        public int ChitDid { get; set; }
        public string Barcode { get; set; }
        public decimal ChitAmt { get; set; }
        public decimal AmtPChit { get; set; }
        public Nullable<System.DateTime> BalDate { get; set; }
        public decimal DueAmt { get; set; }
        public string TenureType { get; set; }
        public DateTime? StartDt { get; set; }
        public string Fathername { get; set; }
        public string Street { get; set; }
        public string City { get; set; }
        public DateTime? ComDt { get; set; }
        public decimal Baldue { get; set; }
        public string ChitTag { get; set; }
        public string reason { get; set; }
        public string errortype { get; set; }
        public string verifytype { get; set; }
        public int SyncUID { get; set; }
        public Nullable<System.DateTime> Verifydate { get; set; }
        public string screen { get; set; }
    }
    public class ReqBulkUploadProcColl
    {
        public int ChitDID { get; set; }
        public Nullable<System.DateTime> CollDate { get; set; }
        public Nullable<System.DateTime> NewBalanceDate { get; set; }
        public decimal COLLAMT { get; set; }
        public string CHITTAG { get; set; }
        public Nullable<System.DateTime> Tstamp { get; set; }
        public int AgentID { get; set; }
    }
    public class ReaBulkColleServerDetails
    {
        public List<ReaBulkColleServer> BulkUploadCollectionDetailsServ { get; set; }
    }
    public class ExportToGrid
    {
        public int AgentID { get; set; }
        public string agentname { get; set; }
        public string amt { get; set; }
        public decimal Noofrecords { get; set; }
    }
    public class ReqBulkUploadCollectionDetails
    {
        public List<ReqBulkUploadCollection> BulkUploadCollectionDetails { get; set; }
    }
    public class ResUploadCollectionDetails
    {
        public int ResponseCode { get; set; }
        public string ResponseMessage { get; set; }
        public string dtp { get; set; }
    }
    public class ResUploadCollectionDetailsServ
    {
        public int ResponseCode { get; set; }
        public string ResponseMessage { get; set; }
        public string dtp { get; set; }
    }
    public class ResUploadCollectionDetailsProc
    {
        public int ResponseCode { get; set; }
        public string ResponseMessage { get; set; }
        public string dtp { get; set; }
    }
}
