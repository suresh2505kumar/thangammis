﻿namespace ThangamMIS
{
    partial class FrmCollectionView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.grInfo = new System.Windows.Forms.GroupBox();
            this.chckCanceled = new System.Windows.Forms.CheckBox();
            this.grProgress = new System.Windows.Forms.GroupBox();
            this.progressBar = new CircularProgressBar.CircularProgressBar();
            this.grAgentSearch = new System.Windows.Forms.GroupBox();
            this.txtAgentSearch = new System.Windows.Forms.TextBox();
            this.DataGridAgentSearch = new System.Windows.Forms.DataGridView();
            this.btnShowDetails = new System.Windows.Forms.Button();
            this.txtAgentName = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtAgent = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.DtpCollDate = new System.Windows.Forms.DateTimePicker();
            this.DataGridCollection = new System.Windows.Forms.DataGridView();
            this.label4 = new System.Windows.Forms.Label();
            this.txtToalamount = new System.Windows.Forms.TextBox();
            this.txtRecords = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.grInfo.SuspendLayout();
            this.grProgress.SuspendLayout();
            this.grAgentSearch.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridAgentSearch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridCollection)).BeginInit();
            this.SuspendLayout();
            // 
            // grInfo
            // 
            this.grInfo.Controls.Add(this.chckCanceled);
            this.grInfo.Controls.Add(this.grProgress);
            this.grInfo.Controls.Add(this.grAgentSearch);
            this.grInfo.Controls.Add(this.btnShowDetails);
            this.grInfo.Controls.Add(this.txtAgentName);
            this.grInfo.Controls.Add(this.label3);
            this.grInfo.Controls.Add(this.txtAgent);
            this.grInfo.Controls.Add(this.label2);
            this.grInfo.Controls.Add(this.label1);
            this.grInfo.Controls.Add(this.DtpCollDate);
            this.grInfo.Controls.Add(this.DataGridCollection);
            this.grInfo.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grInfo.Location = new System.Drawing.Point(12, 12);
            this.grInfo.Name = "grInfo";
            this.grInfo.Size = new System.Drawing.Size(605, 484);
            this.grInfo.TabIndex = 0;
            this.grInfo.TabStop = false;
            // 
            // chckCanceled
            // 
            this.chckCanceled.AutoSize = true;
            this.chckCanceled.Location = new System.Drawing.Point(507, 20);
            this.chckCanceled.Name = "chckCanceled";
            this.chckCanceled.Size = new System.Drawing.Size(88, 22);
            this.chckCanceled.TabIndex = 31;
            this.chckCanceled.Text = "Cancelled";
            this.chckCanceled.UseVisualStyleBackColor = true;
            this.chckCanceled.CheckedChanged += new System.EventHandler(this.chckCanceled_CheckedChanged);
            // 
            // grProgress
            // 
            this.grProgress.BackColor = System.Drawing.Color.White;
            this.grProgress.Controls.Add(this.progressBar);
            this.grProgress.Location = new System.Drawing.Point(199, 176);
            this.grProgress.Name = "grProgress";
            this.grProgress.Size = new System.Drawing.Size(191, 198);
            this.grProgress.TabIndex = 30;
            this.grProgress.TabStop = false;
            this.grProgress.Visible = false;
            // 
            // progressBar
            // 
            this.progressBar.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.progressBar.AnimationFunction = WinFormAnimation.KnownAnimationFunctions.Liner;
            this.progressBar.AnimationSpeed = 500;
            this.progressBar.BackColor = System.Drawing.Color.White;
            this.progressBar.Font = new System.Drawing.Font("Arial", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.progressBar.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.progressBar.InnerColor = System.Drawing.Color.White;
            this.progressBar.InnerMargin = 5;
            this.progressBar.InnerWidth = 5;
            this.progressBar.Location = new System.Drawing.Point(5, 10);
            this.progressBar.MarqueeAnimationSpeed = 2000;
            this.progressBar.Name = "progressBar";
            this.progressBar.OuterColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(73)))), ((int)(((byte)(94)))));
            this.progressBar.OuterMargin = -11;
            this.progressBar.OuterWidth = 10;
            this.progressBar.ProgressColor = System.Drawing.Color.FromArgb(((int)(((byte)(194)))), ((int)(((byte)(251)))), ((int)(((byte)(50)))));
            this.progressBar.ProgressWidth = 10;
            this.progressBar.SecondaryFont = new System.Drawing.Font("Microsoft Sans Serif", 4.125F);
            this.progressBar.Size = new System.Drawing.Size(180, 180);
            this.progressBar.StartAngle = 270;
            this.progressBar.Style = System.Windows.Forms.ProgressBarStyle.Marquee;
            this.progressBar.SubscriptColor = System.Drawing.Color.Silver;
            this.progressBar.SubscriptMargin = new System.Windows.Forms.Padding(0);
            this.progressBar.SubscriptText = "";
            this.progressBar.SuperscriptColor = System.Drawing.Color.Silver;
            this.progressBar.SuperscriptMargin = new System.Windows.Forms.Padding(0);
            this.progressBar.SuperscriptText = "";
            this.progressBar.TabIndex = 10;
            this.progressBar.Text = "Please Wait";
            this.progressBar.TextMargin = new System.Windows.Forms.Padding(0, 2, 0, 0);
            this.progressBar.Value = 80;
            // 
            // grAgentSearch
            // 
            this.grAgentSearch.Controls.Add(this.txtAgentSearch);
            this.grAgentSearch.Controls.Add(this.DataGridAgentSearch);
            this.grAgentSearch.Location = new System.Drawing.Point(244, 89);
            this.grAgentSearch.Name = "grAgentSearch";
            this.grAgentSearch.Size = new System.Drawing.Size(331, 269);
            this.grAgentSearch.TabIndex = 18;
            this.grAgentSearch.TabStop = false;
            this.grAgentSearch.Text = "Search";
            // 
            // txtAgentSearch
            // 
            this.txtAgentSearch.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAgentSearch.Location = new System.Drawing.Point(5, 21);
            this.txtAgentSearch.Name = "txtAgentSearch";
            this.txtAgentSearch.Size = new System.Drawing.Size(312, 26);
            this.txtAgentSearch.TabIndex = 18;
            this.txtAgentSearch.TextChanged += new System.EventHandler(this.txtAgentSearch_TextChanged);
            this.txtAgentSearch.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtAgentSearch_KeyDown);
            this.txtAgentSearch.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtAgentSearch_KeyPress);
            // 
            // DataGridAgentSearch
            // 
            this.DataGridAgentSearch.AllowUserToAddRows = false;
            this.DataGridAgentSearch.BackgroundColor = System.Drawing.SystemColors.ControlDark;
            this.DataGridAgentSearch.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridAgentSearch.Location = new System.Drawing.Point(5, 48);
            this.DataGridAgentSearch.Name = "DataGridAgentSearch";
            this.DataGridAgentSearch.ReadOnly = true;
            this.DataGridAgentSearch.RowHeadersVisible = false;
            this.DataGridAgentSearch.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DataGridAgentSearch.Size = new System.Drawing.Size(312, 211);
            this.DataGridAgentSearch.TabIndex = 0;
            this.DataGridAgentSearch.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.DataGridAgentSearch_MouseDoubleClick);
            // 
            // btnShowDetails
            // 
            this.btnShowDetails.Location = new System.Drawing.Point(381, 18);
            this.btnShowDetails.Name = "btnShowDetails";
            this.btnShowDetails.Size = new System.Drawing.Size(120, 27);
            this.btnShowDetails.TabIndex = 6;
            this.btnShowDetails.Text = "Show Details";
            this.btnShowDetails.UseVisualStyleBackColor = true;
            this.btnShowDetails.Click += new System.EventHandler(this.btnShowDetails_Click);
            // 
            // txtAgentName
            // 
            this.txtAgentName.Location = new System.Drawing.Point(363, 67);
            this.txtAgentName.Name = "txtAgentName";
            this.txtAgentName.Size = new System.Drawing.Size(212, 26);
            this.txtAgentName.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(261, 71);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(85, 18);
            this.label3.TabIndex = 4;
            this.label3.Text = "Agent Name";
            // 
            // txtAgent
            // 
            this.txtAgent.Location = new System.Drawing.Point(95, 67);
            this.txtAgent.Name = "txtAgent";
            this.txtAgent.Size = new System.Drawing.Size(151, 26);
            this.txtAgent.TabIndex = 3;
            this.txtAgent.MouseClick += new System.Windows.Forms.MouseEventHandler(this.txtAgent_MouseClick);
            this.txtAgent.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtAgentSearch_KeyDown);
            this.txtAgent.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtAgentSearch_KeyPress);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(31, 71);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(45, 18);
            this.label2.TabIndex = 2;
            this.label2.Text = "Agent";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(31, 29);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(37, 18);
            this.label1.TabIndex = 1;
            this.label1.Text = "Date";
            // 
            // DtpCollDate
            // 
            this.DtpCollDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.DtpCollDate.Location = new System.Drawing.Point(95, 25);
            this.DtpCollDate.Name = "DtpCollDate";
            this.DtpCollDate.Size = new System.Drawing.Size(107, 26);
            this.DtpCollDate.TabIndex = 0;
            // 
            // DataGridCollection
            // 
            this.DataGridCollection.AllowUserToAddRows = false;
            this.DataGridCollection.BackgroundColor = System.Drawing.Color.White;
            this.DataGridCollection.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridCollection.Location = new System.Drawing.Point(0, 106);
            this.DataGridCollection.Name = "DataGridCollection";
            this.DataGridCollection.ReadOnly = true;
            this.DataGridCollection.RowHeadersVisible = false;
            this.DataGridCollection.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DataGridCollection.Size = new System.Drawing.Size(605, 370);
            this.DataGridCollection.TabIndex = 19;
            this.DataGridCollection.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.DataGridCollection_CellFormatting);
            this.DataGridCollection.DataBindingComplete += new System.Windows.Forms.DataGridViewBindingCompleteEventHandler(this.DataGridCollection_DataBindingComplete);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(223, 503);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(91, 18);
            this.label4.TabIndex = 1;
            this.label4.Text = "Total Amount";
            // 
            // txtToalamount
            // 
            this.txtToalamount.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtToalamount.Location = new System.Drawing.Point(315, 499);
            this.txtToalamount.Name = "txtToalamount";
            this.txtToalamount.Size = new System.Drawing.Size(81, 26);
            this.txtToalamount.TabIndex = 2;
            this.txtToalamount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtRecords
            // 
            this.txtRecords.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRecords.Location = new System.Drawing.Point(536, 499);
            this.txtRecords.Name = "txtRecords";
            this.txtRecords.Size = new System.Drawing.Size(81, 26);
            this.txtRecords.TabIndex = 4;
            this.txtRecords.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(440, 503);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(91, 18);
            this.label5.TabIndex = 3;
            this.label5.Text = "No of records";
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(12, 498);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(160, 27);
            this.button1.TabIndex = 7;
            this.button1.Text = "Balance Date Reconcile";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // FrmCollectionView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(629, 529);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.txtRecords);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtToalamount);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.grInfo);
            this.MaximizeBox = false;
            this.Name = "FrmCollectionView";
            this.Text = "Collection View";
            this.Load += new System.EventHandler(this.FrmCollectionView_Load);
            this.grInfo.ResumeLayout(false);
            this.grInfo.PerformLayout();
            this.grProgress.ResumeLayout(false);
            this.grAgentSearch.ResumeLayout(false);
            this.grAgentSearch.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridAgentSearch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridCollection)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox grInfo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker DtpCollDate;
        private System.Windows.Forms.TextBox txtAgentName;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtAgent;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnShowDetails;
        private System.Windows.Forms.GroupBox grAgentSearch;
        private System.Windows.Forms.TextBox txtAgentSearch;
        private System.Windows.Forms.DataGridView DataGridAgentSearch;
        private System.Windows.Forms.DataGridView DataGridCollection;
        private System.Windows.Forms.GroupBox grProgress;
        private CircularProgressBar.CircularProgressBar progressBar;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtToalamount;
        private System.Windows.Forms.TextBox txtRecords;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.CheckBox chckCanceled;
        private System.Windows.Forms.Button button1;
    }
}