﻿using System;
using System.ComponentModel;
using System.Data;
using System.Windows.Forms;
using System.Threading;
using System.Data.SqlClient;
using System.Configuration;

namespace ThangamMIS
{
    public partial class FrmCollectionView : Form
    {
        public FrmCollectionView()
        {
            InitializeComponent();
        }

        private void BackgroundWorkerReconcile_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {

            }
            catch (Exception)
            {

                throw;
            }
        }

        internal delegate void SetDataSourceDelegate(DataTable table);
        SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnStr"].ConnectionString);
        SqlCommand cmd = new SqlCommand();
        BindingSource bs = new BindingSource();
        private void txtAgent_MouseClick(object sender, MouseEventArgs e)
        {
            grAgentSearch.Visible = true;
            LoadChit();
            txtAgentSearch.Focus();
        }

        private void FrmCollectionView_Load(object sender, EventArgs e)
        {
            grAgentSearch.Visible = false;
        }
        protected void LoadChit()
        {
            try
            {
                conn.Open();
                cmd = new SqlCommand();
                cmd.Connection = conn;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "SP_GetAgent";
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                bs.DataSource = dt;
                DataGridAgentSearch.DataSource = null;
                DataGridAgentSearch.AutoGenerateColumns = false;
                DataGridAgentSearch.ColumnCount = 3;
                DataGridAgentSearch.Columns[0].Name = "AgentId";
                DataGridAgentSearch.Columns[0].HeaderText = "AgentId";
                DataGridAgentSearch.Columns[0].DataPropertyName = "AgentId";
                DataGridAgentSearch.Columns[0].Visible = false;

                DataGridAgentSearch.Columns[1].Name = "AgentCode";
                DataGridAgentSearch.Columns[1].HeaderText = "AgentCode";
                DataGridAgentSearch.Columns[1].DataPropertyName = "AgentCode";

                DataGridAgentSearch.Columns[2].Name = "AgentName";
                DataGridAgentSearch.Columns[2].HeaderText = "AgentName";
                DataGridAgentSearch.Columns[2].DataPropertyName = "AgentName";
                DataGridAgentSearch.Columns[2].Width = 200;
                DataGridAgentSearch.DataSource = bs;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            finally
            {
                conn.Close();
                cmd.Dispose();
            }
        }

        private void txtAgentSearch_TextChanged(object sender, EventArgs e)
        {
            bs.Filter = string.Format("AgentName LIKE '%{0}%' OR AgentCode LIKE '%{1}%'", txtAgentSearch.Text, txtAgentSearch.Text);
        }

        private void txtAgentSearch_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Escape)
            {
                grAgentSearch.Visible = false;
                txtAgentSearch.Text = string.Empty;
            }
        }

        private void txtAgentSearch_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                grAgentSearch.Visible = false;
                txtAgentSearch.Text = string.Empty;
            }

        }

        private void DataGridAgentSearch_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            int Index = DataGridAgentSearch.SelectedCells[0].RowIndex;
            txtAgentName.Text = DataGridAgentSearch.Rows[Index].Cells[2].Value.ToString();
            txtAgent.Text = DataGridAgentSearch.Rows[Index].Cells[1].Value.ToString();
            txtAgent.Tag = DataGridAgentSearch.Rows[Index].Cells[0].Value;
            grAgentSearch.Visible = false;
            txtAgentSearch.Text = string.Empty;
        }

        private void btnShowDetails_Click(object sender, EventArgs e)
        {
            if(txtAgent.Text == string.Empty || txtAgentName.Text == string.Empty)
            {
                MessageBox.Show("Select Agent", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtAgent.Focus();
                return;
            }
            else
            {
                grProgress.Visible = true;
                progressBar.Visible = true;
                Thread thread = new Thread(LoadTable);
                thread.Start();
            }
            
        }
        protected void LoadTable()
        {
            DataTable dt = GetDatas();
            setDataSource(dt);

        }
        private void setDataSource(DataTable table)
        {
            if (this.InvokeRequired)
            {
                this.Invoke(new SetDataSourceDelegate(setDataSource), table);
            }
            else
            {
                FillDataGrid(table);
                grProgress.Visible = false;
                progressBar.Visible = false;
            }
        }

        private void FillDataGrid(DataTable table)
        {
            try
            {
                if(table.Rows.Count != 0)
                {
                    DataGridCollection.DataSource = null;
                    DataGridCollection.AutoGenerateColumns = false;
                    DataGridCollection.ColumnCount = 6;
                    DataGridCollection.Columns[0].Name = "ChitSName";
                    DataGridCollection.Columns[0].HeaderText = "ChitSName";
                    DataGridCollection.Columns[0].DataPropertyName = "CHITSNAME";

                    DataGridCollection.Columns[1].Name = "CustName";
                    DataGridCollection.Columns[1].HeaderText = "CustName";
                    DataGridCollection.Columns[1].DataPropertyName = "CustName";
                    DataGridCollection.Columns[1].Width = 150;
                    DataGridCollection.Columns[2].Name = "SlNo";
                    DataGridCollection.Columns[2].HeaderText = "SlNo";
                    DataGridCollection.Columns[2].DataPropertyName = "SlNo";
                    DataGridCollection.Columns[2].Width = 50;
                    DataGridCollection.Columns[3].Name = "CollAmt";
                    DataGridCollection.Columns[3].HeaderText = "CollAmt";
                    DataGridCollection.Columns[3].DataPropertyName = "CollAmt";
                    DataGridCollection.Columns[3].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                    DataGridCollection.Columns[3].Width = 80;
                    DataGridCollection.Columns[4].Name = "Status";
                    DataGridCollection.Columns[4].HeaderText = "Status";
                    DataGridCollection.Columns[4].DataPropertyName = "Pr";
                    DataGridCollection.Columns[5].Name = "Chit Status";
                    DataGridCollection.Columns[5].HeaderText = "Chit Status";
                    DataGridCollection.Columns[5].DataPropertyName = "CHITTAG";
                    DataGridCollection.DataSource = table;
                    txtToalamount.Text = table.Compute("Sum(CollAmt)", "").ToString();
                    txtRecords.Text = table.Rows.Count.ToString();
                }
                else
                {
                    MessageBox.Show("No Data Found for this date", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    DataGridCollection.DataSource = null;
                    DataGridCollection.AutoGenerateColumns = false;
                    DataGridCollection.ColumnCount = 6;
                    DataGridCollection.Columns[0].Name = "ChitSName";
                    DataGridCollection.Columns[0].HeaderText = "ChitSName";
                    DataGridCollection.Columns[0].DataPropertyName = "CHITSNAME";

                    DataGridCollection.Columns[1].Name = "CustName";
                    DataGridCollection.Columns[1].HeaderText = "CustName";
                    DataGridCollection.Columns[1].DataPropertyName = "CustName";
                    DataGridCollection.Columns[1].Width = 150;
                    DataGridCollection.Columns[2].Name = "SlNo";
                    DataGridCollection.Columns[2].HeaderText = "SlNo";
                    DataGridCollection.Columns[2].DataPropertyName = "SlNo";
                    DataGridCollection.Columns[2].Width = 50;
                    DataGridCollection.Columns[3].Name = "CollAmt";
                    DataGridCollection.Columns[3].HeaderText = "CollAmt";
                    DataGridCollection.Columns[3].DataPropertyName = "CollAmt";
                    DataGridCollection.Columns[3].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                    DataGridCollection.Columns[3].Width = 80;
                    DataGridCollection.Columns[4].Name = "Status";
                    DataGridCollection.Columns[4].HeaderText = "Status";
                    DataGridCollection.Columns[4].DataPropertyName = "Pr";
                    DataGridCollection.Columns[5].Name = "Chit Status";
                    DataGridCollection.Columns[5].HeaderText = "Chit Status";
                    DataGridCollection.Columns[5].DataPropertyName = "CHITTAG";
                    DataGridCollection.DataSource = table;
                    txtToalamount.Text = "0";
                    txtRecords.Text = "0";
                }
                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        protected DataTable GetDatas()
        {
            DataTable dt = new DataTable();
            try
            {
                DateTime dttime = Convert.ToDateTime(DtpCollDate.Text);               
                int AgentId = (int)txtAgent.Tag;
                int CancelTag = 0;
                if(chckCanceled.Checked == false)
                {
                    CancelTag = 0;
                }
                else
                {
                    CancelTag = 1;
                }
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = "SP_GetDetailsforView";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = conn;
                cmd.Parameters.AddWithValue("@Count", CancelTag);
                cmd.Parameters.AddWithValue("@Dt", dttime.ToString("yyyy-MM-dd"));               
                cmd.Parameters.AddWithValue("@AGENTID", AgentId);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            return dt;
        }

        private void DataGridCollection_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            foreach (DataGridViewRow row in DataGridCollection.Rows)
            {
                string val = row.Cells[5].Value.ToString();
                if (row.Cells[5].Value.ToString() == "R")
                {
                    row.Cells[5].Value = "Running";
                }
                else
                {

                }
            }
        }

        private void DataGridCollection_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
           
        }

        private void chckCanceled_CheckedChanged(object sender, EventArgs e)
        {
            if (txtAgent.Text == string.Empty || txtAgentName.Text == string.Empty)
            {
                MessageBox.Show("Select Agent", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtAgent.Focus();
                return;
            }
            else
            {
                grProgress.Visible = true;
                progressBar.Visible = true;
                Thread thread = new Thread(LoadTable);
                thread.Start();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtAgent.Text == string.Empty || txtAgentName.Text == string.Empty)
                {
                    MessageBox.Show("Select Agent", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtAgent.Focus();
                    return;
                }
                else
                {
                    button1.Enabled = false;
                    int runas = 1;
                    SQLDBHelper db = new SQLDBHelper();
                    DateTime dte = Convert.ToDateTime(DtpCollDate.Text);
                    int AgentId = (int)txtAgent.Tag;
                    SqlParameter[] para = {
                        new SqlParameter("@Dt",dte.ToString("yyyy-MM-dd")),
                        new SqlParameter("@Agentid",AgentId)
                    };
                    db.ExecuteQuery(CommandType.StoredProcedure, "SP_ReverseBalancedteProcess", para);

                    SqlParameter[] paraDet = {
                        new SqlParameter("@Dt",dte.ToString("yyyy-MM-dd")),
                        new SqlParameter("@Agentid",AgentId),
                        new SqlParameter("@Runas",runas)
                    };
                    db.ExecuteQuery(CommandType.StoredProcedure, "SP_BalancedteProcessNew", paraDet);
                }
                MessageBox.Show("Completed Successfully", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                button1.Enabled = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }      
    }
}
