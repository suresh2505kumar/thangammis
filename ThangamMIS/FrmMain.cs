﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;

using System.Windows.Forms;

namespace ThangamMIS
{
    public partial class FrmMain : Form
    {    

        public FrmMain()
        {
            InitializeComponent();
            this.FormClosing += FrmMain_FormClosing;
        }
        [DllImport("user32.dll")]
        static extern IntPtr SetParent(IntPtr hwc, IntPtr hwp);
        private void FrmMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                DialogResult result = MessageBox.Show("Do you really want to exit?", "Closing", MessageBoxButtons.YesNo, MessageBoxIcon.Stop);
                if (result == DialogResult.Yes)
                {
                    Environment.Exit(0);
                }
                else
                {
                    e.Cancel = true;
                }
            }
            else
            {
                e.Cancel = true;
            }

        }

        private void FrmMain_Load(object sender, EventArgs e)
        {
            MdiClient ctlMDI;
            foreach (Control ctl in this.Controls)
            {
                try
                {
                    ctlMDI = (MdiClient)ctl;

                    ctlMDI.BackColor = this.BackColor;
                }
                catch
                {
                }
            }
            string Name = FrmLogin.UserName;
            if(Name == "bijoy")
            {
                transactionsToolStripMenuItem.Visible = true;
                scanToolStripMenuItem.Visible = true;
                sendSMSToolStripMenuItem.Visible = true;
            }  
            else if(Name == "admin")
            {
                transactionsToolStripMenuItem.Visible = false;
                scanToolStripMenuItem.Visible = false;
                sendSMSToolStripMenuItem.Visible = true;
            } 
            else if(Name == "mis")
            {
                transactionsToolStripMenuItem.Visible = false;
                scanToolStripMenuItem.Visible = false;
                collectionToolStripMenuItem.Visible = false;
            }
           
        }
        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            
        }

        private void dailyCollectionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Internet.FormID = 1;
            FrmDaily dailycollection = new FrmDaily();
            dailycollection.MdiParent = this;
            dailycollection.StartPosition = FormStartPosition.CenterScreen;
            dailycollection.Show();
        }

        private void monthlyCollectionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Internet.FormID = 2;
            FrmDaily Monthly = new FrmDaily();
            Monthly.MdiParent = this;
            Monthly.StartPosition = FormStartPosition.CenterScreen;
            Monthly.Text = "MIS - Monthly Collection";
            Monthly.Show();
        }

        private void scanToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Process p = Process.Start(Application.StartupPath + "\\scan2web.exe");
            Thread.Sleep(500);            
            p.WaitForInputIdle();
            SetParent(p.MainWindowHandle, this.Handle);
        }

        private void importToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmImport import = new FrmImport();
            import.MdiParent = this;
            import.StartPosition = FormStartPosition.CenterScreen;
            import.Show();
        }

        private void collectionViewToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmCollectionView collection = new FrmCollectionView();
            collection.MdiParent = this;
            collection.StartPosition = FormStartPosition.CenterScreen;
            collection.Show();
        }

        private void collectionUploadRecordToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmUpldView upldview = new FrmUpldView();
            upldview.MdiParent = this;
            upldview.StartPosition = FormStartPosition.CenterScreen;
            upldview.Show();
        }

        private void balanceDateReconcileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmrRconcile reconcile = new FrmrRconcile();
            reconcile.MdiParent = this;
            reconcile.StartPosition = FormStartPosition.CenterScreen;
            reconcile.Show();
        }

        private void dailyCollectionCompletedToolStripMenuItem_Click(object sender, EventArgs e)
        {

            Internet.FormID = 3;
            FrmDaily dailycollection = new FrmDaily();
            dailycollection.MdiParent = this;
            dailycollection.StartPosition = FormStartPosition.CenterScreen;
            dailycollection.Show();
        }

        private void monthlyCollectionCompletedToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Internet.FormID = 4;
            FrmDaily Monthly = new FrmDaily();
            Monthly.MdiParent = this;
            Monthly.StartPosition = FormStartPosition.CenterScreen;
            Monthly.Text = "MIS - Monthly Collection Completed";
            Monthly.Show();
        }

        private void dailyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Internet.FormID = 1;
            FrmSendSMS dailycollection = new FrmSendSMS();
            dailycollection.MdiParent = this;
            dailycollection.StartPosition = FormStartPosition.CenterScreen;
            dailycollection.Show();
        }

        private void monthlyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Internet.FormID = 2;
            FrmSendSMS dailycollection = new FrmSendSMS();
            dailycollection.MdiParent = this;
            dailycollection.StartPosition = FormStartPosition.CenterScreen;
            dailycollection.Show();
        }
    }
}
