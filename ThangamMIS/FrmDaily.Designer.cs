﻿namespace ThangamMIS
{
    partial class FrmDaily
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.grFront = new System.Windows.Forms.GroupBox();
            this.txtCmp = new System.Windows.Forms.TextBox();
            this.label52 = new System.Windows.Forms.Label();
            this.label48 = new System.Windows.Forms.Label();
            this.grChitSearch = new System.Windows.Forms.GroupBox();
            this.txtChitNameSearch = new System.Windows.Forms.TextBox();
            this.DataGridChitSearch = new System.Windows.Forms.DataGridView();
            this.label50 = new System.Windows.Forms.Label();
            this.btnRefersh = new System.Windows.Forms.Button();
            this.label47 = new System.Windows.Forms.Label();
            this.txtBarcodeSearch = new System.Windows.Forms.TextBox();
            this.grProgress = new System.Windows.Forms.GroupBox();
            this.progressBar = new CircularProgressBar.CircularProgressBar();
            this.label44 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.label46 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.DataGridDaily = new System.Windows.Forms.DataGridView();
            this.BtnEdit = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.txtDividend = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtChitName = new System.Windows.Forms.TextBox();
            this.txtEnDt = new System.Windows.Forms.TextBox();
            this.txtComDate = new System.Windows.Forms.TextBox();
            this.label40 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.btnDownload = new System.Windows.Forms.Button();
            this.GrBack = new System.Windows.Forms.GroupBox();
            this.lblStDt = new System.Windows.Forms.Label();
            this.btnExport = new System.Windows.Forms.Button();
            this.btnZoom = new System.Windows.Forms.Button();
            this.txtChitDid = new System.Windows.Forms.TextBox();
            this.txtCustId = new System.Windows.Forms.TextBox();
            this.pickCustImage = new System.Windows.Forms.PictureBox();
            this.btnLoadSignature = new System.Windows.Forms.Button();
            this.btnLoadAddress = new System.Windows.Forms.Button();
            this.BtnBack = new System.Windows.Forms.Button();
            this.tabDaily = new System.Windows.Forms.TabControl();
            this.tabMonthly = new System.Windows.Forms.TabPage();
            this.lblPendingDue = new System.Windows.Forms.Label();
            this.lb = new System.Windows.Forms.Label();
            this.lblDuePaid = new System.Windows.Forms.Label();
            this.label53 = new System.Windows.Forms.Label();
            this.lblDividend = new System.Windows.Forms.Label();
            this.label51 = new System.Windows.Forms.Label();
            this.lblduetobePaid = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.lblMonthlyTotal = new System.Windows.Forms.Label();
            this.label49 = new System.Windows.Forms.Label();
            this.dataGridMonthlyCollection = new System.Windows.Forms.DataGridView();
            this.DGVMonth = new System.Windows.Forms.DataGridView();
            this.tabCollectionDetails = new System.Windows.Forms.TabPage();
            this.btnUpdateBalDate = new System.Windows.Forms.Button();
            this.btnProcessBaldate = new System.Windows.Forms.Button();
            this.lblTotalAmount = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.DataGridCollectionDetails = new System.Windows.Forms.DataGridView();
            this.lblBalDate = new System.Windows.Forms.Label();
            this.txtBalDate = new System.Windows.Forms.TextBox();
            this.label28 = new System.Windows.Forms.Label();
            this.txtNoDays = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.txtLastAuctionNo = new System.Windows.Forms.TextBox();
            this.label33 = new System.Windows.Forms.Label();
            this.txtPaymentDate2 = new System.Windows.Forms.TextBox();
            this.label34 = new System.Windows.Forms.Label();
            this.txtPaymentDate1 = new System.Windows.Forms.TextBox();
            this.label35 = new System.Windows.Forms.Label();
            this.txtAmountPaid = new System.Windows.Forms.TextBox();
            this.label36 = new System.Windows.Forms.Label();
            this.txtDividendPerMember = new System.Windows.Forms.TextBox();
            this.label29 = new System.Windows.Forms.Label();
            this.txtDixcountAmount = new System.Windows.Forms.TextBox();
            this.label30 = new System.Windows.Forms.Label();
            this.txtAuctionDate = new System.Windows.Forms.TextBox();
            this.label31 = new System.Windows.Forms.Label();
            this.txtAuctionNo = new System.Windows.Forms.TextBox();
            this.label32 = new System.Windows.Forms.Label();
            this.tabChitwiseAuction = new System.Windows.Forms.TabPage();
            this.DataGridChitwiseAuction = new System.Windows.Forms.DataGridView();
            this.tabOtherChitDetails = new System.Windows.Forms.TabPage();
            this.DataGridOtherChitDetails = new System.Windows.Forms.DataGridView();
            this.tabTerminationDetails = new System.Windows.Forms.TabPage();
            this.LblEndDate = new System.Windows.Forms.Label();
            this.lblStartDate = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.cmbCustomer = new System.Windows.Forms.ComboBox();
            this.label37 = new System.Windows.Forms.Label();
            this.DataGridTerminationDetails = new System.Windows.Forms.DataGridView();
            this.btnLoadID = new System.Windows.Forms.Button();
            this.txtAmountPerchit = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.lblAuction = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.txtChitAmount = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.txtMobile = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.txtEndDate = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.cmbTicketNumber = new System.Windows.Forms.ComboBox();
            this.label16 = new System.Windows.Forms.Label();
            this.txtBalanceDays = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txtBalanceDue = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.txtDuetobePaid = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.txtCommenceDate = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.txtaddress2 = new System.Windows.Forms.TextBox();
            this.txtFather = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.txtAgent = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.txtCompany = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.lblTenureType = new System.Windows.Forms.Label();
            this.txtDividentChit = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtpaidDays = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtPendingDue = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtActaulDuePaid = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtAddress1 = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtCustomer = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtBarcode = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtChit = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.grZoom = new System.Windows.Forms.GroupBox();
            this.button1 = new System.Windows.Forms.Button();
            this.trackScroolBar = new System.Windows.Forms.TrackBar();
            this.imagePanel1 = new ThangamMIS.ImagePanel();
            this.Btnexit = new System.Windows.Forms.Button();
            this.ChckCompletedChit = new System.Windows.Forms.CheckBox();
            this.grFront.SuspendLayout();
            this.grChitSearch.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridChitSearch)).BeginInit();
            this.grProgress.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridDaily)).BeginInit();
            this.GrBack.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pickCustImage)).BeginInit();
            this.tabDaily.SuspendLayout();
            this.tabMonthly.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridMonthlyCollection)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DGVMonth)).BeginInit();
            this.tabCollectionDetails.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridCollectionDetails)).BeginInit();
            this.tabPage2.SuspendLayout();
            this.tabChitwiseAuction.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridChitwiseAuction)).BeginInit();
            this.tabOtherChitDetails.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridOtherChitDetails)).BeginInit();
            this.tabTerminationDetails.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridTerminationDetails)).BeginInit();
            this.grZoom.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackScroolBar)).BeginInit();
            this.SuspendLayout();
            // 
            // grFront
            // 
            this.grFront.Controls.Add(this.txtCmp);
            this.grFront.Controls.Add(this.label52);
            this.grFront.Controls.Add(this.label48);
            this.grFront.Controls.Add(this.grChitSearch);
            this.grFront.Controls.Add(this.label50);
            this.grFront.Controls.Add(this.btnRefersh);
            this.grFront.Controls.Add(this.label47);
            this.grFront.Controls.Add(this.txtBarcodeSearch);
            this.grFront.Controls.Add(this.grProgress);
            this.grFront.Controls.Add(this.label44);
            this.grFront.Controls.Add(this.label45);
            this.grFront.Controls.Add(this.label46);
            this.grFront.Controls.Add(this.label43);
            this.grFront.Controls.Add(this.label41);
            this.grFront.Controls.Add(this.label11);
            this.grFront.Controls.Add(this.DataGridDaily);
            this.grFront.Controls.Add(this.BtnEdit);
            this.grFront.Controls.Add(this.label2);
            this.grFront.Controls.Add(this.txtDividend);
            this.grFront.Controls.Add(this.label1);
            this.grFront.Controls.Add(this.txtChitName);
            this.grFront.Controls.Add(this.txtEnDt);
            this.grFront.Controls.Add(this.txtComDate);
            this.grFront.Controls.Add(this.label40);
            this.grFront.Controls.Add(this.label27);
            this.grFront.Location = new System.Drawing.Point(11, 6);
            this.grFront.Name = "grFront";
            this.grFront.Size = new System.Drawing.Size(1015, 591);
            this.grFront.TabIndex = 6;
            this.grFront.TabStop = false;
            // 
            // txtCmp
            // 
            this.txtCmp.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCmp.Location = new System.Drawing.Point(861, 56);
            this.txtCmp.Name = "txtCmp";
            this.txtCmp.Size = new System.Drawing.Size(127, 26);
            this.txtCmp.TabIndex = 39;
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label52.Location = new System.Drawing.Point(892, 36);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(66, 18);
            this.label52.TabIndex = 38;
            this.label52.Text = "Company";
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label48.Location = new System.Drawing.Point(145, 60);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(82, 15);
            this.label48.TabIndex = 33;
            this.label48.Text = "Canceled Chit";
            // 
            // grChitSearch
            // 
            this.grChitSearch.Controls.Add(this.txtChitNameSearch);
            this.grChitSearch.Controls.Add(this.DataGridChitSearch);
            this.grChitSearch.Location = new System.Drawing.Point(543, 15);
            this.grChitSearch.Name = "grChitSearch";
            this.grChitSearch.Size = new System.Drawing.Size(331, 262);
            this.grChitSearch.TabIndex = 17;
            this.grChitSearch.TabStop = false;
            this.grChitSearch.Text = "Search";
            // 
            // txtChitNameSearch
            // 
            this.txtChitNameSearch.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtChitNameSearch.Location = new System.Drawing.Point(6, 12);
            this.txtChitNameSearch.Name = "txtChitNameSearch";
            this.txtChitNameSearch.Size = new System.Drawing.Size(312, 26);
            this.txtChitNameSearch.TabIndex = 18;
            this.txtChitNameSearch.TextChanged += new System.EventHandler(this.txtChitNameSearch_TextChanged);
            this.txtChitNameSearch.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtChitName_KeyDown);
            // 
            // DataGridChitSearch
            // 
            this.DataGridChitSearch.AllowUserToAddRows = false;
            this.DataGridChitSearch.BackgroundColor = System.Drawing.SystemColors.ControlDark;
            this.DataGridChitSearch.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridChitSearch.Location = new System.Drawing.Point(6, 37);
            this.DataGridChitSearch.Name = "DataGridChitSearch";
            this.DataGridChitSearch.ReadOnly = true;
            this.DataGridChitSearch.RowHeadersVisible = false;
            this.DataGridChitSearch.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DataGridChitSearch.Size = new System.Drawing.Size(312, 211);
            this.DataGridChitSearch.TabIndex = 0;
            this.DataGridChitSearch.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.DataGridChitSearch_CellMouseDoubleClick);
            this.DataGridChitSearch.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtChitName_KeyDown);
            // 
            // label50
            // 
            this.label50.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.label50.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label50.Location = new System.Drawing.Point(122, 59);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(17, 17);
            this.label50.TabIndex = 32;
            // 
            // btnRefersh
            // 
            this.btnRefersh.Location = new System.Drawing.Point(835, 8);
            this.btnRefersh.Name = "btnRefersh";
            this.btnRefersh.Size = new System.Drawing.Size(89, 29);
            this.btnRefersh.TabIndex = 31;
            this.btnRefersh.Text = "Refresh";
            this.btnRefersh.UseVisualStyleBackColor = true;
            this.btnRefersh.Click += new System.EventHandler(this.btnRefersh_Click);
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label47.Location = new System.Drawing.Point(249, 58);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(58, 18);
            this.label47.TabIndex = 30;
            this.label47.Text = "Barcode";
            // 
            // txtBarcodeSearch
            // 
            this.txtBarcodeSearch.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtBarcodeSearch.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBarcodeSearch.Location = new System.Drawing.Point(319, 54);
            this.txtBarcodeSearch.Name = "txtBarcodeSearch";
            this.txtBarcodeSearch.Size = new System.Drawing.Size(218, 26);
            this.txtBarcodeSearch.TabIndex = 29;
            this.txtBarcodeSearch.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtBarcodeSearch_KeyDown);
            this.txtBarcodeSearch.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtBarcodeSearch_KeyPress);
            // 
            // grProgress
            // 
            this.grProgress.BackColor = System.Drawing.Color.White;
            this.grProgress.Controls.Add(this.progressBar);
            this.grProgress.Location = new System.Drawing.Point(414, 184);
            this.grProgress.Name = "grProgress";
            this.grProgress.Size = new System.Drawing.Size(192, 196);
            this.grProgress.TabIndex = 28;
            this.grProgress.TabStop = false;
            this.grProgress.Visible = false;
            // 
            // progressBar
            // 
            this.progressBar.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.progressBar.AnimationFunction = WinFormAnimation.KnownAnimationFunctions.Liner;
            this.progressBar.AnimationSpeed = 500;
            this.progressBar.BackColor = System.Drawing.Color.White;
            this.progressBar.Font = new System.Drawing.Font("Arial", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.progressBar.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.progressBar.InnerColor = System.Drawing.Color.White;
            this.progressBar.InnerMargin = 5;
            this.progressBar.InnerWidth = 5;
            this.progressBar.Location = new System.Drawing.Point(6, 8);
            this.progressBar.MarqueeAnimationSpeed = 2000;
            this.progressBar.Name = "progressBar";
            this.progressBar.OuterColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(73)))), ((int)(((byte)(94)))));
            this.progressBar.OuterMargin = -11;
            this.progressBar.OuterWidth = 10;
            this.progressBar.ProgressColor = System.Drawing.Color.FromArgb(((int)(((byte)(194)))), ((int)(((byte)(251)))), ((int)(((byte)(50)))));
            this.progressBar.ProgressWidth = 10;
            this.progressBar.SecondaryFont = new System.Drawing.Font("Microsoft Sans Serif", 4.125F);
            this.progressBar.Size = new System.Drawing.Size(180, 180);
            this.progressBar.StartAngle = 270;
            this.progressBar.Style = System.Windows.Forms.ProgressBarStyle.Marquee;
            this.progressBar.SubscriptColor = System.Drawing.Color.Silver;
            this.progressBar.SubscriptMargin = new System.Windows.Forms.Padding(0);
            this.progressBar.SubscriptText = "";
            this.progressBar.SuperscriptColor = System.Drawing.Color.Silver;
            this.progressBar.SuperscriptMargin = new System.Windows.Forms.Padding(0);
            this.progressBar.SuperscriptText = "";
            this.progressBar.TabIndex = 10;
            this.progressBar.Text = "Please Wait";
            this.progressBar.TextMargin = new System.Windows.Forms.Padding(0, 2, 0, 0);
            this.progressBar.Value = 80;
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label44.Location = new System.Drawing.Point(32, 15);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(92, 15);
            this.label44.TabIndex = 26;
            this.label44.Text = "Auction Carried";
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label45.Location = new System.Drawing.Point(32, 60);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(84, 15);
            this.label45.TabIndex = 25;
            this.label45.Text = "Selected Rows";
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label46.Location = new System.Drawing.Point(32, 38);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(114, 15);
            this.label46.TabIndex = 24;
            this.label46.Text = "Auction Not Carried";
            // 
            // label43
            // 
            this.label43.BackColor = System.Drawing.Color.LightGreen;
            this.label43.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label43.Location = new System.Drawing.Point(14, 14);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(17, 17);
            this.label43.TabIndex = 23;
            // 
            // label41
            // 
            this.label41.BackColor = System.Drawing.Color.RoyalBlue;
            this.label41.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label41.Location = new System.Drawing.Point(14, 59);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(17, 17);
            this.label41.TabIndex = 22;
            // 
            // label11
            // 
            this.label11.BackColor = System.Drawing.Color.LightBlue;
            this.label11.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(14, 36);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(17, 17);
            this.label11.TabIndex = 21;
            // 
            // DataGridDaily
            // 
            this.DataGridDaily.AllowUserToAddRows = false;
            this.DataGridDaily.AllowUserToDeleteRows = false;
            this.DataGridDaily.BackgroundColor = System.Drawing.Color.White;
            this.DataGridDaily.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridDaily.Location = new System.Drawing.Point(11, 89);
            this.DataGridDaily.Name = "DataGridDaily";
            this.DataGridDaily.ReadOnly = true;
            this.DataGridDaily.RowHeadersVisible = false;
            this.DataGridDaily.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DataGridDaily.Size = new System.Drawing.Size(998, 494);
            this.DataGridDaily.TabIndex = 12;
            this.DataGridDaily.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridDaily_CellContentClick);
            this.DataGridDaily.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.DataGridDaily_CellFormatting);
            this.DataGridDaily.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.DataGridDaily_CellMouseDoubleClick);
            this.DataGridDaily.KeyDown += new System.Windows.Forms.KeyEventHandler(this.DataGridDaily_KeyDown);
            this.DataGridDaily.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.DataGridDaily_KeyPress);
            // 
            // BtnEdit
            // 
            this.BtnEdit.Location = new System.Drawing.Point(930, 8);
            this.BtnEdit.Name = "BtnEdit";
            this.BtnEdit.Size = new System.Drawing.Size(75, 29);
            this.BtnEdit.TabIndex = 10;
            this.BtnEdit.Text = "View";
            this.BtnEdit.UseVisualStyleBackColor = true;
            this.BtnEdit.Click += new System.EventHandler(this.BtnEdit_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(563, 13);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(97, 18);
            this.label2.TabIndex = 9;
            this.label2.Text = "Total Dividend";
            // 
            // txtDividend
            // 
            this.txtDividend.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDividend.Location = new System.Drawing.Point(680, 9);
            this.txtDividend.Name = "txtDividend";
            this.txtDividend.Size = new System.Drawing.Size(127, 26);
            this.txtDividend.TabIndex = 8;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(236, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(73, 18);
            this.label1.TabIndex = 7;
            this.label1.Text = "Chit Name";
            // 
            // txtChitName
            // 
            this.txtChitName.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtChitName.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtChitName.Location = new System.Drawing.Point(319, 9);
            this.txtChitName.Name = "txtChitName";
            this.txtChitName.Size = new System.Drawing.Size(218, 26);
            this.txtChitName.TabIndex = 6;
            this.txtChitName.MouseClick += new System.Windows.Forms.MouseEventHandler(this.txtChitName_MouseClick);
            this.txtChitName.TextChanged += new System.EventHandler(this.txtChitName_TextChanged);
            this.txtChitName.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtChitName_KeyDown);
            // 
            // txtEnDt
            // 
            this.txtEnDt.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtEnDt.Location = new System.Drawing.Point(708, 56);
            this.txtEnDt.Name = "txtEnDt";
            this.txtEnDt.Size = new System.Drawing.Size(127, 26);
            this.txtEnDt.TabIndex = 37;
            // 
            // txtComDate
            // 
            this.txtComDate.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtComDate.Location = new System.Drawing.Point(560, 56);
            this.txtComDate.Name = "txtComDate";
            this.txtComDate.Size = new System.Drawing.Size(127, 26);
            this.txtComDate.TabIndex = 36;
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label40.Location = new System.Drawing.Point(739, 36);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(63, 18);
            this.label40.TabIndex = 35;
            this.label40.Text = "End Date";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(563, 36);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(68, 18);
            this.label27.TabIndex = 34;
            this.label27.Text = "Com Date";
            // 
            // btnDownload
            // 
            this.btnDownload.Location = new System.Drawing.Point(823, 599);
            this.btnDownload.Name = "btnDownload";
            this.btnDownload.Size = new System.Drawing.Size(125, 28);
            this.btnDownload.TabIndex = 20;
            this.btnDownload.Text = "Download to Excel";
            this.btnDownload.UseVisualStyleBackColor = true;
            this.btnDownload.Click += new System.EventHandler(this.btnDownload_Click);
            // 
            // GrBack
            // 
            this.GrBack.Controls.Add(this.lblStDt);
            this.GrBack.Controls.Add(this.btnExport);
            this.GrBack.Controls.Add(this.btnZoom);
            this.GrBack.Controls.Add(this.txtChitDid);
            this.GrBack.Controls.Add(this.txtCustId);
            this.GrBack.Controls.Add(this.pickCustImage);
            this.GrBack.Controls.Add(this.btnLoadSignature);
            this.GrBack.Controls.Add(this.btnLoadAddress);
            this.GrBack.Controls.Add(this.BtnBack);
            this.GrBack.Controls.Add(this.tabDaily);
            this.GrBack.Controls.Add(this.btnLoadID);
            this.GrBack.Controls.Add(this.txtAmountPerchit);
            this.GrBack.Controls.Add(this.label25);
            this.GrBack.Controls.Add(this.lblAuction);
            this.GrBack.Controls.Add(this.label23);
            this.GrBack.Controls.Add(this.txtChitAmount);
            this.GrBack.Controls.Add(this.label22);
            this.GrBack.Controls.Add(this.txtMobile);
            this.GrBack.Controls.Add(this.label21);
            this.GrBack.Controls.Add(this.txtEndDate);
            this.GrBack.Controls.Add(this.label20);
            this.GrBack.Controls.Add(this.cmbTicketNumber);
            this.GrBack.Controls.Add(this.label16);
            this.GrBack.Controls.Add(this.txtBalanceDays);
            this.GrBack.Controls.Add(this.label12);
            this.GrBack.Controls.Add(this.txtBalanceDue);
            this.GrBack.Controls.Add(this.label13);
            this.GrBack.Controls.Add(this.txtDuetobePaid);
            this.GrBack.Controls.Add(this.label14);
            this.GrBack.Controls.Add(this.txtCommenceDate);
            this.GrBack.Controls.Add(this.label15);
            this.GrBack.Controls.Add(this.txtaddress2);
            this.GrBack.Controls.Add(this.txtFather);
            this.GrBack.Controls.Add(this.label17);
            this.GrBack.Controls.Add(this.txtAgent);
            this.GrBack.Controls.Add(this.label18);
            this.GrBack.Controls.Add(this.txtCompany);
            this.GrBack.Controls.Add(this.label19);
            this.GrBack.Controls.Add(this.lblTenureType);
            this.GrBack.Controls.Add(this.txtDividentChit);
            this.GrBack.Controls.Add(this.label10);
            this.GrBack.Controls.Add(this.txtpaidDays);
            this.GrBack.Controls.Add(this.label9);
            this.GrBack.Controls.Add(this.txtPendingDue);
            this.GrBack.Controls.Add(this.label8);
            this.GrBack.Controls.Add(this.txtActaulDuePaid);
            this.GrBack.Controls.Add(this.label7);
            this.GrBack.Controls.Add(this.txtAddress1);
            this.GrBack.Controls.Add(this.label6);
            this.GrBack.Controls.Add(this.txtCustomer);
            this.GrBack.Controls.Add(this.label5);
            this.GrBack.Controls.Add(this.txtBarcode);
            this.GrBack.Controls.Add(this.label4);
            this.GrBack.Controls.Add(this.txtChit);
            this.GrBack.Controls.Add(this.label3);
            this.GrBack.Controls.Add(this.grZoom);
            this.GrBack.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GrBack.Location = new System.Drawing.Point(12, 6);
            this.GrBack.Name = "GrBack";
            this.GrBack.Size = new System.Drawing.Size(1015, 591);
            this.GrBack.TabIndex = 13;
            this.GrBack.TabStop = false;
            this.GrBack.Enter += new System.EventHandler(this.GrBack_Enter);
            // 
            // lblStDt
            // 
            this.lblStDt.AutoSize = true;
            this.lblStDt.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStDt.ForeColor = System.Drawing.Color.Blue;
            this.lblStDt.Location = new System.Drawing.Point(879, 90);
            this.lblStDt.Name = "lblStDt";
            this.lblStDt.Size = new System.Drawing.Size(13, 18);
            this.lblStDt.TabIndex = 59;
            this.lblStDt.Text = "-";
            this.lblStDt.Visible = false;
            // 
            // btnExport
            // 
            this.btnExport.Location = new System.Drawing.Point(709, 84);
            this.btnExport.Name = "btnExport";
            this.btnExport.Size = new System.Drawing.Size(148, 31);
            this.btnExport.TabIndex = 58;
            this.btnExport.Text = "Export to Excel";
            this.btnExport.UseVisualStyleBackColor = true;
            this.btnExport.Click += new System.EventHandler(this.btnExport_Click);
            // 
            // btnZoom
            // 
            this.btnZoom.Location = new System.Drawing.Point(785, 558);
            this.btnZoom.Name = "btnZoom";
            this.btnZoom.Size = new System.Drawing.Size(106, 27);
            this.btnZoom.TabIndex = 57;
            this.btnZoom.Text = "Zoom Picture";
            this.btnZoom.UseVisualStyleBackColor = true;
            this.btnZoom.Click += new System.EventHandler(this.btnZoom_Click);
            // 
            // txtChitDid
            // 
            this.txtChitDid.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.txtChitDid.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtChitDid.Location = new System.Drawing.Point(281, 72);
            this.txtChitDid.Name = "txtChitDid";
            this.txtChitDid.Size = new System.Drawing.Size(121, 26);
            this.txtChitDid.TabIndex = 55;
            this.txtChitDid.Visible = false;
            // 
            // txtCustId
            // 
            this.txtCustId.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.txtCustId.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCustId.Location = new System.Drawing.Point(280, 41);
            this.txtCustId.Name = "txtCustId";
            this.txtCustId.Size = new System.Drawing.Size(121, 26);
            this.txtCustId.TabIndex = 54;
            this.txtCustId.Visible = false;
            // 
            // pickCustImage
            // 
            this.pickCustImage.Location = new System.Drawing.Point(704, 242);
            this.pickCustImage.Name = "pickCustImage";
            this.pickCustImage.Size = new System.Drawing.Size(305, 307);
            this.pickCustImage.TabIndex = 53;
            this.pickCustImage.TabStop = false;
            // 
            // btnLoadSignature
            // 
            this.btnLoadSignature.Location = new System.Drawing.Point(910, 181);
            this.btnLoadSignature.Name = "btnLoadSignature";
            this.btnLoadSignature.Size = new System.Drawing.Size(102, 34);
            this.btnLoadSignature.TabIndex = 52;
            this.btnLoadSignature.Text = "View Sign";
            this.btnLoadSignature.UseVisualStyleBackColor = true;
            this.btnLoadSignature.Click += new System.EventHandler(this.btnLoadSignature_Click);
            // 
            // btnLoadAddress
            // 
            this.btnLoadAddress.Location = new System.Drawing.Point(808, 182);
            this.btnLoadAddress.Name = "btnLoadAddress";
            this.btnLoadAddress.Size = new System.Drawing.Size(102, 32);
            this.btnLoadAddress.TabIndex = 51;
            this.btnLoadAddress.Text = "View Address";
            this.btnLoadAddress.UseVisualStyleBackColor = true;
            this.btnLoadAddress.Click += new System.EventHandler(this.btnLoadAddress_Click);
            // 
            // BtnBack
            // 
            this.BtnBack.Location = new System.Drawing.Point(704, 558);
            this.BtnBack.Name = "BtnBack";
            this.BtnBack.Size = new System.Drawing.Size(75, 27);
            this.BtnBack.TabIndex = 50;
            this.BtnBack.Text = "Back";
            this.BtnBack.UseVisualStyleBackColor = true;
            this.BtnBack.Click += new System.EventHandler(this.BtnBack_Click);
            // 
            // tabDaily
            // 
            this.tabDaily.Controls.Add(this.tabMonthly);
            this.tabDaily.Controls.Add(this.tabCollectionDetails);
            this.tabDaily.Controls.Add(this.tabPage2);
            this.tabDaily.Controls.Add(this.tabChitwiseAuction);
            this.tabDaily.Controls.Add(this.tabOtherChitDetails);
            this.tabDaily.Controls.Add(this.tabTerminationDetails);
            this.tabDaily.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabDaily.Location = new System.Drawing.Point(63, 274);
            this.tabDaily.Name = "tabDaily";
            this.tabDaily.SelectedIndex = 0;
            this.tabDaily.Size = new System.Drawing.Size(635, 308);
            this.tabDaily.TabIndex = 49;
            this.tabDaily.SelectedIndexChanged += new System.EventHandler(this.tabDaily_SelectedIndexChanged);
            // 
            // tabMonthly
            // 
            this.tabMonthly.Controls.Add(this.lblPendingDue);
            this.tabMonthly.Controls.Add(this.lb);
            this.tabMonthly.Controls.Add(this.lblDuePaid);
            this.tabMonthly.Controls.Add(this.label53);
            this.tabMonthly.Controls.Add(this.lblDividend);
            this.tabMonthly.Controls.Add(this.label51);
            this.tabMonthly.Controls.Add(this.lblduetobePaid);
            this.tabMonthly.Controls.Add(this.label24);
            this.tabMonthly.Controls.Add(this.lblMonthlyTotal);
            this.tabMonthly.Controls.Add(this.label49);
            this.tabMonthly.Controls.Add(this.dataGridMonthlyCollection);
            this.tabMonthly.Controls.Add(this.DGVMonth);
            this.tabMonthly.Location = new System.Drawing.Point(4, 27);
            this.tabMonthly.Name = "tabMonthly";
            this.tabMonthly.Padding = new System.Windows.Forms.Padding(3);
            this.tabMonthly.Size = new System.Drawing.Size(627, 277);
            this.tabMonthly.TabIndex = 5;
            this.tabMonthly.Text = "Monthly Collection";
            this.tabMonthly.UseVisualStyleBackColor = true;
            // 
            // lblPendingDue
            // 
            this.lblPendingDue.AutoSize = true;
            this.lblPendingDue.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPendingDue.ForeColor = System.Drawing.Color.Blue;
            this.lblPendingDue.Location = new System.Drawing.Point(546, 236);
            this.lblPendingDue.Name = "lblPendingDue";
            this.lblPendingDue.Size = new System.Drawing.Size(11, 15);
            this.lblPendingDue.TabIndex = 67;
            this.lblPendingDue.Text = "-";
            // 
            // lb
            // 
            this.lb.AutoSize = true;
            this.lb.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb.Location = new System.Drawing.Point(546, 254);
            this.lb.Name = "lb";
            this.lb.Size = new System.Drawing.Size(75, 15);
            this.lb.TabIndex = 66;
            this.lb.Text = "Pending Due";
            // 
            // lblDuePaid
            // 
            this.lblDuePaid.AutoSize = true;
            this.lblDuePaid.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDuePaid.ForeColor = System.Drawing.Color.Blue;
            this.lblDuePaid.Location = new System.Drawing.Point(480, 235);
            this.lblDuePaid.Name = "lblDuePaid";
            this.lblDuePaid.Size = new System.Drawing.Size(11, 15);
            this.lblDuePaid.TabIndex = 65;
            this.lblDuePaid.Text = "-";
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label53.Location = new System.Drawing.Point(480, 253);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(56, 15);
            this.label53.TabIndex = 64;
            this.label53.Text = "Due Paid";
            // 
            // lblDividend
            // 
            this.lblDividend.AutoSize = true;
            this.lblDividend.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDividend.ForeColor = System.Drawing.Color.Blue;
            this.lblDividend.Location = new System.Drawing.Point(408, 236);
            this.lblDividend.Name = "lblDividend";
            this.lblDividend.Size = new System.Drawing.Size(11, 15);
            this.lblDividend.TabIndex = 63;
            this.lblDividend.Text = "-";
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label51.Location = new System.Drawing.Point(408, 255);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(56, 15);
            this.label51.TabIndex = 62;
            this.label51.Text = "Dividend";
            // 
            // lblduetobePaid
            // 
            this.lblduetobePaid.AutoSize = true;
            this.lblduetobePaid.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblduetobePaid.ForeColor = System.Drawing.Color.Blue;
            this.lblduetobePaid.Location = new System.Drawing.Point(309, 237);
            this.lblduetobePaid.Name = "lblduetobePaid";
            this.lblduetobePaid.Size = new System.Drawing.Size(11, 15);
            this.lblduetobePaid.TabIndex = 61;
            this.lblduetobePaid.Text = "-";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(309, 255);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(83, 15);
            this.label24.TabIndex = 60;
            this.label24.Text = "Due to bePaid";
            // 
            // lblMonthlyTotal
            // 
            this.lblMonthlyTotal.AutoSize = true;
            this.lblMonthlyTotal.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMonthlyTotal.ForeColor = System.Drawing.Color.Blue;
            this.lblMonthlyTotal.Location = new System.Drawing.Point(190, 247);
            this.lblMonthlyTotal.Name = "lblMonthlyTotal";
            this.lblMonthlyTotal.Size = new System.Drawing.Size(11, 15);
            this.lblMonthlyTotal.TabIndex = 59;
            this.lblMonthlyTotal.Text = "-";
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label49.Location = new System.Drawing.Point(133, 247);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(38, 18);
            this.label49.TabIndex = 58;
            this.label49.Text = "Total";
            // 
            // dataGridMonthlyCollection
            // 
            this.dataGridMonthlyCollection.AllowUserToAddRows = false;
            this.dataGridMonthlyCollection.BackgroundColor = System.Drawing.Color.White;
            this.dataGridMonthlyCollection.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridMonthlyCollection.Location = new System.Drawing.Point(6, 6);
            this.dataGridMonthlyCollection.Name = "dataGridMonthlyCollection";
            this.dataGridMonthlyCollection.ReadOnly = true;
            this.dataGridMonthlyCollection.RowHeadersVisible = false;
            this.dataGridMonthlyCollection.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridMonthlyCollection.Size = new System.Drawing.Size(262, 229);
            this.dataGridMonthlyCollection.TabIndex = 1;
            // 
            // DGVMonth
            // 
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DGVMonth.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.DGVMonth.BackgroundColor = System.Drawing.Color.White;
            this.DGVMonth.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DGVMonth.Location = new System.Drawing.Point(274, 6);
            this.DGVMonth.Name = "DGVMonth";
            this.DGVMonth.RowHeadersVisible = false;
            this.DGVMonth.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DGVMonth.Size = new System.Drawing.Size(350, 228);
            this.DGVMonth.TabIndex = 2;
            // 
            // tabCollectionDetails
            // 
            this.tabCollectionDetails.Controls.Add(this.btnUpdateBalDate);
            this.tabCollectionDetails.Controls.Add(this.btnProcessBaldate);
            this.tabCollectionDetails.Controls.Add(this.lblTotalAmount);
            this.tabCollectionDetails.Controls.Add(this.label42);
            this.tabCollectionDetails.Controls.Add(this.DataGridCollectionDetails);
            this.tabCollectionDetails.Controls.Add(this.lblBalDate);
            this.tabCollectionDetails.Controls.Add(this.txtBalDate);
            this.tabCollectionDetails.Controls.Add(this.label28);
            this.tabCollectionDetails.Controls.Add(this.txtNoDays);
            this.tabCollectionDetails.Controls.Add(this.label26);
            this.tabCollectionDetails.Location = new System.Drawing.Point(4, 27);
            this.tabCollectionDetails.Name = "tabCollectionDetails";
            this.tabCollectionDetails.Padding = new System.Windows.Forms.Padding(3);
            this.tabCollectionDetails.Size = new System.Drawing.Size(627, 277);
            this.tabCollectionDetails.TabIndex = 0;
            this.tabCollectionDetails.Text = "Collection Details";
            this.tabCollectionDetails.ToolTipText = "CollectionDetails";
            this.tabCollectionDetails.UseVisualStyleBackColor = true;
            // 
            // btnUpdateBalDate
            // 
            this.btnUpdateBalDate.Location = new System.Drawing.Point(199, 247);
            this.btnUpdateBalDate.Name = "btnUpdateBalDate";
            this.btnUpdateBalDate.Size = new System.Drawing.Size(170, 25);
            this.btnUpdateBalDate.TabIndex = 59;
            this.btnUpdateBalDate.Text = "Update Balance Date";
            this.btnUpdateBalDate.UseVisualStyleBackColor = true;
            // 
            // btnProcessBaldate
            // 
            this.btnProcessBaldate.Location = new System.Drawing.Point(10, 247);
            this.btnProcessBaldate.Name = "btnProcessBaldate";
            this.btnProcessBaldate.Size = new System.Drawing.Size(183, 25);
            this.btnProcessBaldate.TabIndex = 58;
            this.btnProcessBaldate.Text = "Process Bal Date";
            this.btnProcessBaldate.UseVisualStyleBackColor = true;
            this.btnProcessBaldate.Click += new System.EventHandler(this.btnProcessBaldate_Click);
            // 
            // lblTotalAmount
            // 
            this.lblTotalAmount.AutoSize = true;
            this.lblTotalAmount.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotalAmount.ForeColor = System.Drawing.Color.Blue;
            this.lblTotalAmount.Location = new System.Drawing.Point(470, 251);
            this.lblTotalAmount.Name = "lblTotalAmount";
            this.lblTotalAmount.Size = new System.Drawing.Size(11, 15);
            this.lblTotalAmount.TabIndex = 57;
            this.lblTotalAmount.Text = "-";
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label42.Location = new System.Drawing.Point(421, 249);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(38, 18);
            this.label42.TabIndex = 56;
            this.label42.Text = "Total";
            this.label42.Click += new System.EventHandler(this.label42_Click);
            // 
            // DataGridCollectionDetails
            // 
            this.DataGridCollectionDetails.AllowUserToAddRows = false;
            this.DataGridCollectionDetails.BackgroundColor = System.Drawing.Color.White;
            this.DataGridCollectionDetails.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridCollectionDetails.Location = new System.Drawing.Point(11, 38);
            this.DataGridCollectionDetails.Name = "DataGridCollectionDetails";
            this.DataGridCollectionDetails.RowHeadersVisible = false;
            this.DataGridCollectionDetails.Size = new System.Drawing.Size(551, 207);
            this.DataGridCollectionDetails.TabIndex = 55;
            // 
            // lblBalDate
            // 
            this.lblBalDate.AutoSize = true;
            this.lblBalDate.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBalDate.ForeColor = System.Drawing.Color.Blue;
            this.lblBalDate.Location = new System.Drawing.Point(522, 15);
            this.lblBalDate.Name = "lblBalDate";
            this.lblBalDate.Size = new System.Drawing.Size(11, 15);
            this.lblBalDate.TabIndex = 54;
            this.lblBalDate.Text = "-";
            // 
            // txtBalDate
            // 
            this.txtBalDate.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBalDate.Location = new System.Drawing.Point(374, 9);
            this.txtBalDate.Name = "txtBalDate";
            this.txtBalDate.Size = new System.Drawing.Size(141, 26);
            this.txtBalDate.TabIndex = 53;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Location = new System.Drawing.Point(306, 11);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(63, 18);
            this.label28.TabIndex = 52;
            this.label28.Text = "Bal. Date";
            // 
            // txtNoDays
            // 
            this.txtNoDays.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNoDays.Location = new System.Drawing.Point(180, 8);
            this.txtNoDays.Name = "txtNoDays";
            this.txtNoDays.Size = new System.Drawing.Size(100, 26);
            this.txtNoDays.TabIndex = 51;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(101, 12);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(75, 18);
            this.label26.TabIndex = 50;
            this.label26.Text = "No.of Days";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.txtLastAuctionNo);
            this.tabPage2.Controls.Add(this.label33);
            this.tabPage2.Controls.Add(this.txtPaymentDate2);
            this.tabPage2.Controls.Add(this.label34);
            this.tabPage2.Controls.Add(this.txtPaymentDate1);
            this.tabPage2.Controls.Add(this.label35);
            this.tabPage2.Controls.Add(this.txtAmountPaid);
            this.tabPage2.Controls.Add(this.label36);
            this.tabPage2.Controls.Add(this.txtDividendPerMember);
            this.tabPage2.Controls.Add(this.label29);
            this.tabPage2.Controls.Add(this.txtDixcountAmount);
            this.tabPage2.Controls.Add(this.label30);
            this.tabPage2.Controls.Add(this.txtAuctionDate);
            this.tabPage2.Controls.Add(this.label31);
            this.tabPage2.Controls.Add(this.txtAuctionNo);
            this.tabPage2.Controls.Add(this.label32);
            this.tabPage2.Location = new System.Drawing.Point(4, 27);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(627, 277);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Auction Details";
            this.tabPage2.UseVisualStyleBackColor = true;
            this.tabPage2.Click += new System.EventHandler(this.tabPage2_Click);
            // 
            // txtLastAuctionNo
            // 
            this.txtLastAuctionNo.BackColor = System.Drawing.Color.Cyan;
            this.txtLastAuctionNo.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLastAuctionNo.Location = new System.Drawing.Point(257, 223);
            this.txtLastAuctionNo.Name = "txtLastAuctionNo";
            this.txtLastAuctionNo.Size = new System.Drawing.Size(100, 26);
            this.txtLastAuctionNo.TabIndex = 31;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.Location = new System.Drawing.Point(139, 227);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(116, 18);
            this.label33.TabIndex = 30;
            this.label33.Text = "Latest Auction No";
            // 
            // txtPaymentDate2
            // 
            this.txtPaymentDate2.BackColor = System.Drawing.Color.Cyan;
            this.txtPaymentDate2.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPaymentDate2.Location = new System.Drawing.Point(257, 194);
            this.txtPaymentDate2.Name = "txtPaymentDate2";
            this.txtPaymentDate2.Size = new System.Drawing.Size(100, 26);
            this.txtPaymentDate2.TabIndex = 29;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.Location = new System.Drawing.Point(151, 198);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(102, 18);
            this.label34.TabIndex = 28;
            this.label34.Text = "Payment Date1";
            // 
            // txtPaymentDate1
            // 
            this.txtPaymentDate1.BackColor = System.Drawing.Color.Cyan;
            this.txtPaymentDate1.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPaymentDate1.Location = new System.Drawing.Point(257, 165);
            this.txtPaymentDate1.Name = "txtPaymentDate1";
            this.txtPaymentDate1.Size = new System.Drawing.Size(100, 26);
            this.txtPaymentDate1.TabIndex = 27;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.Location = new System.Drawing.Point(151, 169);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(102, 18);
            this.label35.TabIndex = 26;
            this.label35.Text = "Payment Date1";
            // 
            // txtAmountPaid
            // 
            this.txtAmountPaid.BackColor = System.Drawing.Color.Cyan;
            this.txtAmountPaid.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAmountPaid.Location = new System.Drawing.Point(257, 136);
            this.txtAmountPaid.Name = "txtAmountPaid";
            this.txtAmountPaid.Size = new System.Drawing.Size(100, 26);
            this.txtAmountPaid.TabIndex = 25;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label36.Location = new System.Drawing.Point(163, 140);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(88, 18);
            this.label36.TabIndex = 24;
            this.label36.Text = "Amount Paid";
            // 
            // txtDividendPerMember
            // 
            this.txtDividendPerMember.BackColor = System.Drawing.Color.Cyan;
            this.txtDividendPerMember.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDividendPerMember.Location = new System.Drawing.Point(257, 107);
            this.txtDividendPerMember.Name = "txtDividendPerMember";
            this.txtDividendPerMember.Size = new System.Drawing.Size(100, 26);
            this.txtDividendPerMember.TabIndex = 23;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.Location = new System.Drawing.Point(115, 111);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(144, 18);
            this.label29.TabIndex = 22;
            this.label29.Text = "Dividend Per Member";
            // 
            // txtDixcountAmount
            // 
            this.txtDixcountAmount.BackColor = System.Drawing.Color.Cyan;
            this.txtDixcountAmount.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDixcountAmount.Location = new System.Drawing.Point(257, 78);
            this.txtDixcountAmount.Name = "txtDixcountAmount";
            this.txtDixcountAmount.Size = new System.Drawing.Size(100, 26);
            this.txtDixcountAmount.TabIndex = 21;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.Location = new System.Drawing.Point(139, 82);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(115, 18);
            this.label30.TabIndex = 20;
            this.label30.Text = "Discount Amount";
            // 
            // txtAuctionDate
            // 
            this.txtAuctionDate.BackColor = System.Drawing.Color.Cyan;
            this.txtAuctionDate.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAuctionDate.Location = new System.Drawing.Point(257, 49);
            this.txtAuctionDate.Name = "txtAuctionDate";
            this.txtAuctionDate.Size = new System.Drawing.Size(100, 26);
            this.txtAuctionDate.TabIndex = 19;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.Location = new System.Drawing.Point(164, 53);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(87, 18);
            this.label31.TabIndex = 18;
            this.label31.Text = "Auction Date";
            // 
            // txtAuctionNo
            // 
            this.txtAuctionNo.BackColor = System.Drawing.Color.Cyan;
            this.txtAuctionNo.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAuctionNo.Location = new System.Drawing.Point(257, 20);
            this.txtAuctionNo.Name = "txtAuctionNo";
            this.txtAuctionNo.Size = new System.Drawing.Size(100, 26);
            this.txtAuctionNo.TabIndex = 17;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.Location = new System.Drawing.Point(174, 24);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(76, 18);
            this.label32.TabIndex = 16;
            this.label32.Text = "Auction No";
            // 
            // tabChitwiseAuction
            // 
            this.tabChitwiseAuction.Controls.Add(this.DataGridChitwiseAuction);
            this.tabChitwiseAuction.Location = new System.Drawing.Point(4, 27);
            this.tabChitwiseAuction.Name = "tabChitwiseAuction";
            this.tabChitwiseAuction.Padding = new System.Windows.Forms.Padding(3);
            this.tabChitwiseAuction.Size = new System.Drawing.Size(627, 277);
            this.tabChitwiseAuction.TabIndex = 2;
            this.tabChitwiseAuction.Text = "Chitwise Auction";
            this.tabChitwiseAuction.ToolTipText = "Chitwise Auction";
            this.tabChitwiseAuction.UseVisualStyleBackColor = true;
            // 
            // DataGridChitwiseAuction
            // 
            this.DataGridChitwiseAuction.AllowUserToAddRows = false;
            this.DataGridChitwiseAuction.BackgroundColor = System.Drawing.Color.White;
            this.DataGridChitwiseAuction.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridChitwiseAuction.Location = new System.Drawing.Point(6, 6);
            this.DataGridChitwiseAuction.Name = "DataGridChitwiseAuction";
            this.DataGridChitwiseAuction.RowHeadersVisible = false;
            this.DataGridChitwiseAuction.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DataGridChitwiseAuction.Size = new System.Drawing.Size(589, 265);
            this.DataGridChitwiseAuction.TabIndex = 0;
            // 
            // tabOtherChitDetails
            // 
            this.tabOtherChitDetails.Controls.Add(this.DataGridOtherChitDetails);
            this.tabOtherChitDetails.Location = new System.Drawing.Point(4, 27);
            this.tabOtherChitDetails.Name = "tabOtherChitDetails";
            this.tabOtherChitDetails.Padding = new System.Windows.Forms.Padding(3);
            this.tabOtherChitDetails.Size = new System.Drawing.Size(627, 277);
            this.tabOtherChitDetails.TabIndex = 3;
            this.tabOtherChitDetails.Text = "Other Chit Details";
            this.tabOtherChitDetails.ToolTipText = "Other Chit Details";
            this.tabOtherChitDetails.UseVisualStyleBackColor = true;
            // 
            // DataGridOtherChitDetails
            // 
            this.DataGridOtherChitDetails.AllowUserToAddRows = false;
            this.DataGridOtherChitDetails.BackgroundColor = System.Drawing.Color.White;
            this.DataGridOtherChitDetails.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridOtherChitDetails.Location = new System.Drawing.Point(6, 6);
            this.DataGridOtherChitDetails.Name = "DataGridOtherChitDetails";
            this.DataGridOtherChitDetails.RowHeadersVisible = false;
            this.DataGridOtherChitDetails.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DataGridOtherChitDetails.Size = new System.Drawing.Size(587, 265);
            this.DataGridOtherChitDetails.TabIndex = 0;
            // 
            // tabTerminationDetails
            // 
            this.tabTerminationDetails.Controls.Add(this.LblEndDate);
            this.tabTerminationDetails.Controls.Add(this.lblStartDate);
            this.tabTerminationDetails.Controls.Add(this.label38);
            this.tabTerminationDetails.Controls.Add(this.label39);
            this.tabTerminationDetails.Controls.Add(this.cmbCustomer);
            this.tabTerminationDetails.Controls.Add(this.label37);
            this.tabTerminationDetails.Controls.Add(this.DataGridTerminationDetails);
            this.tabTerminationDetails.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabTerminationDetails.Location = new System.Drawing.Point(4, 27);
            this.tabTerminationDetails.Name = "tabTerminationDetails";
            this.tabTerminationDetails.Padding = new System.Windows.Forms.Padding(3);
            this.tabTerminationDetails.Size = new System.Drawing.Size(627, 277);
            this.tabTerminationDetails.TabIndex = 4;
            this.tabTerminationDetails.Text = "Termination Details";
            this.tabTerminationDetails.ToolTipText = "Termination Details";
            this.tabTerminationDetails.UseVisualStyleBackColor = true;
            // 
            // LblEndDate
            // 
            this.LblEndDate.AutoSize = true;
            this.LblEndDate.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblEndDate.ForeColor = System.Drawing.Color.Blue;
            this.LblEndDate.Location = new System.Drawing.Point(514, 14);
            this.LblEndDate.Name = "LblEndDate";
            this.LblEndDate.Size = new System.Drawing.Size(13, 18);
            this.LblEndDate.TabIndex = 51;
            this.LblEndDate.Text = "-";
            // 
            // lblStartDate
            // 
            this.lblStartDate.AutoSize = true;
            this.lblStartDate.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStartDate.ForeColor = System.Drawing.Color.Blue;
            this.lblStartDate.Location = new System.Drawing.Point(360, 14);
            this.lblStartDate.Name = "lblStartDate";
            this.lblStartDate.Size = new System.Drawing.Size(13, 18);
            this.lblStartDate.TabIndex = 50;
            this.lblStartDate.Text = "-";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label38.Location = new System.Drawing.Point(446, 14);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(63, 18);
            this.label38.TabIndex = 38;
            this.label38.Text = "End Date";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label39.Location = new System.Drawing.Point(288, 14);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(69, 18);
            this.label39.TabIndex = 37;
            this.label39.Text = "Start Date";
            // 
            // cmbCustomer
            // 
            this.cmbCustomer.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbCustomer.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbCustomer.FormattingEnabled = true;
            this.cmbCustomer.Location = new System.Drawing.Point(110, 10);
            this.cmbCustomer.Name = "cmbCustomer";
            this.cmbCustomer.Size = new System.Drawing.Size(173, 26);
            this.cmbCustomer.TabIndex = 36;
            this.cmbCustomer.SelectedIndexChanged += new System.EventHandler(this.cmbCustomer_SelectedIndexChanged);
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label37.Location = new System.Drawing.Point(27, 13);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(68, 18);
            this.label37.TabIndex = 35;
            this.label37.Text = "Customer";
            // 
            // DataGridTerminationDetails
            // 
            this.DataGridTerminationDetails.AllowUserToAddRows = false;
            this.DataGridTerminationDetails.BackgroundColor = System.Drawing.Color.White;
            this.DataGridTerminationDetails.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridTerminationDetails.Location = new System.Drawing.Point(23, 42);
            this.DataGridTerminationDetails.Name = "DataGridTerminationDetails";
            this.DataGridTerminationDetails.RowHeadersVisible = false;
            this.DataGridTerminationDetails.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DataGridTerminationDetails.Size = new System.Drawing.Size(532, 217);
            this.DataGridTerminationDetails.TabIndex = 0;
            // 
            // btnLoadID
            // 
            this.btnLoadID.Location = new System.Drawing.Point(704, 181);
            this.btnLoadID.Name = "btnLoadID";
            this.btnLoadID.Size = new System.Drawing.Size(102, 32);
            this.btnLoadID.TabIndex = 45;
            this.btnLoadID.Text = "View ID";
            this.btnLoadID.UseVisualStyleBackColor = true;
            this.btnLoadID.Click += new System.EventHandler(this.btnLoadImage_Click);
            // 
            // txtAmountPerchit
            // 
            this.txtAmountPerchit.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.txtAmountPerchit.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAmountPerchit.Location = new System.Drawing.Point(479, 163);
            this.txtAmountPerchit.Name = "txtAmountPerchit";
            this.txtAmountPerchit.Size = new System.Drawing.Size(100, 26);
            this.txtAmountPerchit.TabIndex = 44;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(389, 166);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(86, 18);
            this.label25.TabIndex = 43;
            this.label25.Text = "Amt Per Chit";
            // 
            // lblAuction
            // 
            this.lblAuction.AutoSize = true;
            this.lblAuction.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAuction.ForeColor = System.Drawing.Color.Blue;
            this.lblAuction.Location = new System.Drawing.Point(585, 214);
            this.lblAuction.Name = "lblAuction";
            this.lblAuction.Size = new System.Drawing.Size(13, 18);
            this.lblAuction.TabIndex = 42;
            this.lblAuction.Text = "-";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.ForeColor = System.Drawing.Color.Red;
            this.label23.Location = new System.Drawing.Point(585, 169);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(13, 18);
            this.label23.TabIndex = 41;
            this.label23.Text = "-";
            // 
            // txtChitAmount
            // 
            this.txtChitAmount.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.txtChitAmount.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtChitAmount.Location = new System.Drawing.Point(110, 162);
            this.txtChitAmount.Name = "txtChitAmount";
            this.txtChitAmount.Size = new System.Drawing.Size(100, 26);
            this.txtChitAmount.TabIndex = 40;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(12, 166);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(86, 18);
            this.label22.TabIndex = 39;
            this.label22.Text = "Chit Amount";
            // 
            // txtMobile
            // 
            this.txtMobile.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.txtMobile.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMobile.Location = new System.Drawing.Point(106, 133);
            this.txtMobile.Name = "txtMobile";
            this.txtMobile.Size = new System.Drawing.Size(185, 26);
            this.txtMobile.TabIndex = 38;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(25, 135);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(73, 18);
            this.label21.TabIndex = 37;
            this.label21.Text = "Mobile No";
            // 
            // txtEndDate
            // 
            this.txtEndDate.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.txtEndDate.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtEndDate.Location = new System.Drawing.Point(579, 133);
            this.txtEndDate.Name = "txtEndDate";
            this.txtEndDate.Size = new System.Drawing.Size(108, 26);
            this.txtEndDate.TabIndex = 36;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(512, 136);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(63, 18);
            this.label20.TabIndex = 35;
            this.label20.Text = "End Date";
            // 
            // cmbTicketNumber
            // 
            this.cmbTicketNumber.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbTicketNumber.FormattingEnabled = true;
            this.cmbTicketNumber.Location = new System.Drawing.Point(315, 15);
            this.cmbTicketNumber.Name = "cmbTicketNumber";
            this.cmbTicketNumber.Size = new System.Drawing.Size(182, 26);
            this.cmbTicketNumber.TabIndex = 34;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(215, 18);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(99, 18);
            this.label16.TabIndex = 33;
            this.label16.Text = "Ticket Number";
            // 
            // txtBalanceDays
            // 
            this.txtBalanceDays.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.txtBalanceDays.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBalanceDays.Location = new System.Drawing.Point(479, 244);
            this.txtBalanceDays.Name = "txtBalanceDays";
            this.txtBalanceDays.Size = new System.Drawing.Size(100, 26);
            this.txtBalanceDays.TabIndex = 32;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(387, 248);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(88, 18);
            this.label12.TabIndex = 31;
            this.label12.Text = "Balance Days";
            // 
            // txtBalanceDue
            // 
            this.txtBalanceDue.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.txtBalanceDue.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBalanceDue.Location = new System.Drawing.Point(479, 217);
            this.txtBalanceDue.Name = "txtBalanceDue";
            this.txtBalanceDue.Size = new System.Drawing.Size(100, 26);
            this.txtBalanceDue.TabIndex = 30;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(391, 224);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(84, 18);
            this.label13.TabIndex = 29;
            this.label13.Text = "Balance Due";
            // 
            // txtDuetobePaid
            // 
            this.txtDuetobePaid.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.txtDuetobePaid.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDuetobePaid.Location = new System.Drawing.Point(805, 41);
            this.txtDuetobePaid.Name = "txtDuetobePaid";
            this.txtDuetobePaid.Size = new System.Drawing.Size(100, 26);
            this.txtDuetobePaid.TabIndex = 28;
            this.txtDuetobePaid.Visible = false;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(704, 45);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(98, 18);
            this.label14.TabIndex = 27;
            this.label14.Text = "Due to be Paid";
            this.label14.Visible = false;
            // 
            // txtCommenceDate
            // 
            this.txtCommenceDate.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.txtCommenceDate.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCommenceDate.Location = new System.Drawing.Point(400, 133);
            this.txtCommenceDate.Name = "txtCommenceDate";
            this.txtCommenceDate.Size = new System.Drawing.Size(111, 26);
            this.txtCommenceDate.TabIndex = 26;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(292, 137);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(110, 18);
            this.label15.TabIndex = 25;
            this.label15.Text = "Commence Date";
            // 
            // txtaddress2
            // 
            this.txtaddress2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.txtaddress2.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtaddress2.Location = new System.Drawing.Point(398, 106);
            this.txtaddress2.Name = "txtaddress2";
            this.txtaddress2.Size = new System.Drawing.Size(288, 26);
            this.txtaddress2.TabIndex = 24;
            // 
            // txtFather
            // 
            this.txtFather.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.txtFather.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFather.Location = new System.Drawing.Point(479, 72);
            this.txtFather.Name = "txtFather";
            this.txtFather.Size = new System.Drawing.Size(168, 26);
            this.txtFather.TabIndex = 22;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(411, 76);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(48, 18);
            this.label17.TabIndex = 21;
            this.label17.Text = "Father";
            // 
            // txtAgent
            // 
            this.txtAgent.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.txtAgent.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAgent.Location = new System.Drawing.Point(479, 41);
            this.txtAgent.Name = "txtAgent";
            this.txtAgent.Size = new System.Drawing.Size(168, 26);
            this.txtAgent.TabIndex = 20;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(417, 45);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(45, 18);
            this.label18.TabIndex = 19;
            this.label18.Text = "Agent";
            // 
            // txtCompany
            // 
            this.txtCompany.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.txtCompany.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCompany.Location = new System.Drawing.Point(574, 10);
            this.txtCompany.Name = "txtCompany";
            this.txtCompany.Size = new System.Drawing.Size(112, 26);
            this.txtCompany.TabIndex = 18;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(503, 13);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(66, 18);
            this.label19.TabIndex = 17;
            this.label19.Text = "Company";
            // 
            // lblTenureType
            // 
            this.lblTenureType.AutoSize = true;
            this.lblTenureType.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTenureType.ForeColor = System.Drawing.Color.Blue;
            this.lblTenureType.Location = new System.Drawing.Point(225, 163);
            this.lblTenureType.Name = "lblTenureType";
            this.lblTenureType.Size = new System.Drawing.Size(13, 18);
            this.lblTenureType.TabIndex = 16;
            this.lblTenureType.Text = "-";
            // 
            // txtDividentChit
            // 
            this.txtDividentChit.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.txtDividentChit.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDividentChit.Location = new System.Drawing.Point(479, 190);
            this.txtDividentChit.Name = "txtDividentChit";
            this.txtDividentChit.Size = new System.Drawing.Size(100, 26);
            this.txtDividentChit.TabIndex = 15;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(411, 195);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(64, 18);
            this.label10.TabIndex = 14;
            this.label10.Text = "Dividend";
            // 
            // txtpaidDays
            // 
            this.txtpaidDays.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.txtpaidDays.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtpaidDays.Location = new System.Drawing.Point(110, 190);
            this.txtpaidDays.Name = "txtpaidDays";
            this.txtpaidDays.Size = new System.Drawing.Size(100, 26);
            this.txtpaidDays.TabIndex = 13;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(31, 196);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(67, 18);
            this.label9.TabIndex = 12;
            this.label9.Text = "Paid Days";
            // 
            // txtPendingDue
            // 
            this.txtPendingDue.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.txtPendingDue.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPendingDue.Location = new System.Drawing.Point(110, 248);
            this.txtPendingDue.Name = "txtPendingDue";
            this.txtPendingDue.Size = new System.Drawing.Size(100, 26);
            this.txtPendingDue.TabIndex = 11;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(11, 251);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(87, 18);
            this.label8.TabIndex = 10;
            this.label8.Text = "Pending Due";
            // 
            // txtActaulDuePaid
            // 
            this.txtActaulDuePaid.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.txtActaulDuePaid.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtActaulDuePaid.Location = new System.Drawing.Point(110, 218);
            this.txtActaulDuePaid.Name = "txtActaulDuePaid";
            this.txtActaulDuePaid.Size = new System.Drawing.Size(100, 26);
            this.txtActaulDuePaid.TabIndex = 9;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(10, 225);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(88, 18);
            this.label7.TabIndex = 8;
            this.label7.Text = "Paid Amount";
            // 
            // txtAddress1
            // 
            this.txtAddress1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.txtAddress1.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAddress1.Location = new System.Drawing.Point(106, 106);
            this.txtAddress1.Name = "txtAddress1";
            this.txtAddress1.Size = new System.Drawing.Size(284, 26);
            this.txtAddress1.TabIndex = 7;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(40, 110);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(58, 18);
            this.label6.TabIndex = 6;
            this.label6.Text = "Address";
            // 
            // txtCustomer
            // 
            this.txtCustomer.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.txtCustomer.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCustomer.Location = new System.Drawing.Point(106, 76);
            this.txtCustomer.Name = "txtCustomer";
            this.txtCustomer.Size = new System.Drawing.Size(168, 26);
            this.txtCustomer.TabIndex = 5;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(30, 80);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(68, 18);
            this.label5.TabIndex = 4;
            this.label5.Text = "Customer";
            // 
            // txtBarcode
            // 
            this.txtBarcode.BackColor = System.Drawing.Color.PaleGreen;
            this.txtBarcode.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBarcode.Location = new System.Drawing.Point(106, 45);
            this.txtBarcode.Name = "txtBarcode";
            this.txtBarcode.Size = new System.Drawing.Size(168, 26);
            this.txtBarcode.TabIndex = 3;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(38, 49);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(60, 18);
            this.label4.TabIndex = 2;
            this.label4.Text = "BarCode";
            // 
            // txtChit
            // 
            this.txtChit.BackColor = System.Drawing.Color.PaleGreen;
            this.txtChit.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtChit.Location = new System.Drawing.Point(106, 16);
            this.txtChit.Name = "txtChit";
            this.txtChit.Size = new System.Drawing.Size(100, 26);
            this.txtChit.TabIndex = 1;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(65, 20);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(33, 18);
            this.label3.TabIndex = 0;
            this.label3.Text = "Chit";
            // 
            // grZoom
            // 
            this.grZoom.Controls.Add(this.button1);
            this.grZoom.Controls.Add(this.trackScroolBar);
            this.grZoom.Controls.Add(this.imagePanel1);
            this.grZoom.Location = new System.Drawing.Point(6, 15);
            this.grZoom.Name = "grZoom";
            this.grZoom.Size = new System.Drawing.Size(692, 567);
            this.grZoom.TabIndex = 56;
            this.grZoom.TabStop = false;
            this.grZoom.Text = "Zoom Picture";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(494, 513);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 27);
            this.button1.TabIndex = 51;
            this.button1.Text = "Hide";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // trackScroolBar
            // 
            this.trackScroolBar.Location = new System.Drawing.Point(246, 513);
            this.trackScroolBar.Name = "trackScroolBar";
            this.trackScroolBar.Size = new System.Drawing.Size(223, 45);
            this.trackScroolBar.TabIndex = 1;
            this.trackScroolBar.Scroll += new System.EventHandler(this.trackScroolBar_Scroll);
            // 
            // imagePanel1
            // 
            this.imagePanel1.CanvasSize = new System.Drawing.Size(60, 40);
            this.imagePanel1.Image = null;
            this.imagePanel1.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBilinear;
            this.imagePanel1.Location = new System.Drawing.Point(27, 25);
            this.imagePanel1.Margin = new System.Windows.Forms.Padding(4);
            this.imagePanel1.Name = "imagePanel1";
            this.imagePanel1.Size = new System.Drawing.Size(652, 484);
            this.imagePanel1.TabIndex = 0;
            this.imagePanel1.Zoom = 1F;
            // 
            // Btnexit
            // 
            this.Btnexit.Location = new System.Drawing.Point(948, 599);
            this.Btnexit.Name = "Btnexit";
            this.Btnexit.Size = new System.Drawing.Size(78, 29);
            this.Btnexit.TabIndex = 14;
            this.Btnexit.Text = "Exit";
            this.Btnexit.UseVisualStyleBackColor = true;
            this.Btnexit.Click += new System.EventHandler(this.Btnexit_Click_1);
            // 
            // ChckCompletedChit
            // 
            this.ChckCompletedChit.AutoSize = true;
            this.ChckCompletedChit.Location = new System.Drawing.Point(18, 604);
            this.ChckCompletedChit.Name = "ChckCompletedChit";
            this.ChckCompletedChit.Size = new System.Drawing.Size(97, 17);
            this.ChckCompletedChit.TabIndex = 21;
            this.ChckCompletedChit.Text = "Completed Chit";
            this.ChckCompletedChit.UseVisualStyleBackColor = true;
            // 
            // FrmDaily
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1041, 629);
            this.Controls.Add(this.ChckCompletedChit);
            this.Controls.Add(this.Btnexit);
            this.Controls.Add(this.btnDownload);
            this.Controls.Add(this.grFront);
            this.Controls.Add(this.GrBack);
            this.Name = "FrmDaily";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "MIS - Daily Collection";
            this.Load += new System.EventHandler(this.FrmDaily_Load);
            this.grFront.ResumeLayout(false);
            this.grFront.PerformLayout();
            this.grChitSearch.ResumeLayout(false);
            this.grChitSearch.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridChitSearch)).EndInit();
            this.grProgress.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DataGridDaily)).EndInit();
            this.GrBack.ResumeLayout(false);
            this.GrBack.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pickCustImage)).EndInit();
            this.tabDaily.ResumeLayout(false);
            this.tabMonthly.ResumeLayout(false);
            this.tabMonthly.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridMonthlyCollection)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DGVMonth)).EndInit();
            this.tabCollectionDetails.ResumeLayout(false);
            this.tabCollectionDetails.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridCollectionDetails)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.tabChitwiseAuction.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DataGridChitwiseAuction)).EndInit();
            this.tabOtherChitDetails.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DataGridOtherChitDetails)).EndInit();
            this.tabTerminationDetails.ResumeLayout(false);
            this.tabTerminationDetails.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridTerminationDetails)).EndInit();
            this.grZoom.ResumeLayout(false);
            this.grZoom.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackScroolBar)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox grFront;
        private System.Windows.Forms.Button BtnEdit;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtDividend;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtChitName;
        private System.Windows.Forms.DataGridView DataGridDaily;
        private System.Windows.Forms.GroupBox GrBack;
        private System.Windows.Forms.TextBox txtAddress1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtCustomer;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtBarcode;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtChit;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtActaulDuePaid;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtPendingDue;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtDividentChit;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label lblTenureType;
        private System.Windows.Forms.TextBox txtBalanceDays;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtBalanceDue;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtDuetobePaid;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txtCommenceDate;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox txtaddress2;
        private System.Windows.Forms.TextBox txtFather;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox txtAgent;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox txtCompany;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.ComboBox cmbTicketNumber;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox txtEndDate;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox txtMobile;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label lblAuction;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox txtAmountPerchit;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Button btnLoadID;
        private System.Windows.Forms.TabControl tabDaily;
        private System.Windows.Forms.TabPage tabCollectionDetails;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabChitwiseAuction;
        private System.Windows.Forms.TabPage tabOtherChitDetails;
        private System.Windows.Forms.TabPage tabTerminationDetails;
        private System.Windows.Forms.DataGridView DataGridCollectionDetails;
        private System.Windows.Forms.Label lblBalDate;
        private System.Windows.Forms.TextBox txtBalDate;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.TextBox txtNoDays;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.TextBox txtPaymentDate2;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.TextBox txtPaymentDate1;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.TextBox txtAmountPaid;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.TextBox txtDividendPerMember;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.TextBox txtDixcountAmount;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.TextBox txtAuctionDate;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.TextBox txtAuctionNo;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.DataGridView DataGridChitwiseAuction;
        private System.Windows.Forms.DataGridView DataGridOtherChitDetails;
        private System.Windows.Forms.DataGridView DataGridTerminationDetails;
        private System.Windows.Forms.Label LblEndDate;
        private System.Windows.Forms.Label lblStartDate;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.ComboBox cmbCustomer;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Button BtnBack;
        private System.Windows.Forms.GroupBox grChitSearch;
        private System.Windows.Forms.TextBox txtChitNameSearch;
        private System.Windows.Forms.DataGridView DataGridChitSearch;
        private System.Windows.Forms.Button btnLoadSignature;
        private System.Windows.Forms.Button btnLoadAddress;
        private System.Windows.Forms.PictureBox pickCustImage;
        private System.Windows.Forms.Label lblTotalAmount;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.TextBox txtLastAuctionNo;
        private System.Windows.Forms.TextBox txtCustId;
        private System.Windows.Forms.TextBox txtChitDid;
        private System.Windows.Forms.TextBox txtChitAmount;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox txtpaidDays;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TabPage tabMonthly;
        private System.Windows.Forms.DataGridView DGVMonth;
        private System.Windows.Forms.DataGridView dataGridMonthlyCollection;
        private System.Windows.Forms.Button btnDownload;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.GroupBox grProgress;
        private System.Windows.Forms.Label lblMonthlyTotal;
        private System.Windows.Forms.Label label49;
        private CircularProgressBar.CircularProgressBar progressBar;
        private System.Windows.Forms.GroupBox grZoom;
        private ImagePanel imagePanel1;
        private System.Windows.Forms.TrackBar trackScroolBar;
        private System.Windows.Forms.Button btnZoom;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.TextBox txtBarcodeSearch;
        private System.Windows.Forms.Button btnRefersh;
        private System.Windows.Forms.Button Btnexit;
        private System.Windows.Forms.Label lblPendingDue;
        private System.Windows.Forms.Label lb;
        private System.Windows.Forms.Label lblDuePaid;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.Label lblDividend;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.Label lblduetobePaid;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.TextBox txtEnDt;
        private System.Windows.Forms.TextBox txtComDate;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.TextBox txtCmp;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.Button btnExport;
        private System.Windows.Forms.Button btnUpdateBalDate;
        private System.Windows.Forms.Button btnProcessBaldate;
        private System.Windows.Forms.Label lblStDt;
        private System.Windows.Forms.CheckBox ChckCompletedChit;
    }
}