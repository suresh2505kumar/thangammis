﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Threading;
using System.Windows.Forms;

namespace ThangamMIS
{
    public partial class FrmrRconcile : Form
    {
        public FrmrRconcile()
        {
            InitializeComponent();
        }
        internal delegate void SetDataSourceDelegate(DataTable table);
        SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnStr"].ConnectionString);
        SqlCommand cmd = new SqlCommand();
        BindingSource bs = new BindingSource();
        private void FrmrRconcile_Load(object sender, EventArgs e)
        {
            grAgentSearch.Visible = false;
        }
        protected void LoadChit()
        {
            try
            {
                conn.Open();
                cmd = new SqlCommand();
                cmd.Connection = conn;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "SP_GetAgent";
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                bs.DataSource = dt;
                DataGridAgentSearch.DataSource = null;
                DataGridAgentSearch.AutoGenerateColumns = false;
                DataGridAgentSearch.ColumnCount = 3;
                DataGridAgentSearch.Columns[0].Name = "AgentId";
                DataGridAgentSearch.Columns[0].HeaderText = "AgentId";
                DataGridAgentSearch.Columns[0].DataPropertyName = "AgentId";
                DataGridAgentSearch.Columns[0].Visible = false;

                DataGridAgentSearch.Columns[1].Name = "AgentCode";
                DataGridAgentSearch.Columns[1].HeaderText = "AgentCode";
                DataGridAgentSearch.Columns[1].DataPropertyName = "AgentCode";

                DataGridAgentSearch.Columns[2].Name = "AgentName";
                DataGridAgentSearch.Columns[2].HeaderText = "AgentName";
                DataGridAgentSearch.Columns[2].DataPropertyName = "AgentName";
                DataGridAgentSearch.Columns[2].Width = 200;
                DataGridAgentSearch.DataSource = bs;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            finally
            {
                conn.Close();
                cmd.Dispose();
            }
        }
        protected void LoadTable()
        {
            DataTable dt = GetDatas();
            setDataSource(dt);

        }
        private void setDataSource(DataTable table)
        {
            if (this.InvokeRequired)
            {
                this.Invoke(new SetDataSourceDelegate(setDataSource), table);
            }
            else
            {
                FillDataGrid(table);
                grProgress.Visible = false;
                progressBar.Visible = false;
            }
        }
        private void FillDataGrid(DataTable table)
        {
            try
            {
                if (table.Rows.Count != 0)
                {
                    DataGridCollection.DataSource = null;
                    DataGridCollection.AutoGenerateColumns = false;
                    DataGridCollection.ColumnCount = 9;
                    DataGridCollection.Columns[0].Name = "ChitName";
                    DataGridCollection.Columns[0].HeaderText = "ChitName";
                    DataGridCollection.Columns[0].DataPropertyName = "ChitName";
                    DataGridCollection.Columns[0].Width = 80;

                    DataGridCollection.Columns[1].Name = "CustName";
                    DataGridCollection.Columns[1].HeaderText = "CustName";
                    DataGridCollection.Columns[1].DataPropertyName = "CustName";

                    DataGridCollection.Columns[2].Name = "SlNo";
                    DataGridCollection.Columns[2].HeaderText = "SlNo";
                    DataGridCollection.Columns[2].DataPropertyName = "slno";
                    DataGridCollection.Columns[2].Width = 50;

                    DataGridCollection.Columns[3].Name = "Coll Date";
                    DataGridCollection.Columns[3].HeaderText = "Coll Date";
                    DataGridCollection.Columns[3].DataPropertyName = "colldate";
                    DataGridCollection.Columns[3].DefaultCellStyle.Format = "dd-MM-yyyy";

                    DataGridCollection.Columns[4].Name = "Amount";
                    DataGridCollection.Columns[4].HeaderText = "Amount";
                    DataGridCollection.Columns[4].DataPropertyName = "amount";
                    DataGridCollection.Columns[4].Width = 70;

                    DataGridCollection.Columns[5].Name = "BalDate";
                    DataGridCollection.Columns[5].HeaderText = "BalDate";
                    DataGridCollection.Columns[5].DataPropertyName = "baldate";
                    DataGridCollection.Columns[5].DefaultCellStyle.Format = "dd-MM-yyyy";

                    DataGridCollection.Columns[6].Name = "Update BalDate";
                    DataGridCollection.Columns[6].HeaderText = "Update BalDate";
                    DataGridCollection.Columns[6].DataPropertyName = "updbaldate";
                    DataGridCollection.Columns[6].DefaultCellStyle.Format = "dd-MM-yyyy";

                    DataGridCollection.Columns[7].Name = "chitddid";
                    DataGridCollection.Columns[7].HeaderText = "chitddid";
                    DataGridCollection.Columns[7].DataPropertyName = "chitddid";
                    DataGridCollection.Columns[7].Visible = false;

                    DataGridCollection.Columns[8].Name = "chitdid";
                    DataGridCollection.Columns[8].HeaderText = "chitdid";
                    DataGridCollection.Columns[8].DataPropertyName = "chitdid";
                    DataGridCollection.Columns[8].Visible = false;
                    DataGridCollection.DataSource = table;
                    //txtToalamount.Text = table.Compute("Sum(COLLAMT)", "").ToString();
                    //txtRecords.Text = table.Rows.Count.ToString();
                }
                else
                {
                    MessageBox.Show("No Data Found for this date", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        protected DataTable GetDatas()
        {
            DataTable dt = new DataTable();
            try
            {
                DateTime dttime = Convert.ToDateTime(DtpCollDate.Text);
                int AgentId = (int)txtAgent.Tag;
                conn.Open();
                SqlCommand cmd1 = new SqlCommand();
                cmd1.CommandText = "SP_RECONCILATION2";
                cmd1.CommandType = CommandType.StoredProcedure;
                cmd1.Parameters.AddWithValue("@Agent", AgentId);
                cmd1.Parameters.AddWithValue("@Dt", dttime.ToString("yyyy-MM-dd"));
                cmd1.Connection = conn;
                cmd1.CommandTimeout = 1800;
                cmd1.ExecuteNonQuery();
                conn.Close();
                string Query = @"SELECT		b.slno,b.barcode,d.ChitName,c.CustName,a.chitddid,a.chitdid,a.colldate,a.amount,a.baldate,a.updbaldate FROM	MEMTRANSR a 
                                inner join	ChitTrans b on a.chitdid =b.ChitDid  and b.ChitTranTag ='A'
                                inner join	Customer c on b.CustId= c.Custid
                                inner join Chit D on b.ChitId = d.ChitId
                                where a.baldate <> a.updbaldate and a.updbaldate is not null
                                order by  a.chitdid,a.colldate";
                //string Query = @"Select b.ChitDid,a.collamt as CollAmt,c.amtpchit as AmtPChit,c.TenureType from Coll_AND a 
                //                inner join ChitTrans b on a.Chitid = b.Chitid and a.SlNo= b.slno and b.ChitTranTag ='A'
                //                inner join Chit c on c.Chitid = a.Chitid and a.SlNo= b.slno
                //                Where a.AgentId = " + AgentId + " and Cast(a.ColDate as date) = '" + dttime.ToString("yyyy-MM-dd") + "'";
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = Query;
                cmd.CommandType = CommandType.Text;
                //cmd.Parameters.AddWithValue("@AgentId", AgentId);
                //cmd.Parameters.AddWithValue("@Date", dttime.ToString("yyyy-MM-dd"));
                cmd.Connection = conn;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            return dt;
        }
        protected DateTime GetOldBalDate(int ChitDid, DateTime dat)
        {
            DateTime oldBalDate = DateTime.Now;
            string Query = "Select Max(BalDate) as BalDate from MemTrans a where a.ChitDid = " + ChitDid + " and colldate >= '" + dat + "'";
            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = Query;
            cmd.CommandType = CommandType.Text;
            cmd.Connection = conn;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            oldBalDate = (DateTime)dt.Rows[0]["BalDate"];
            return oldBalDate;
        }
        private void txtAgentSearch_TextChanged(object sender, EventArgs e)
        {
            bs.Filter = string.Format("AgentName LIKE '%{0}%' OR AgentCode LIKE '%{1}%'", txtAgentSearch.Text, txtAgentSearch.Text);
        }

        private void txtAgentSearch_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Escape)
            {
                grAgentSearch.Visible = false;
                txtAgentSearch.Text = string.Empty;
            }
        }

        private void txtAgentSearch_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                grAgentSearch.Visible = false;
                txtAgentSearch.Text = string.Empty;
            }
        }

        private void DataGridAgentSearch_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {

        }

        private void DataGridAgentSearch_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            int Index = DataGridAgentSearch.SelectedCells[0].RowIndex;
            txtAgentName.Text = DataGridAgentSearch.Rows[Index].Cells[2].Value.ToString();
            txtAgent.Text = DataGridAgentSearch.Rows[Index].Cells[1].Value.ToString();
            txtAgent.Tag = DataGridAgentSearch.Rows[Index].Cells[0].Value;
            grAgentSearch.Visible = false;
            txtAgentSearch.Text = string.Empty;
            Thread thred = new Thread(LoadTable);
            thred.Start();
            grProgress.Visible = true;
            progressBar.Visible = true;
            //LoadTable();
        }

        private void txtAgent_MouseClick(object sender, MouseEventArgs e)
        {
            grAgentSearch.Visible = true;
            LoadChit();
            txtAgentSearch.Focus();
        }

        private void btnBalDateUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                for (int i = 0; i < DataGridCollection.Rows.Count; i++)
                {
                    DateTime BalDate = Convert.ToDateTime(DataGridCollection.Rows[i].Cells[6].Value.ToString());
                    DateTime NewBalDate = BalDate.AddDays(Convert.ToDouble(DataGridCollection.Rows[i].Cells[3].Value) / Convert.ToDouble(DataGridCollection.Rows[i].Cells[4].Value));
                    DataGridCollection.Rows[i].Cells[7].Value = NewBalDate.ToString("dd-MMM-yyyy");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //DateTime CollDate = Convert.ToDateTime(DtpCollDate.Text);
            //for (int i = 0; i < DataGridCollection.Rows.Count; i++)
            //{
            conn.Open();
            button1.Enabled = false;
            //DateTime NewBalDate = Convert.ToDateTime(DataGridCollection.Rows[i].Cells[7].Value.ToString());
            //int ChitDid = Convert.ToInt32(DataGridCollection.Rows[i].Cells[0].Value.ToString());
            cmd = new SqlCommand();
            cmd.CommandText = "SP_UpdateBalDate";
            cmd.CommandType = CommandType.StoredProcedure;
            //cmd.Parameters.AddWithValue("@BalDate", NewBalDate.ToString("yyyy-MM-dd"));
            //cmd.Parameters.AddWithValue("@CollDate", CollDate.ToString("yyyy-MM-dd"));
            //cmd.Parameters.AddWithValue("@ChitDid", ChitDid);
            cmd.Connection = conn;
            cmd.CommandTimeout = 1800;
            cmd.ExecuteNonQuery();
            conn.Close();
            //}
            MessageBox.Show("Balance Date Updated");
            button1.Enabled = true;
        }
    }
}

