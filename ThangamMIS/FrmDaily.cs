﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using Microsoft.Office.Interop;
using System.Windows.Forms;
using System.Configuration;
using System.Data.SqlClient;
using System.Globalization;
using System.Threading;
using System.Diagnostics;

namespace ThangamMIS
{
    public partial class FrmDaily : Form
    {
        public FrmDaily()
        {
            InitializeComponent();
            CultureInfo[] cultures = { new CultureInfo("en-IN") };
        }
        BindingSource bs = new BindingSource();
        int FormId;
        string Barcode = string.Empty;
        SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnStr"].ConnectionString);
        SqlConnection connPhoto = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnStrPhoto"].ConnectionString);
        SqlCommand cmd;
        public int chidID;
        public int chitDID;
        public bool EditMode = false;
        public bool Termination = false;
        public string TenureType = string.Empty;

        private void Btnexit_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void BtnEdit_Click(object sender, EventArgs e)
        {
            if (DataGridDaily.Rows.Count != 0)
            {
                int Index = DataGridDaily.SelectedRows[0].Index;
                chitDID = (int)DataGridDaily.Rows[Index].Cells[8].Value;
                string Auction = DataGridDaily.Rows[Index].Cells[12].Value.ToString();
                string Tag = DataGridDaily.Rows[Index].Cells[14].Value.ToString();
                if (Auction == "N")
                {
                    lblAuction.Text = "Auction Not Carried";
                }
                else
                {
                    lblAuction.Text = "Auction Carried";
                }
                if (Tag == "C         ")
                {
                    lblAuction.Text = "Canceled Chit";
                }
                EditMode = true;
                DataTable dt = GetdataDet(chitDID);
                LoadTextbox(dt);
                grFront.Visible = false;
                GrBack.Visible = true;
                btnDownload.Visible = false;
            }
            else
            {
                MessageBox.Show("No Data Found", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

        }

        private DataTable GetdataDet(int chitDID)
        {
            DataTable dt = new DataTable();
            try
            {
                conn.Close();
                conn.Open();
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = conn;

                if (FormId == 1)
                {
                    cmd.CommandText = "SP_CHITMISDET";
                }
                else
                {
                    cmd.CommandText = "ChitMISMonthly";
                }
                if (txtChitName.Text != string.Empty && EditMode == false)
                {
                    cmd.Parameters.AddWithValue("@ChitId", chitDID);
                    //cmd.Parameters.AddWithValue("@Tag", 0);
                    //cmd.Parameters.AddWithValue("@chitDId", null);
                }
                else if (EditMode == true && FormId == 1)
                {
                    cmd.Parameters.AddWithValue("@CHITDID", chitDID);
                }
                else if (EditMode == true && FormId == 2)
                {
                    cmd.Parameters.AddWithValue("@ChitId", chidID);
                    cmd.Parameters.AddWithValue("@Tag", 1);
                    cmd.Parameters.AddWithValue("@chitDId", chitDID);
                }
                else
                {
                    cmd.Parameters.AddWithValue("@ChitId", 0);
                }
                cmd.CommandTimeout = 1200;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            return dt;
        }

        private void btnLoadImage_Click(object sender, EventArgs e)
        {
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "SP_GetPhotoById";
                cmd.Parameters.AddWithValue("@CustId", Convert.ToInt32(txtCustId.Text));
                cmd.Connection = connPhoto;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                if (dt.Rows[0]["CustIdentityProof"] != System.DBNull.Value)
                {
                    byte[] ImgID = (byte[])dt.Rows[0]["CustIdentityProof"];
                    MemoryStream ms = new MemoryStream(ImgID);
                    pickCustImage.Image = Image.FromStream(ms);
                    pickCustImage.SizeMode = PictureBoxSizeMode.StretchImage;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }
        private void GrBack_Enter(object sender, EventArgs e)
        {
        }
        private void BtnBack_Click(object sender, EventArgs e)
        {
            grZoom.Visible = false;
            GrBack.Visible = false;
            grFront.Visible = true;
            EditMode = false;
            Termination = false;
            txtChitNameSearch.Text = string.Empty;
            txtBarcodeSearch.Text = string.Empty;
            grChitSearch.Visible = false;
        }
        private void FrmDaily_Load(object sender, EventArgs e)
        {
            string UseName = FrmLogin.UserName;
            if (UseName == "bijoy")
            {
                btnProcessBaldate.Visible = true;
                btnUpdateBalDate.Visible = true;
            }
            else
            {
                btnProcessBaldate.Visible = false;
                btnUpdateBalDate.Visible = false;
            }
            grZoom.Visible = false;
            grChitSearch.Visible = false;
            FormId = Internet.FormID;

            if (FormId == 1)
            {
                lblTenureType.Text = "Daily";
                label9.Text = "Paid Days";
                label12.Visible = true;
                txtBalanceDays.Visible = true;
                tabDaily.TabPages.Remove(tabMonthly);
            }
            else
            {
                lblTenureType.Text = "Monthly";
                label9.Text = "Paid Months";
                label12.Visible = false;
                txtBalanceDays.Visible = false;
                tabDaily.TabPages.Remove(tabCollectionDetails);

            }
            GrBack.Visible = false;
            grFront.Visible = true;
            DataTable dt = Getdata();
            LoadChitByDid(dt);
        }
        protected void LoadChitByDid(DataTable dt)
        {
            try
            {
                DataGridDaily.DataSource = null;
                DataGridDaily.AutoGenerateColumns = false;
                DataGridDaily.ColumnCount = 15;

                DataGridDaily.Columns[0].Name = "SlNo";
                DataGridDaily.Columns[0].HeaderText = "SlNo";
                DataGridDaily.Columns[0].DataPropertyName = "SlNo";
                DataGridDaily.Columns[0].Width = 50;

                DataGridDaily.Columns[1].Name = "Customer Name";
                DataGridDaily.Columns[1].HeaderText = "Customer Name";
                DataGridDaily.Columns[1].DataPropertyName = "CustName";
                DataGridDaily.Columns[1].Width = 180;

                DataGridDaily.Columns[2].Name = "Balance Date";
                DataGridDaily.Columns[2].HeaderText = "Balance Date";
                DataGridDaily.Columns[2].DataPropertyName = "BalDate";
                DataGridDaily.Columns[2].DefaultCellStyle.Format = "dd-MM-yyy";

                if (FormId == 1)
                {
                    DataGridDaily.Columns[3].Name = "No.of Days Coll";
                    DataGridDaily.Columns[3].HeaderText = "No.of Days Coll";
                    DataGridDaily.Columns[3].DataPropertyName = "NoDays";
                }
                else
                {
                    DataGridDaily.Columns[3].Name = "PaidMonths";
                    DataGridDaily.Columns[3].HeaderText = "PaidMonths";
                    DataGridDaily.Columns[3].DataPropertyName = "PaidMonth";
                }
                DataGridDaily.Columns[3].DefaultCellStyle.Format = "N2";
                DataGridDaily.Columns[4].Name = "Over Due ";
                DataGridDaily.Columns[4].HeaderText = "Over Due";
                DataGridDaily.Columns[4].DataPropertyName = "Pendingdays";

                DataGridDaily.Columns[4].DefaultCellStyle.Format = "N2";
                DataGridDaily.Columns[5].Name = "Agent";
                DataGridDaily.Columns[5].HeaderText = "Agent";
                DataGridDaily.Columns[5].DataPropertyName = "AgentName";
                DataGridDaily.Columns[5].Width = 135;

                DataGridDaily.Columns[6].Name = "Contact Number";
                DataGridDaily.Columns[6].HeaderText = "Contact Number";
                DataGridDaily.Columns[6].DataPropertyName = "ContactNo";
                DataGridDaily.Columns[6].Width = 130;

                DataGridDaily.Columns[7].Name = "City";
                DataGridDaily.Columns[7].HeaderText = "City";
                DataGridDaily.Columns[7].DataPropertyName = "City";
                DataGridDaily.Columns[7].Width = 150;
                DataGridDaily.Columns[7].Visible = false;

                DataGridDaily.Columns[8].Name = "ChitDid";
                DataGridDaily.Columns[8].HeaderText = "ChitDid";
                DataGridDaily.Columns[8].DataPropertyName = "ChitDid";
                DataGridDaily.Columns[8].Visible = false;

                DataGridDaily.Columns[9].Name = "ChitAmt";
                DataGridDaily.Columns[9].HeaderText = "ChitAmt";
                DataGridDaily.Columns[9].DataPropertyName = "ChitAmt";
                DataGridDaily.Columns[9].Visible = false;

                DataGridDaily.Columns[10].Name = "AmtPchit";
                DataGridDaily.Columns[10].HeaderText = "AmtPchit";
                DataGridDaily.Columns[10].DataPropertyName = "AmtPchit";
                DataGridDaily.Columns[10].Visible = false;

                DataGridDaily.Columns[11].Name = "Tenuretype";
                DataGridDaily.Columns[11].HeaderText = "Tenuretype";
                DataGridDaily.Columns[11].DataPropertyName = "Tenuretype";
                DataGridDaily.Columns[11].Visible = false;

                DataGridDaily.Columns[12].Name = "Auction Carried";
                DataGridDaily.Columns[12].HeaderText = "Auction Carried";
                DataGridDaily.Columns[12].DataPropertyName = "AUC";
                DataGridDaily.Columns[12].Width = 70;

                DataGridDaily.Columns[13].Name = "Last Collection Date";
                DataGridDaily.Columns[13].HeaderText = "Last Collection Date";
                DataGridDaily.Columns[13].DataPropertyName = "LastColDate";
                DataGridDaily.Columns[13].DefaultCellStyle.Format = "dd-MM-yyyy";

                DataGridDaily.Columns[14].Name = "ChitTag";
                DataGridDaily.Columns[14].HeaderText = "ChitTag";
                DataGridDaily.Columns[14].DataPropertyName = "ChitTag";
                DataGridDaily.Columns[14].Visible = false;
                DataGridDaily.DataSource = dt;
                if (dt.Rows.Count != 0)
                {
                    txtComDate.Text = Convert.ToDateTime(dt.Rows[0]["ComDt"].ToString()).ToString("dd-MM-yyyy");
                    txtEnDt.Text = Convert.ToDateTime(dt.Rows[0]["EndDt"].ToString()).ToString("dd-MM-yyyy");
                    txtCmp.Text = dt.Rows[0]["CPrefix"].ToString();
                }
                if (FormId == 2)
                {
                    for (int i = 0; i < DataGridDaily.Rows.Count - 1; i++)
                    {
                        progressBar.Value = i;
                        DataGridDaily.BeginEdit(true);
                        decimal AmtChit = Convert.ToDecimal(DataGridDaily.Rows[i].Cells[10].Value.ToString());
                        if(txtDividend.Text == string.Empty)
                        {
                            txtDividend.Text = "0";
                        }
                        decimal TotalDvedent = Convert.ToInt32(txtDividend.Text);
                        decimal dividentdays = TotalDvedent / AmtChit;
                        //string Value = dividentdays.ToString();
                        //decimal nn = decimal.Parse(Value, CultureInfo.InvariantCulture);
                        decimal ActualPaid = Convert.ToDecimal(DataGridDaily.Rows[i].Cells[3].Value.ToString());
                        DataGridDaily.Rows[i].Cells[3].Value = ActualPaid + dividentdays;
                        progressBar.Update();
                    }
                }
                if (FormId == 2)
                {
                    CheckAuction();
                }
                CellChanged();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            finally
            {
                conn.Close();
            }
        }

        protected void LoadChit()
        {
            try
            {
                if (txtChitName.Text == string.Empty)
                {
                    conn.Open();
                    cmd = new SqlCommand();
                    cmd.Connection = conn;
                    cmd.CommandType = CommandType.StoredProcedure;
                    if (FormId == 1)
                    {
                        if(ChckCompletedChit.Checked == false)
                        {
                            cmd.CommandText = "SP_GetCurrentChit";
                        }
                        else
                        {
                            cmd.CommandText = "SP_GetCompletedChit";
                        }
                    }
                    else
                    {
                        if(ChckCompletedChit.Checked == true)
                        {
                            cmd.CommandText = "SP_GetCompleteChitMonthly";
                        }
                        else
                        {
                            cmd.CommandText = "SP_GetCurrentChitMonthly";
                        }
                    }

                    if (txtChitNameSearch.Text != string.Empty)
                    {
                        cmd.Parameters.AddWithValue("@Tag", 1);
                        cmd.Parameters.AddWithValue("@ChitName", txtChitNameSearch.Text);
                    }
                    else

                    {
                        cmd.Parameters.AddWithValue("@Tag", 0);
                        cmd.Parameters.AddWithValue("@ChitName", txtChitNameSearch.Text);
                    }

                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    DataTable dt = new DataTable();
                    da.Fill(dt);
                    bs.DataSource = dt;
                    DataGridChitSearch.DataSource = null;
                    DataGridChitSearch.AutoGenerateColumns = false;
                    DataGridChitSearch.ColumnCount = 3;

                    DataGridChitSearch.Columns[0].Name = "ChitName";
                    DataGridChitSearch.Columns[0].HeaderText = "ChitName";
                    DataGridChitSearch.Columns[0].DataPropertyName = "ChitName";
                    DataGridChitSearch.Columns[0].Width = 290;
                    DataGridChitSearch.Columns[1].Name = "ChitID";
                    DataGridChitSearch.Columns[1].HeaderText = "ChitID";
                    DataGridChitSearch.Columns[1].DataPropertyName = "ChitID";
                    DataGridChitSearch.Columns[1].Visible = false;
                    DataGridChitSearch.Columns[2].Name = "TenureType";
                    DataGridChitSearch.Columns[2].HeaderText = "TenureType";
                    DataGridChitSearch.Columns[2].DataPropertyName = "TenureType";
                    DataGridChitSearch.Columns[2].Visible = false;
                    DataGridChitSearch.DataSource = bs;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            finally
            {
                conn.Close();
                cmd.Dispose();
            }
        }

        private void txtChitName_TextChanged(object sender, EventArgs e)
        {
            //txtChitNameSearch.Text = string.Empty;
            grChitSearch.Visible = true;
            txtBarcodeSearch.Text = string.Empty;
            //LoadChit();
        }

        private void txtChitName_MouseClick(object sender, MouseEventArgs e)
        {
            txtChitNameSearch.Text = string.Empty;
            grChitSearch.Visible = true;
            LoadChit();
            txtChitNameSearch.Focus();
        }

        private void DataGridChitSearch_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            int index = e.RowIndex;
            txtChitName.Text = DataGridChitSearch.Rows[index].Cells[0].Value.ToString();
            chidID = (int)DataGridChitSearch.Rows[index].Cells[1].Value;
            if (DataGridDaily.Rows.Count != 0)
            {
                DataGridDaily.DataSource = null;
            }
            grChitSearch.Visible = false;
            txtChitNameSearch.Text = string.Empty;
            grProgress.Visible = true;
            progressBar.Visible = true;
            Thread thread = new Thread(loadTable);
            thread.Start();
        }
        private void loadTable()
        {
            // Load your Table...
            DataTable table = new DataTable();
            table = Getdata();
            GetDividendAmount(chidID);
            setDataSource(table);
        }
        internal delegate void SetDataSourceDelegate(DataTable table);
        private void setDataSource(DataTable table)
        {
            // Invoke method if required:
            if (this.InvokeRequired)
            {
                this.Invoke(new SetDataSourceDelegate(setDataSource), table);
            }
            else
            {
                LoadChitByDid(table);
                grProgress.Visible = false;
                progressBar.Visible = false;
            }
        }
        private void txtChitNameSearch_TextChanged(object sender, EventArgs e)
        {
            bs.Filter = string.Format("ChitName LIKE '%{0}%'", txtChitNameSearch.Text);
        }

        private void DataGridDaily_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            grZoom.Visible = false;
            int Index = DataGridDaily.SelectedCells[0].RowIndex;
            chitDID = (int)DataGridDaily.Rows[Index].Cells[8].Value;
            string Auction = DataGridDaily.Rows[Index].Cells[12].Value.ToString();
            string Tag = DataGridDaily.Rows[Index].Cells[14].Value.ToString();
            if (Auction == "N")
            {
                lblAuction.Text = "Auction Not Carried";
            }
            else
            {
                lblAuction.Text = "Auction Carried";
            }
            if (Tag == "C         ")
            {
                lblAuction.Text = "Canceled Chit";
            }
            EditMode = true;
            DataTable dt = GetdataDet(chitDID);
            LoadTextbox(dt);
            grFront.Visible = false;
            GrBack.Visible = true;
            btnDownload.Visible = false;
        }

        protected DataTable Getdata()
        {
            DataTable dt = new DataTable();
            try
            {
                conn.Close();
                conn.Open();
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = conn;

                if (FormId == 1)
                {
                    cmd.CommandText = "SP_ChitMIS";
                }
                else 
                {
                    cmd.CommandText = "ChitMISMonthly";
                }
                if (txtChitName.Text != string.Empty && EditMode == false)
                {
                    cmd.Parameters.AddWithValue("@ChitId", chidID);
                    //cmd.Parameters.AddWithValue("@Tag", 0);
                    //cmd.Parameters.AddWithValue("@chitDId", null);
                }
                else if (EditMode == true)
                {
                    cmd.Parameters.AddWithValue("@ChitId", chidID);
                    //cmd.Parameters.AddWithValue("@Tag", 1);
                    //cmd.Parameters.AddWithValue("@chitDId", chitDID);
                }
                else
                {
                    cmd.Parameters.AddWithValue("@ChitId", 0);
                }
                cmd.CommandTimeout = 1200;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            return dt;
        }

        public void LoadTextbox(DataTable dt)
        {
            try
            {
                double AmtPChit = Convert.ToDouble(dt.Rows[0]["AmtPChit"].ToString());
                DateTime ComDt = (DateTime)dt.Rows[0]["ComDt"];
                DateTime EndDt = (DateTime)dt.Rows[0]["EndDt"];
                int days = (DateTime.Now.Date - ComDt).Days;
                txtDuetobePaid.Text = (days * AmtPChit / 1).ToString(); //Due to be paid as on date
                txtChit.Text = dt.Rows[0]["ChitName"].ToString();
                cmbTicketNumber.Text = dt.Rows[0]["SlNo"].ToString() + " - " + dt.Rows[0]["CustName"].ToString();
                txtCompany.Text = dt.Rows[0]["CPrefix"].ToString();
                txtCustomer.Text = dt.Rows[0]["CustName"].ToString();
                txtAgent.Text = dt.Rows[0]["AgentCode"].ToString();
                txtFather.Text = dt.Rows[0]["FatherName"].ToString();
                txtBarcode.Text = dt.Rows[0]["Barcode"].ToString();
                txtAddress1.Text = dt.Rows[0]["Street"].ToString();
                txtaddress2.Text = dt.Rows[0]["City"].ToString();
                txtMobile.Text = dt.Rows[0]["Mobile"].ToString();
                txtCommenceDate.Text = ComDt.ToString("dd-MM-yyyy");
                txtEndDate.Text = EndDt.ToString("dd-MM-yyyy");
                txtAmountPerchit.Text = dt.Rows[0]["AmtPChit"].ToString();
                txtChitAmount.Text = dt.Rows[0]["ChitAmt"].ToString();
                txtCustId.Text = dt.Rows[0]["Custid"].ToString();
                txtChitDid.Text = dt.Rows[0]["ChitDid"].ToString();
                lblStDt.Text = Convert.ToDateTime(dt.Rows[0]["StartDt"].ToString()).ToString("dd/MM/yyyy");
                getCollection(chitDID);
                if (txtBarcodeSearch.Text == string.Empty)
                {
                    GetDividendAmount(chidID);
                }
                else
                {
                    GetDividendAmount(chidID);
                }
                if (FormId == 1)
                {
                    int index = DataGridDaily.SelectedCells[0].RowIndex;
                    decimal PaidDays = Convert.ToDecimal(DataGridDaily.Rows[index].Cells[3].Value.ToString());
                    txtpaidDays.Text = PaidDays.ToString("0.00");
                }
                else
                {
                    int index = DataGridDaily.SelectedCells[0].RowIndex;
                    decimal PaidMOnths = Convert.ToDecimal(DataGridDaily.Rows[index].Cells[3].Value.ToString());
                    txtpaidDays.Text = PaidMOnths.ToString("0.00");
                }
                if (txtDividentChit.Text == string.Empty)
                {
                    txtDividentChit.Text = "0";
                }
                int ChitAmount = Convert.ToInt32(txtChitAmount.Text);
                int ActaulDuePaid1 = Convert.ToInt32(txtActaulDuePaid.Text);
                int Dividend1 = Convert.ToInt32(txtDividentChit.Text);
                txtBalanceDue.Text = (ChitAmount - ActaulDuePaid1 - Dividend1).ToString();
                txtBalanceDays.Text = (Convert.ToDecimal(txtBalanceDue.Text) / Convert.ToDecimal(txtAmountPerchit.Text)).ToString("0.00");
                decimal bal = Convert.ToDecimal(txtBalanceDue.Text);
                if (bal <= 0)
                {
                    txtPendingDue.Text = "Completed";
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }
        protected void getCollection(int ID)
        {
            DataTable dt = new DataTable();
            try
            {
                if (EditMode == true)
                {
                    conn.Close();
                    conn.Open();

                    string Query = @"SELECT		M.CHITDID,M.CollDate,M.BalDate,M.Amount,
                                    DATEDIFF(D, C.ComDt, M.BalDate) +1 AS DAYS, C.AmtPChit* DATEDIFF(D, C.ComDt, M.BalDate) AS TOTALAMT
                                    FROM        MemTrans M
                                    INNER JOIN  ChitTrans CT ON M.ChitDid = CT.ChitDid AND CT.ChitTranTag = 'A'
                                    INNER JOIN  Chit C  ON C.ChitId = CT.Chitid
                                    WHERE M.ChitDid = " + ID + " ORDER BY CollDate DESC";
                    cmd = new SqlCommand(Query, conn);
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    da.Fill(dt);
                    if (FormId == 1)
                    {
                        DataGridCollectionDetails.DataSource = null;
                        DataGridCollectionDetails.AutoGenerateColumns = false;
                        DataGridCollectionDetails.ColumnCount = 7;
                        DataGridCollectionDetails.Columns[0].Name = "S.No";
                        DataGridCollectionDetails.Columns[0].HeaderText = "S.No";
                        DataGridCollectionDetails.Columns[0].Width = 60;
                        DataGridCollectionDetails.Columns[1].Name = "Collection Date";
                        DataGridCollectionDetails.Columns[1].HeaderText = "Collection Date";
                        DataGridCollectionDetails.Columns[1].DataPropertyName = "CollDate";
                        DataGridCollectionDetails.Columns[1].DefaultCellStyle.Format = "dd-MM-yyyy";
                        DataGridCollectionDetails.Columns[1].Width = 150;
                        DataGridCollectionDetails.Columns[2].Name = "Balance Date";
                        DataGridCollectionDetails.Columns[2].HeaderText = "Balance Date";
                        DataGridCollectionDetails.Columns[2].DataPropertyName = "BalDate";
                        DataGridCollectionDetails.Columns[2].DefaultCellStyle.Format = "dd-MM-yyyy"; 
                        DataGridCollectionDetails.Columns[2].Width = 120;
                        DataGridCollectionDetails.Columns[3].Name = "Amount";
                        DataGridCollectionDetails.Columns[3].HeaderText = "Amount";
                        DataGridCollectionDetails.Columns[3].DataPropertyName = "Amount";

                        DataGridCollectionDetails.Columns[4].Name = "DAYS";
                        DataGridCollectionDetails.Columns[4].HeaderText = "DAYS";
                        DataGridCollectionDetails.Columns[4].DataPropertyName = "DAYS";

                        DataGridCollectionDetails.Columns[5].Name = "TOTALAMT";
                        DataGridCollectionDetails.Columns[5].HeaderText = "TOTALAMT";
                        DataGridCollectionDetails.Columns[5].DataPropertyName = "TOTALAMT";
                        DataGridCollectionDetails.Columns[5].Visible = false;
                        DataGridCollectionDetails.Columns[6].Name = "Diff";
                        DataGridCollectionDetails.Columns[6].HeaderText = "Diff";
                        DataGridCollectionDetails.Columns[6].Visible = false;
                        DataGridCollectionDetails.DataSource = dt;
                        int sum = 0;
                        for (int i = 0; i < DataGridCollectionDetails.Rows.Count; i++)
                        {
                            DataGridCollectionDetails.Rows[i].Cells[0].Value = (i + 1).ToString();
                            sum += Convert.ToInt32(DataGridCollectionDetails.Rows[i].Cells[3].Value);
                        }
                        lblTotalAmount.Text = sum.ToString() + ".00";
                        txtActaulDuePaid.Text = sum.ToString();
                        int ActaulDuePaid = int.Parse(txtActaulDuePaid.Text);
                        int DuetobePaid = int.Parse(txtDuetobePaid.Text);
                        txtPendingDue.Text = (DuetobePaid - ActaulDuePaid).ToString();
                    }
                    else
                    {
                        dataGridMonthlyCollection.DataSource = null;
                        dataGridMonthlyCollection.AutoGenerateColumns = false;
                        dataGridMonthlyCollection.ColumnCount = 3;
                        dataGridMonthlyCollection.Columns[0].Name = "S.No";
                        dataGridMonthlyCollection.Columns[0].HeaderText = "S.No";
                        dataGridMonthlyCollection.Columns[0].Width = 50;

                        dataGridMonthlyCollection.Columns[1].Name = "Collection Date";
                        dataGridMonthlyCollection.Columns[1].HeaderText = "Collection Date";
                        dataGridMonthlyCollection.Columns[1].DataPropertyName = "CollDate";
                        dataGridMonthlyCollection.Columns[1].DefaultCellStyle.Format = "dd-MM-yyyy";
                        //dataGridMonthlyCollection.Columns[1].Width = 140;
                        dataGridMonthlyCollection.Columns[2].Name = "Amount";
                        dataGridMonthlyCollection.Columns[2].HeaderText = "Amount";
                        dataGridMonthlyCollection.Columns[2].DataPropertyName = "Amount";
                        dataGridMonthlyCollection.Columns[2].Width = 80;
                        dataGridMonthlyCollection.Columns[2].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                        dataGridMonthlyCollection.DataSource = dt;
                        int sum = 0;
                        for (int i = 0; i < dataGridMonthlyCollection.Rows.Count; i++)
                        {
                            dataGridMonthlyCollection.Rows[i].Cells[0].Value = (i + 1).ToString();
                            sum += Convert.ToInt32(dataGridMonthlyCollection.Rows[i].Cells[2].Value);
                        }
                        lblMonthlyTotal.Text = sum.ToString();
                        txtActaulDuePaid.Text = sum.ToString();
                        int ActaulDuePaid = int.Parse(txtActaulDuePaid.Text);
                        int DuetobePaid = int.Parse(txtDuetobePaid.Text);
                        txtPendingDue.Text = (DuetobePaid - ActaulDuePaid).ToString();
                        SQLDBHelper db = new SQLDBHelper();
                        DataSet ds = new DataSet();
                        SqlParameter[] para = { new SqlParameter("@ChitId", chidID), new SqlParameter("@ChitDID", ID) };
                        ds = db.GetDatasWithPara(CommandType.StoredProcedure, "SP_GetDetforMonth", para);
                        DataTable dtauction = new DataTable();
                        dtauction = ds.Tables[0];
                        DGVMonth.DataSource = null;
                        DGVMonth.AutoGenerateColumns = false;
                        DGVMonth.ColumnCount = 5;
                        DGVMonth.Columns[0].Name = "Month";
                        DGVMonth.Columns[0].HeaderText = "Month";
                        DGVMonth.Columns[0].Width = 50;
                        DGVMonth.Columns[1].Name = "DueAmount";
                        DGVMonth.Columns[1].HeaderText = "Due Amount";
                        DGVMonth.Columns[1].Width = 70;
                        DGVMonth.Columns[2].Name = "Divident";
                        DGVMonth.Columns[2].HeaderText = "Divident";
                        DGVMonth.Columns[2].Width = 70;
                        DGVMonth.Columns[3].Name = "DuePaid";
                        DGVMonth.Columns[3].HeaderText = "Due Paid";
                        DGVMonth.Columns[3].Width = 70;
                        DGVMonth.Columns[4].Name = "Pending";
                        DGVMonth.Columns[4].HeaderText = "Pending";
                        DGVMonth.Columns[4].Width = 70;
                        DGVMonth.Columns[0].DataPropertyName = "Month";
                        DGVMonth.Columns[1].DataPropertyName = "Due";
                        DGVMonth.Columns[2].DataPropertyName = "Divident";
                        DGVMonth.Columns[3].DataPropertyName = "DuePaid";
                        DGVMonth.Columns[4].DataPropertyName = "Pending";
                        DGVMonth.DataSource = dtauction;
                        lblduetobePaid.Text = Convert.ToInt32(dtauction.Compute("SUM(Due)", string.Empty)).ToString();
                        lblDividend.Text = Convert.ToInt32(dtauction.Compute("SUM(Divident)", string.Empty)).ToString();
                        lblDuePaid.Text = Convert.ToInt32(dtauction.Compute("SUM(DuePaid)", string.Empty)).ToString();
                        lblPendingDue.Text = Convert.ToInt32(dtauction.Compute("SUM(Pending)", string.Empty)).ToString();
                        txtPendingDue.Text = lblPendingDue.Text;

                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            finally
            {
                conn.Close();
            }
        }

        private void label42_Click(object sender, EventArgs e)
        {

        }

        private void tabPage2_Click(object sender, EventArgs e)
        {


        }
        public string GetLastAuctionNumber()
        {
            string AuctionNumver = string.Empty;
            try
            {
                conn.Close();
                string Query = "select max(AuctionNo) ANo from vw_Auction where chitid ='" + chidID + "'";
                conn.Open();
                cmd = new SqlCommand(Query, conn);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                AuctionNumver = dt.Rows[0]["ANo"].ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conn.Close();
                cmd.Dispose();
            }
            return AuctionNumver;
        }

        private void tabDaily_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                conn.Close();
                if (tabDaily.SelectedIndex == 0)
                {
                    if (lblTenureType.Text == "Daily")
                    {
                        getCollection(chitDID);
                    }

                    txtLastAuctionNo.Text = GetLastAuctionNumber();
                }
                if (tabDaily.SelectedIndex == 1)
                {
                    string Query = "select AuctionNo, AuctionDate, Discount, DividendPMem, AmtPaid, PaymentDt, PaymentDt2 from vw_Auction where barcode = '" + txtBarcode.Text + "'";
                    conn.Open();
                    cmd = new SqlCommand(Query, conn);
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    DataTable dt = new DataTable();
                    da.Fill(dt);
                    if (dt.Rows.Count != 0)
                    {
                        DateTime AuctionDate = (DateTime)dt.Rows[0]["AuctionDate"];
                        DateTime PaymentDt = (DateTime)dt.Rows[0]["PaymentDt"];
                        DateTime PaymentDt2 = (DateTime)dt.Rows[0]["PaymentDt2"];
                        txtAuctionNo.Text = dt.Rows[0]["AuctionNo"].ToString();
                        txtAuctionDate.Text = AuctionDate.ToString("dd/MMM/yyyy");
                        txtDixcountAmount.Text = dt.Rows[0]["Discount"].ToString();
                        txtDividendPerMember.Text = dt.Rows[0]["DividendPMem"].ToString();
                        txtAmountPaid.Text = dt.Rows[0]["AmtPaid"].ToString();
                        txtPaymentDate1.Text = PaymentDt.ToString("dd-MM-yyyy");
                        txtPaymentDate2.Text = PaymentDt2.ToString("dd-MMM-yyyy");
                    }
                    txtLastAuctionNo.Text = GetLastAuctionNumber();
                }
                else if (tabDaily.SelectedIndex == 2)
                {
                    string Query = "select distinct AuctionId,AuctionNo as ANo,AuctionDate,SlNo,CustCode,CustName,Discount,DividendPMem,Barcode,AuctionNo from VW_Auction where chitid ='" + chidID + "'  Order by AuctionNo,SlNo,CustName";
                    conn.Open();
                    cmd = new SqlCommand(Query, conn);
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    DataTable dt = new DataTable();
                    da.Fill(dt);
                    DataGridChitwiseAuction.DataSource = null;
                    DataGridChitwiseAuction.AutoGenerateColumns = false;
                    DataGridChitwiseAuction.ColumnCount = 7;
                    DataGridChitwiseAuction.Columns[0].Name = "AuctionNo";
                    DataGridChitwiseAuction.Columns[0].HeaderText = "AuctionNo";
                    DataGridChitwiseAuction.Columns[0].DataPropertyName = "ANo";
                    DataGridChitwiseAuction.Columns[0].Width = 60;

                    DataGridChitwiseAuction.Columns[1].Name = "AuctionDate";
                    DataGridChitwiseAuction.Columns[1].HeaderText = "AuctionDate";
                    DataGridChitwiseAuction.Columns[1].DataPropertyName = "AuctionDate";
                    DataGridChitwiseAuction.Columns[1].DefaultCellStyle.Format = "dd-MM-yyyy";

                    DataGridChitwiseAuction.Columns[2].Name = "Ticket No";
                    DataGridChitwiseAuction.Columns[2].HeaderText = "Ticket No";
                    DataGridChitwiseAuction.Columns[2].DataPropertyName = "SlNo";
                    DataGridChitwiseAuction.Columns[2].Width = 60;

                    DataGridChitwiseAuction.Columns[3].Name = "CustCode";
                    DataGridChitwiseAuction.Columns[3].HeaderText = "CustCode";
                    DataGridChitwiseAuction.Columns[3].DataPropertyName = "CustCode";
                    DataGridChitwiseAuction.Columns[3].Width = 60;

                    DataGridChitwiseAuction.Columns[4].Name = "CustomerName";
                    DataGridChitwiseAuction.Columns[4].HeaderText = "CustomerName";
                    DataGridChitwiseAuction.Columns[4].DataPropertyName = "CustName";

                    DataGridChitwiseAuction.Columns[5].Name = "Discount";
                    DataGridChitwiseAuction.Columns[5].HeaderText = "Discount";
                    DataGridChitwiseAuction.Columns[5].DataPropertyName = "Discount";
                    DataGridChitwiseAuction.Columns[5].Width = 60;

                    DataGridChitwiseAuction.Columns[6].Name = "DividendPerMem";
                    DataGridChitwiseAuction.Columns[6].HeaderText = "DividendPerMem";
                    DataGridChitwiseAuction.Columns[6].DataPropertyName = "DividendPMem";
                    DataGridChitwiseAuction.DataSource = dt;
                }
                else if (tabDaily.SelectedIndex == 3)
                {

                    string Query = "Select chitname Chit,SlNo,convert(varchar,max(CollDate),105) as  CollDate,convert(varchar,max(BalDate),105) as BalanceDate,convert(varchar,sum(amount)/AmtPChit,4)  PaidDays, sum(amount) PaidAmount from vw_memtrans where CustId ='" + txtCustId.Text + "' and ChitTag = 'A' group by  chitname,SlNo,AmtPChit";
                    conn.Open();
                    cmd = new SqlCommand(Query, conn);
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    DataTable dt = new DataTable();
                    da.Fill(dt);
                    DataGridOtherChitDetails.DataSource = null;
                    DataGridOtherChitDetails.AutoGenerateColumns = false;
                    DataGridOtherChitDetails.ColumnCount = 6;
                    DataGridOtherChitDetails.Columns[0].Name = "Chit Name";
                    DataGridOtherChitDetails.Columns[0].HeaderText = "Chit Name";
                    DataGridOtherChitDetails.Columns[0].DataPropertyName = "Chit";

                    DataGridOtherChitDetails.Columns[1].Name = "SlNo";
                    DataGridOtherChitDetails.Columns[1].HeaderText = "SlNo";
                    DataGridOtherChitDetails.Columns[1].DataPropertyName = "SlNo";

                    DataGridOtherChitDetails.Columns[2].Name = "CollDate";
                    DataGridOtherChitDetails.Columns[2].HeaderText = "CollDate";
                    DataGridOtherChitDetails.Columns[2].DataPropertyName = "CollDate";
                    DataGridOtherChitDetails.Columns[2].DefaultCellStyle.Format = "dd-MM-yyyy";

                    DataGridOtherChitDetails.Columns[3].Name = "BalanceDate";
                    DataGridOtherChitDetails.Columns[3].HeaderText = "BalanceDate";
                    DataGridOtherChitDetails.Columns[3].DataPropertyName = "BalanceDate";
                    DataGridOtherChitDetails.Columns[3].DefaultCellStyle.Format = "dd-MM-yyyy";

                    DataGridOtherChitDetails.Columns[4].Name = "Paid Days";
                    DataGridOtherChitDetails.Columns[4].HeaderText = "Paid Days";
                    DataGridOtherChitDetails.Columns[4].DataPropertyName = "PaidDays";

                    DataGridOtherChitDetails.Columns[5].Name = "Paid Amount";
                    DataGridOtherChitDetails.Columns[5].HeaderText = "Paid Amount";
                    DataGridOtherChitDetails.Columns[5].DataPropertyName = "PaidAmount";
                    DataGridOtherChitDetails.DataSource = dt;
                }
                else if (tabDaily.SelectedIndex == 4)
                {
                    cmd = new SqlCommand();
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "SP_GetTerminatoinDaily";
                    cmd.Parameters.AddWithValue("@ChitDid", chitDID);
                    cmd.Connection = conn;
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    DataTable dt = new DataTable();
                    da.Fill(dt);
                    cmbCustomer.DataSource = null;
                    cmbCustomer.DisplayMember = "CustName";
                    cmbCustomer.ValueMember = "Custid";
                    cmbCustomer.DataSource = dt;


                }
            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            finally
            {
                conn.Close();
                cmd.Dispose();
            }
        }

        private void cmbCustomer_SelectedIndexChanged(object sender, EventArgs e)
        {
            conn.Close();
            if (cmbCustomer.SelectedIndex != -1)
            {
                lblStartDate.Text = "-";
                LblEndDate.Text = "-";
                string ChitTerminationQuery = string.Empty;
                string Query1 = "select distinct ChitDid,Stdate,EnDate from chittrans where  Chitdid =" + chitDID + " and Custid =" + Convert.ToInt32(cmbCustomer.SelectedValue.ToString()) + "  Order by ChitDid desc";
                conn.Open();
                SqlCommand cmd1 = new SqlCommand(Query1, conn);
                SqlDataAdapter da1 = new SqlDataAdapter(cmd1);
                DataTable dt1 = new DataTable();
                da1.Fill(dt1);
                if (dt1.Rows.Count != 0)
                {
                    DateTime StartDate = (DateTime)dt1.Rows[0]["Stdate"];
                    if (dt1.Rows[0]["EnDate"].ToString() == string.Empty)
                    {
                        lblStartDate.Text = "-";
                    }
                    else
                    {
                        DateTime EndDate = (DateTime)dt1.Rows[0]["EnDate"];
                        LblEndDate.Text = EndDate.ToString("dd/MMM/yyyy");
                    }
                    lblStartDate.Text = StartDate.ToString("dd/MMM/yyyy");

                    if (cmbCustomer.SelectedIndex == 1)
                    {

                    }
                }

                if (cmbCustomer.SelectedIndex == cmbCustomer.Items.Count - 1)
                {
                    if (lblStartDate.Text == "-" && LblEndDate.Text == "-")
                    {
                        ChitTerminationQuery = "select distinct CollDate,BalDate,Amount,chitddid  from vw_MemTrans where Chitdid =" + chitDID + "  and Custid =" + Convert.ToInt32(cmbCustomer.SelectedValue.ToString()) + " Order by CollDate desc";
                    }
                    else if (lblStartDate.Text == "-")
                    {
                        ChitTerminationQuery = "select distinct CollDate,BalDate,Amount,chitddid from vw_MemTrans where Chitdid =" + chitDID + "  and CollDate <= '" + Convert.ToDateTime(LblEndDate.Text) + "'  and Custid =" + Convert.ToInt32(cmbCustomer.SelectedValue.ToString()) + "  Order by CollDate desc";
                    }
                    else if (LblEndDate.Text == "-")
                    {
                        ChitTerminationQuery = "select distinct CollDate,BalDate,Amount,chitddid from vw_MemTrans where Chitdid =" + chitDID + "  and CollDate >= '" + Convert.ToDateTime(lblStartDate.Text) + "' and Custid =" + Convert.ToInt32(cmbCustomer.SelectedValue.ToString()) + "  Order by CollDate desc";
                    }
                    else
                    {
                        ChitTerminationQuery = "select distinct CollDate,BalDate,Amount,chitddid from vw_MemTrans where Chitdid =" + chitDID + "  and CollDate <= '" + Convert.ToDateTime(LblEndDate.Text) + "' and Custid =" + Convert.ToInt32(cmbCustomer.SelectedValue.ToString()) + "  Order by CollDate desc";
                    }

                }
                else
                {
                    if (lblStartDate.Text == "-" && LblEndDate.Text == "-")
                    {
                        ChitTerminationQuery = "select distinct CollDate,BalDate,Amount,chitddid from vw_MemTrans where Chitdid =" + chitDID + " and Custid =" + Convert.ToInt32(cmbCustomer.SelectedValue.ToString()) + "  Order by CollDate desc";
                    }
                    else if (lblStartDate.Text == "-")
                    {
                        ChitTerminationQuery = "select distinct CollDate,BalDate,Amount,chitddid from vw_MemTrans where Chitdid =" + chitDID + " and CollDate <= '" + Convert.ToDateTime(LblEndDate.Text) + "' and Custid =" + Convert.ToInt32(cmbCustomer.SelectedValue.ToString()) + "  Order by CollDate desc";
                    }
                    else if (LblEndDate.Text == "-")
                    {
                        ChitTerminationQuery = "select distinct CollDate,BalDate,Amount,chitddid from vw_MemTrans where Chitdid =" + chitDID + "  and CollDate >= '" + Convert.ToDateTime(lblStartDate.Text) + "' and Custid =" + Convert.ToInt32(cmbCustomer.SelectedValue.ToString()) + "  Order by CollDate desc";
                    }
                    else
                    {
                        ChitTerminationQuery = "select distinct CollDate,BalDate,Amount,chitddid from vw_MemTrans where Chitdid =" + chitDID + " and CollDate between '" + Convert.ToDateTime(lblStartDate.Text) + "' and '" + Convert.ToDateTime(LblEndDate.Text) + "' and Custid =" + Convert.ToInt32(cmbCustomer.SelectedValue.ToString()) + "   Order by CollDate desc";
                    }
                }
                SqlCommand CmdTermination = new SqlCommand(ChitTerminationQuery, conn);
                SqlDataAdapter DaTermination = new SqlDataAdapter(CmdTermination);
                DataTable DtTermination = new DataTable();
                DaTermination.Fill(DtTermination);
                DataGridTerminationDetails.DataSource = null;
                DataGridTerminationDetails.AutoGenerateColumns = false;
                DataGridTerminationDetails.ColumnCount = 4;
                DataGridTerminationDetails.Columns[0].Name = "Collection Date";
                DataGridTerminationDetails.Columns[0].HeaderText = "Collection Date";
                DataGridTerminationDetails.Columns[0].DataPropertyName = "CollDate";
                DataGridTerminationDetails.Columns[0].DefaultCellStyle.Format = "dd-MM-yyyy";
                DataGridTerminationDetails.Columns[0].Width = 200;

                DataGridTerminationDetails.Columns[1].Name = "Balance Date";
                DataGridTerminationDetails.Columns[1].HeaderText = "Balance date";
                DataGridTerminationDetails.Columns[1].DataPropertyName = "BalDate";
                DataGridTerminationDetails.Columns[1].DefaultCellStyle.Format = "dd-MM-yyyy";
                DataGridTerminationDetails.Columns[1].Width = 200;

                DataGridTerminationDetails.Columns[2].Name = "Amount";
                DataGridTerminationDetails.Columns[2].HeaderText = "Amount";
                DataGridTerminationDetails.Columns[2].DataPropertyName = "Amount";
                DataGridTerminationDetails.Columns[2].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

                DataGridTerminationDetails.Columns[3].Name = "Chitddid";
                DataGridTerminationDetails.Columns[3].HeaderText = "Chitddid";
                DataGridTerminationDetails.Columns[3].DataPropertyName = "Chitddid";
                DataGridTerminationDetails.Columns[3].Visible = false;
                DataGridTerminationDetails.DataSource = DtTermination;
            }
        }
        protected void GetDividendAmount(int ChitId)
        {
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = conn;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "SP_GetDvidendAmount";
                cmd.Parameters.AddWithValue("@ChitID", ChitId);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                TextBox.CheckForIllegalCrossThreadCalls = false;
                txtDividentChit.Text = dt.Rows[0]["Discount"].ToString();
                txtDividend.Text = dt.Rows[0]["Discount"].ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }
        private void txtChitName_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                grChitSearch.Visible = false;
            }
            else if (e.KeyCode == Keys.Enter)
            {
                int index = DataGridChitSearch.SelectedCells[0].RowIndex;
                txtChitName.Text = DataGridChitSearch.Rows[index].Cells[0].Value.ToString();
                chidID = (int)DataGridChitSearch.Rows[index].Cells[1].Value;
                if (DataGridDaily.Rows.Count != 0)
                {
                    DataGridDaily.DataSource = null;
                }
                grChitSearch.Visible = false;
                txtChitNameSearch.Text = string.Empty;
                grProgress.Visible = true;
                progressBar.Visible = true;
                Thread thread = new Thread(loadTable);
                thread.Start();
            }
        }

        private void txtBarcodeSearch_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    Barcode = txtBarcodeSearch.Text;
                    DataTable dt = new DataTable();
                    dt = GetdataByBarcode();
                    chidID = Convert.ToInt32(dt.Rows[0]["ChitId"].ToString());
                    LoadChitByDid(dt);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return;
            }
            finally
            {
                conn.Close();
            }
        }
        protected void CheckAuction()
        {
            try
            {
                for (int i = 0; i < DataGridDaily.Rows.Count; i++)
                {
                    progressBar.Value = 1;
                    string Query = "select * from Auction Where ChitDId =" + DataGridDaily.Rows[i].Cells[8].Value.ToString() + "";
                    SqlCommand cmd = new SqlCommand(Query, conn);
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    DataTable dt = new DataTable();
                    da.Fill(dt);
                    if (dt.Rows.Count == 0)
                    {
                        DataGridDaily.Rows[i].Cells[12].Value = "N";
                    }
                    else
                    {
                        DataGridDaily.Rows[i].Cells[12].Value = "Y";
                    }
                    progressBar.Update();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }

        protected void CellChanged()
        {
            foreach (DataGridViewRow Myrow in DataGridDaily.Rows)
            {
                string Auction = Myrow.Cells[12].Value.ToString();
                string Tag = Myrow.Cells[14].Value.ToString();
                if (Auction == "Y")// Or your condition 
                {
                    Myrow.DefaultCellStyle.BackColor = Color.LightGreen;
                }
                else
                {
                    Myrow.DefaultCellStyle.BackColor = Color.LightBlue;
                }
                if (Tag == "C         ")
                {
                    Myrow.DefaultCellStyle.BackColor = Color.IndianRed;
                }
            }
        }

        private void DataGridDaily_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
        }

        private void btnDownload_Click(object sender, EventArgs e)
        {
            try
            {
                Microsoft.Office.Interop.Excel._Application excel = new Microsoft.Office.Interop.Excel.Application();
                Microsoft.Office.Interop.Excel._Workbook workbook = excel.Workbooks.Add(Type.Missing);
                Microsoft.Office.Interop.Excel._Worksheet worksheet = null;

                try
                {
                    worksheet = workbook.ActiveSheet;
                    worksheet.Name = txtChitName.Text;
                    int cellRowIndex = 1;
                    int cellColumnIndex = 1;
                    for (int j = 0; j < DataGridDaily.Columns.Count; j++)
                    {
                        if (DataGridDaily.Columns[j].Visible == true)
                        {
                            // Excel index starts from 1,1. As first Row would have the Column headers, adding a condition check. 
                            if (cellRowIndex == 1)
                            {
                                worksheet.Cells[cellRowIndex, cellColumnIndex] = DataGridDaily.Columns[j].HeaderText;
                                worksheet.Cells[cellRowIndex, cellColumnIndex].Font.FontStyle = FontStyle.Bold;
                                //worksheet.Cells[cellRowIndex, cellColumnIndex] = DataGridDaily.Rows[i].Cells[j].Value.ToString();
                            }
                            cellColumnIndex++;
                        }
                    }
                    cellColumnIndex = 1;
                    cellRowIndex++;
                    //Loop through each row and read value from each column. 
                    for (int i = 0; i < DataGridDaily.Rows.Count; i++)
                    {
                        for (int j = 0; j < DataGridDaily.Columns.Count; j++)
                        {
                            if (DataGridDaily.Columns[j].Visible == true)
                            {
                                worksheet.Cells[cellRowIndex, cellColumnIndex] = DataGridDaily.Rows[i].Cells[j].Value.ToString();
                                cellColumnIndex++;
                            }
                        }
                        cellColumnIndex = 1;
                        cellRowIndex++;
                    }

                    //Getting the location and file name of the excel to save from user. 
                    SaveFileDialog saveDialog = new SaveFileDialog();
                    saveDialog.Filter = "Excel files (*.xlsx)|*.xlsx|All files (*.*)|*.*";
                    saveDialog.FilterIndex = 2;

                    if (saveDialog.ShowDialog() == DialogResult.OK)
                    {
                        workbook.SaveAs(saveDialog.FileName);
                        MessageBox.Show("Export Successful", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                excel.Quit();
                workbook = null;
                excel = null;
            }
            finally
            {

            }
        }
        private void ExportToExcel()
        {
            // Creating a Excel object. 
            Microsoft.Office.Interop.Excel._Application excel = new Microsoft.Office.Interop.Excel.Application();
            Microsoft.Office.Interop.Excel._Workbook workbook = excel.Workbooks.Add(Type.Missing);
            Microsoft.Office.Interop.Excel._Worksheet worksheet = null;

            try
            {

                worksheet = workbook.ActiveSheet;

                worksheet.Name = "ExportedFromDatGrid";

                int cellRowIndex = 1;
                int cellColumnIndex = 1;
                //Loop through each row and read value from each column. 
                for (int i = 0; i < DataGridDaily.Rows.Count; i++)
                {
                    for (int j = 0; j < DataGridDaily.Columns.Count; j++)
                    {
                        // Excel index starts from 1,1. As first Row would have the Column headers, adding a condition check. 
                        if (cellRowIndex == 1)
                        {
                            worksheet.Cells[cellRowIndex, cellColumnIndex] = DataGridDaily.Columns[j].HeaderText;
                        }
                        else
                        {
                            worksheet.Cells[cellRowIndex, cellColumnIndex] = DataGridDaily.Rows[i].Cells[j].Value.ToString();
                        }
                        cellColumnIndex++;
                    }
                    cellColumnIndex = 1;
                    cellRowIndex++;
                }
                //Getting the location and file name of the excel to save from user. 
                SaveFileDialog saveDialog = new SaveFileDialog();
                saveDialog.Filter = "Excel files (*.xlsx)|*.xlsx|All files (*.*)|*.*";
                saveDialog.FilterIndex = 2;

                if (saveDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    workbook.SaveAs(saveDialog.FileName);
                    MessageBox.Show("Export Successful");
                }
            }
            catch (System.Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                excel.Quit();
                workbook = null;
                excel = null;
            }

        }

        private void btnLoadAddress_Click(object sender, EventArgs e)
        {
            try
            {
                SqlCommand cmd = new SqlCommand
                {
                    CommandType = CommandType.StoredProcedure,
                    CommandText = "SP_GetPhotoById"
                };
                cmd.Parameters.AddWithValue("@CustId", Convert.ToInt32(txtCustId.Text));
                cmd.Connection = connPhoto;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                if (dt.Rows[0]["CustAddressProof"] != System.DBNull.Value)
                {
                    byte[] ImgID = (byte[])dt.Rows[0]["CustAddressProof"];
                    MemoryStream ms = new MemoryStream(ImgID);
                    pickCustImage.Image = Image.FromStream(ms);
                    pickCustImage.SizeMode = PictureBoxSizeMode.StretchImage;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }

        private void btnLoadSignature_Click(object sender, EventArgs e)
        {
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "SP_GetPhotoById";
                cmd.Parameters.AddWithValue("@CustId", Convert.ToInt32(txtCustId.Text));
                cmd.Connection = connPhoto;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                if (dt.Rows[0]["CustSignatureProof"] != System.DBNull.Value)
                {
                    byte[] ImgID = (byte[])dt.Rows[0]["CustSignatureProof"];
                    MemoryStream ms = new MemoryStream(ImgID);
                    pickCustImage.Image = Image.FromStream(ms);
                    pickCustImage.SizeMode = PictureBoxSizeMode.StretchImage;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }

        private void btnZoom_Click(object sender, EventArgs e)
        {
            grZoom.Visible = true;
            imagePanel1.Image = (Bitmap)pickCustImage.Image;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            grZoom.Visible = false;
        }

        private void trackScroolBar_Scroll(object sender, EventArgs e)
        {
            imagePanel1.Zoom = trackScroolBar.Value * 0.20f;
        }

        private void txtBarcodeSearch_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                if (e.KeyChar == (char)Keys.Enter)
                {
                    Barcode = txtBarcodeSearch.Text;
                    DataTable dt = new DataTable();
                    dt = GetdataByBarcode();
                    chidID = Convert.ToInt32(dt.Rows[0]["ChitId"].ToString());
                    LoadChitByDid(dt);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }
        protected DataTable GetdataByBarcode()
        {
            conn.Close();
            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Connection = conn;
            cmd.CommandText = "ChitMISByBarcode";
            cmd.Parameters.AddWithValue("@Barcode", Barcode);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            return dt;
        }

        private void btnRefersh_Click(object sender, EventArgs e)
        {
            txtBarcodeSearch.Text = string.Empty;
            txtChitNameSearch.Text = string.Empty;
            DataGridDaily.Rows.Clear();
            DataGridDaily.Refresh();
        }
        private void DataGridDaily_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
        private void Btnexit_Click_1(object sender, EventArgs e)
        {
            this.Close();
        }

        private void DataGridDaily_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {

            }
        }

        private void DataGridDaily_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {

            }
        }

        private void btnExport_Click(object sender, EventArgs e)
        {
            try
            {
                ExportData();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Infromation", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        protected void ExportData()
        {
            Microsoft.Office.Interop.Excel.Application xlApp = new Microsoft.Office.Interop.Excel.Application();
            Microsoft.Office.Interop.Excel.Workbook xlWb = xlApp.Workbooks.Open(Application.StartupPath + "/Collection.xls", ReadOnly: false, Editable: true) as Microsoft.Office.Interop.Excel.Workbook;
            xlApp.DisplayAlerts = false;
            xlWb.SaveAs(Application.StartupPath + "/Collection1.xls", Microsoft.Office.Interop.Excel.XlFileFormat.xlWorkbookDefault, Type.Missing, Type.Missing, true, false, Microsoft.Office.Interop.Excel.XlSaveAsAccessMode.xlNoChange, Microsoft.Office.Interop.Excel.XlSaveConflictResolution.xlLocalSessionChanges, Type.Missing, Type.Missing);
            Microsoft.Office.Interop.Excel.Workbook xlWbN = xlApp.Workbooks.Open(Application.StartupPath + "/Collection1.xls", ReadOnly: false, Editable: true) as Microsoft.Office.Interop.Excel.Workbook;
            Microsoft.Office.Interop.Excel.Worksheet xlSht = xlWbN.Sheets[1];
            xlApp.Visible = true;

            Microsoft.Office.Interop.Excel.Range row1 = xlSht.Rows.Cells[1, 1];
            Microsoft.Office.Interop.Excel.Range row2 = xlSht.Rows.Cells[3, 3];
            Microsoft.Office.Interop.Excel.Range row3 = xlSht.Rows.Cells[4, 3];
            Microsoft.Office.Interop.Excel.Range row4 = xlSht.Rows.Cells[5, 3];
            Microsoft.Office.Interop.Excel.Range row5 = xlSht.Rows.Cells[6, 3];
            Microsoft.Office.Interop.Excel.Range row6 = xlSht.Rows.Cells[7, 3];
            Microsoft.Office.Interop.Excel.Range row7 = xlSht.Rows.Cells[8, 3];
            Microsoft.Office.Interop.Excel.Range row8 = xlSht.Rows.Cells[9, 3];
            Microsoft.Office.Interop.Excel.Range row9 = xlSht.Rows.Cells[3, 7];
            Microsoft.Office.Interop.Excel.Range row10 = xlSht.Rows.Cells[4, 7];
            Microsoft.Office.Interop.Excel.Range row11 = xlSht.Rows.Cells[5, 7];
            Microsoft.Office.Interop.Excel.Range row12 = xlSht.Rows.Cells[6, 7];
            Microsoft.Office.Interop.Excel.Range row13 = xlSht.Rows.Cells[7, 7];
            Microsoft.Office.Interop.Excel.Range row14 = xlSht.Rows.Cells[8, 7];
            Microsoft.Office.Interop.Excel.Range row15 = xlSht.Rows.Cells[9, 7];
            if (txtCompany.Text == "GTLC")
            {
                row1.Value = "GOPALAPURAM THANGALAKSHMI CHITS (P) LTD";
            }
            else
            {
                row1.Value = "THANGALAKSHMI CHIT FUNDS (P) LTD";
            }
            row2.Value = txtCustomer.Text;
            row3.Value = txtChitName.Text;
            row4.Value = txtChitAmount.Text;
            row5.Value = txtActaulDuePaid.Text;
            row6.Value = txtPendingDue.Text;
            row7.Value = txtpaidDays.Text;
            row8.Value = txtDividentChit.Text;
            row9.Value = cmbTicketNumber.Text;
            row10.Value = lblStDt.Text;
            row11.Value = txtAmountPerchit.Text;
            row12.Value = txtBalanceDue.Text;
            row13.Value = DateTime.Now.Date;
            row14.Value = txtBalanceDue.Text;
            row15.Value = txtBalanceDays.Text;
            int j = 12;
            DataTable dt = new DataTable();
            dt.Columns.Add("SlNo", typeof(int));
            dt.Columns.Add("CollDate", typeof(DateTime));
            dt.Columns.Add("BalDate", typeof(DateTime));
            dt.Columns.Add("Amount", typeof(decimal));
            if (FormId == 1)
            {
                for (int i = 0; i < DataGridCollectionDetails.Rows.Count; i++)
                {
                    DataRow row = dt.NewRow();
                    row[0] = DataGridCollectionDetails.Rows[i].Cells[0].Value.ToString();
                    row[1] = DataGridCollectionDetails.Rows[i].Cells[1].Value.ToString();
                    row[2] = DataGridCollectionDetails.Rows[i].Cells[2].Value.ToString();
                    row[3] = DataGridCollectionDetails.Rows[i].Cells[3].Value.ToString();
                    dt.Rows.Add(row);
                }
            }
            else
            {
                for (int i = 0; i < dataGridMonthlyCollection.Rows.Count; i++)
                {
                    DataRow row = dt.NewRow();
                    row[0] = dataGridMonthlyCollection.Rows[i].Cells[0].Value.ToString();
                    row[1] = dataGridMonthlyCollection.Rows[i].Cells[1].Value.ToString();
                    row[2] = DBNull.Value;
                    row[3] = dataGridMonthlyCollection.Rows[i].Cells[2].Value.ToString();
                    dt.Rows.Add(row);
                }
            }

            if (FormId == 1)
            {
                DataTable reversedDt = dt.Clone();
                for (var row = dt.Rows.Count - 1; row >= 0; row--)
                {
                    reversedDt.ImportRow(dt.Rows[row]);
                }
                DataView view = reversedDt.DefaultView;
                view.Sort = "CollDate ASC";
                DataTable sortedDate = view.ToTable();
                for (int i = 0; i < DataGridCollectionDetails.Rows.Count; i++)
                {
                    Microsoft.Office.Interop.Excel.Range k = xlSht.Rows.Cells[j, 2];
                    Microsoft.Office.Interop.Excel.Range L = xlSht.Rows.Cells[j, 3];
                    Microsoft.Office.Interop.Excel.Range M = xlSht.Rows.Cells[j, 4];
                    Microsoft.Office.Interop.Excel.Range N = xlSht.Rows.Cells[j, 5];
                    k.Value = i + 1;
                    L.Value = DataGridCollectionDetails.Rows[i].Cells[1].Value.ToString();
                    M.Value = DataGridCollectionDetails.Rows[i].Cells[2].Value.ToString();
                    N.Value = DataGridCollectionDetails.Rows[i].Cells[3].Value.ToString();
                    j += 1;
                }
            }
            else
            {
                DataTable reversedDt = dt.Clone();
                for (var row = dt.Rows.Count - 1; row >= 0; row--)
                {
                    reversedDt.ImportRow(dt.Rows[row]);
                }
                DataView view = reversedDt.DefaultView;
                view.Sort = "CollDate ASC";
                DataTable sortedDate = view.ToTable();
                for (int i = 0; i < dataGridMonthlyCollection.Rows.Count; i++)
                {
                    Microsoft.Office.Interop.Excel.Range k = xlSht.Rows.Cells[j, 2];
                    Microsoft.Office.Interop.Excel.Range L = xlSht.Rows.Cells[j, 3];
                    Microsoft.Office.Interop.Excel.Range M = xlSht.Rows.Cells[j, 4];
                    Microsoft.Office.Interop.Excel.Range N = xlSht.Rows.Cells[j, 5];
                    k.Value = i + 1;
                    L.Value = dataGridMonthlyCollection.Rows[i].Cells[1].Value.ToString();
                    M.Value = DBNull.Value;
                    N.Value = dataGridMonthlyCollection.Rows[i].Cells[2].Value.ToString();
                    j += 1;
                }
            }
            MessageBox.Show("Exported Successfully", "Infromation", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void btnProcessBaldate_Click(object sender, EventArgs e)
        {
            try
            {
                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Infromation", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }
    }
}