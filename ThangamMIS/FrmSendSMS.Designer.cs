﻿namespace ThangamMIS
{
    partial class FrmSendSMS
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.DataGridChit = new System.Windows.Forms.DataGridView();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.grChitSearch = new System.Windows.Forms.GroupBox();
            this.txtChitNameSearch = new System.Windows.Forms.TextBox();
            this.DataGridChitSearch = new System.Windows.Forms.DataGridView();
            this.BtnCustomer = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.txtChitName = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.DataGridCustomer = new System.Windows.Forms.DataGridView();
            this.Btngetchits = new System.Windows.Forms.Button();
            this.DtpSMSDate = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.BtnSendSMS = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.grProgress = new System.Windows.Forms.GroupBox();
            this.progressBar = new CircularProgressBar.CircularProgressBar();
            this.button2 = new System.Windows.Forms.Button();
            this.ChckSelectAll = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridChit)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.grChitSearch.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridChitSearch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridCustomer)).BeginInit();
            this.grProgress.SuspendLayout();
            this.SuspendLayout();
            // 
            // DataGridChit
            // 
            this.DataGridChit.AllowUserToAddRows = false;
            this.DataGridChit.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.DeepSkyBlue;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DataGridChit.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.DataGridChit.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridChit.EnableHeadersVisualStyles = false;
            this.DataGridChit.Location = new System.Drawing.Point(6, 49);
            this.DataGridChit.Name = "DataGridChit";
            this.DataGridChit.RowHeadersVisible = false;
            this.DataGridChit.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DataGridChit.Size = new System.Drawing.Size(399, 382);
            this.DataGridChit.TabIndex = 0;
            this.DataGridChit.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.DataGridChitSearch_CellMouseDoubleClick);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.grChitSearch);
            this.groupBox1.Controls.Add(this.BtnCustomer);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.txtChitName);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.DataGridCustomer);
            this.groupBox1.Controls.Add(this.Btngetchits);
            this.groupBox1.Controls.Add(this.DataGridChit);
            this.groupBox1.Controls.Add(this.DtpSMSDate);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(963, 446);
            this.groupBox1.TabIndex = 21;
            this.groupBox1.TabStop = false;
            this.groupBox1.Enter += new System.EventHandler(this.groupBox1_Enter);
            // 
            // grChitSearch
            // 
            this.grChitSearch.Controls.Add(this.txtChitNameSearch);
            this.grChitSearch.Controls.Add(this.DataGridChitSearch);
            this.grChitSearch.Location = new System.Drawing.Point(514, 48);
            this.grChitSearch.Name = "grChitSearch";
            this.grChitSearch.Size = new System.Drawing.Size(331, 262);
            this.grChitSearch.TabIndex = 45;
            this.grChitSearch.TabStop = false;
            this.grChitSearch.Text = "Search";
            this.grChitSearch.Visible = false;
            // 
            // txtChitNameSearch
            // 
            this.txtChitNameSearch.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtChitNameSearch.Location = new System.Drawing.Point(6, 12);
            this.txtChitNameSearch.Name = "txtChitNameSearch";
            this.txtChitNameSearch.Size = new System.Drawing.Size(312, 26);
            this.txtChitNameSearch.TabIndex = 18;
            this.txtChitNameSearch.TextChanged += new System.EventHandler(this.TxtChitNameSearch_TextChanged_1);
            // 
            // DataGridChitSearch
            // 
            this.DataGridChitSearch.AllowUserToAddRows = false;
            this.DataGridChitSearch.BackgroundColor = System.Drawing.SystemColors.ControlDark;
            this.DataGridChitSearch.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridChitSearch.Location = new System.Drawing.Point(6, 37);
            this.DataGridChitSearch.Name = "DataGridChitSearch";
            this.DataGridChitSearch.ReadOnly = true;
            this.DataGridChitSearch.RowHeadersVisible = false;
            this.DataGridChitSearch.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DataGridChitSearch.Size = new System.Drawing.Size(312, 211);
            this.DataGridChitSearch.TabIndex = 0;
            this.DataGridChitSearch.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.DataGridChitSearch_CellMouseDoubleClick_1);
            // 
            // BtnCustomer
            // 
            this.BtnCustomer.Location = new System.Drawing.Point(776, 19);
            this.BtnCustomer.Name = "BtnCustomer";
            this.BtnCustomer.Size = new System.Drawing.Size(106, 27);
            this.BtnCustomer.TabIndex = 45;
            this.BtnCustomer.Text = "Get Customer";
            this.BtnCustomer.UseVisualStyleBackColor = true;
            this.BtnCustomer.Click += new System.EventHandler(this.Button2_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(435, 23);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(73, 18);
            this.label3.TabIndex = 44;
            this.label3.Text = "Chit Name";
            // 
            // txtChitName
            // 
            this.txtChitName.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtChitName.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtChitName.Location = new System.Drawing.Point(514, 20);
            this.txtChitName.Name = "txtChitName";
            this.txtChitName.Size = new System.Drawing.Size(218, 26);
            this.txtChitName.TabIndex = 43;
            this.txtChitName.Click += new System.EventHandler(this.txtChitName_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(6, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(55, 18);
            this.label1.TabIndex = 42;
            this.label1.Text = "Auction";
            // 
            // DataGridCustomer
            // 
            this.DataGridCustomer.AllowUserToAddRows = false;
            this.DataGridCustomer.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.DeepSkyBlue;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DataGridCustomer.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.DataGridCustomer.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridCustomer.EnableHeadersVisualStyles = false;
            this.DataGridCustomer.Location = new System.Drawing.Point(410, 49);
            this.DataGridCustomer.Name = "DataGridCustomer";
            this.DataGridCustomer.RowHeadersVisible = false;
            this.DataGridCustomer.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DataGridCustomer.Size = new System.Drawing.Size(547, 382);
            this.DataGridCustomer.TabIndex = 41;
            // 
            // Btngetchits
            // 
            this.Btngetchits.Location = new System.Drawing.Point(258, 19);
            this.Btngetchits.Name = "Btngetchits";
            this.Btngetchits.Size = new System.Drawing.Size(76, 27);
            this.Btngetchits.TabIndex = 39;
            this.Btngetchits.Text = "Get Chit";
            this.Btngetchits.UseVisualStyleBackColor = true;
            this.Btngetchits.Click += new System.EventHandler(this.Btngetchits_Click);
            // 
            // DtpSMSDate
            // 
            this.DtpSMSDate.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DtpSMSDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.DtpSMSDate.Location = new System.Drawing.Point(135, 19);
            this.DtpSMSDate.Name = "DtpSMSDate";
            this.DtpSMSDate.Size = new System.Drawing.Size(120, 26);
            this.DtpSMSDate.TabIndex = 38;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(90, 23);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(37, 18);
            this.label2.TabIndex = 37;
            this.label2.Text = "Date";
            // 
            // BtnSendSMS
            // 
            this.BtnSendSMS.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSendSMS.Location = new System.Drawing.Point(869, 462);
            this.BtnSendSMS.Name = "BtnSendSMS";
            this.BtnSendSMS.Size = new System.Drawing.Size(106, 30);
            this.BtnSendSMS.TabIndex = 36;
            this.BtnSendSMS.Text = "Send SMS";
            this.BtnSendSMS.UseVisualStyleBackColor = true;
            this.BtnSendSMS.Click += new System.EventHandler(this.BtnSendSMS_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(12, 464);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(76, 27);
            this.button1.TabIndex = 40;
            this.button1.Text = "Exit";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // grProgress
            // 
            this.grProgress.BackColor = System.Drawing.Color.White;
            this.grProgress.Controls.Add(this.progressBar);
            this.grProgress.Location = new System.Drawing.Point(360, 152);
            this.grProgress.Name = "grProgress";
            this.grProgress.Size = new System.Drawing.Size(192, 196);
            this.grProgress.TabIndex = 41;
            this.grProgress.TabStop = false;
            this.grProgress.Visible = false;
            // 
            // progressBar
            // 
            this.progressBar.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.progressBar.AnimationFunction = WinFormAnimation.KnownAnimationFunctions.Liner;
            this.progressBar.AnimationSpeed = 500;
            this.progressBar.BackColor = System.Drawing.Color.White;
            this.progressBar.Font = new System.Drawing.Font("Arial", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.progressBar.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.progressBar.InnerColor = System.Drawing.Color.White;
            this.progressBar.InnerMargin = 5;
            this.progressBar.InnerWidth = 5;
            this.progressBar.Location = new System.Drawing.Point(6, 8);
            this.progressBar.MarqueeAnimationSpeed = 2000;
            this.progressBar.Name = "progressBar";
            this.progressBar.OuterColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(73)))), ((int)(((byte)(94)))));
            this.progressBar.OuterMargin = -11;
            this.progressBar.OuterWidth = 10;
            this.progressBar.ProgressColor = System.Drawing.Color.FromArgb(((int)(((byte)(194)))), ((int)(((byte)(251)))), ((int)(((byte)(50)))));
            this.progressBar.ProgressWidth = 10;
            this.progressBar.SecondaryFont = new System.Drawing.Font("Microsoft Sans Serif", 4.125F);
            this.progressBar.Size = new System.Drawing.Size(180, 180);
            this.progressBar.StartAngle = 270;
            this.progressBar.Style = System.Windows.Forms.ProgressBarStyle.Marquee;
            this.progressBar.SubscriptColor = System.Drawing.Color.Silver;
            this.progressBar.SubscriptMargin = new System.Windows.Forms.Padding(0);
            this.progressBar.SubscriptText = "";
            this.progressBar.SuperscriptColor = System.Drawing.Color.Silver;
            this.progressBar.SuperscriptMargin = new System.Windows.Forms.Padding(0);
            this.progressBar.SuperscriptText = "";
            this.progressBar.TabIndex = 10;
            this.progressBar.Text = "Please Wait";
            this.progressBar.TextMargin = new System.Windows.Forms.Padding(0, 2, 0, 0);
            this.progressBar.Value = 80;
            // 
            // button2
            // 
            this.button2.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.Location = new System.Drawing.Point(94, 462);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(106, 30);
            this.button2.TabIndex = 42;
            this.button2.Text = "Send Test SMS";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.Button2_Click_1);
            // 
            // ChckSelectAll
            // 
            this.ChckSelectAll.AutoSize = true;
            this.ChckSelectAll.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChckSelectAll.Location = new System.Drawing.Point(422, 462);
            this.ChckSelectAll.Name = "ChckSelectAll";
            this.ChckSelectAll.Size = new System.Drawing.Size(107, 22);
            this.ChckSelectAll.TabIndex = 46;
            this.ChckSelectAll.Text = "All Customer";
            this.ChckSelectAll.UseVisualStyleBackColor = true;
            this.ChckSelectAll.CheckedChanged += new System.EventHandler(this.ChckSelectAll_CheckedChanged);
            // 
            // FrmSendSMS
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(984, 500);
            this.Controls.Add(this.ChckSelectAll);
            this.Controls.Add(this.grProgress);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.BtnSendSMS);
            this.Controls.Add(this.button1);
            this.Name = "FrmSendSMS";
            this.Text = "FrmSendSMS";
            this.Load += new System.EventHandler(this.FrmSendSMS_Load);
            ((System.ComponentModel.ISupportInitialize)(this.DataGridChit)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.grChitSearch.ResumeLayout(false);
            this.grChitSearch.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridChitSearch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridCustomer)).EndInit();
            this.grProgress.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.DataGridView DataGridChit;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button BtnSendSMS;
        private System.Windows.Forms.DateTimePicker DtpSMSDate;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button Btngetchits;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button BtnCustomer;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView DataGridCustomer;
        private System.Windows.Forms.GroupBox grProgress;
        private CircularProgressBar.CircularProgressBar progressBar;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.GroupBox grChitSearch;
        private System.Windows.Forms.TextBox txtChitNameSearch;
        private System.Windows.Forms.DataGridView DataGridChitSearch;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtChitName;
        private System.Windows.Forms.CheckBox ChckSelectAll;
    }
}