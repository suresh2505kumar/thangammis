﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace ThangamMIS
{
    public partial class FrmSendSMS : Form
    {
        public FrmSendSMS()
        {
            InitializeComponent();
        }
        SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnStr"].ConnectionString);
        SqlConnection connPhoto = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnStrPhoto"].ConnectionString);
        SqlCommand cmd;
        int FormId;
        BindingSource bs = new BindingSource();

        private void DataGridChitSearch_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {

        }

        private void LoadTable()
        {
            // Load your Table...
            SetDataSource(DataGridCustomer);
        }

        protected DataTable Getdata(string ChitId)
        {
            DataTable dt = new DataTable();
            try
            {
                SQLDBHelper db = new SQLDBHelper();
                if (Internet.FormID == 1)
                {
                    SqlParameter[] sqlParameters = {
                        new SqlParameter("@ChitId",ChitId)
                    };
                    dt = db.GetDatasWithParameter(CommandType.StoredProcedure, "Proc_GETCHITFORSMS", sqlParameters);
                }
                else
                {
                    SqlParameter[] sqlParameters = {
                        new SqlParameter("@ChitId",ChitId)
                    };
                    dt = db.GetDatasWithParameter(CommandType.StoredProcedure, "Proc_Get_CustomerbyChit", sqlParameters);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            return dt;
        }

        internal delegate void SetDataSourceDelegate(DataGridView table);
        private void SetDataSource(DataGridView datagridcu)
        {
            // Invoke method if required:
            if (this.InvokeRequired)
            {
                this.Invoke(new SetDataSourceDelegate(SetDataSource), datagridcu);
            }
            else
            {
                LoadChitByDid(datagridcu);
                grProgress.Visible = false;
                progressBar.Visible = false;
            }
        }

        protected void LoadChitByDid(DataGridView dt)
        {
            try
            {
                string Mobile = string.Empty;
                string chitName = string.Empty;
                if (dt.Rows.Count > 0)
                {
                    foreach (DataGridViewRow dataGridRow in DataGridCustomer.Rows)
                    {
                        if (dataGridRow.Cells["Chck"].Value != null && (bool)dataGridRow.Cells["Chck"].Value)
                        {
                            chitName = dataGridRow.Cells[1].Value.ToString();
                            if (Mobile == string.Empty)
                            {
                                Mobile = dataGridRow.Cells[4].Value.ToString();
                            }
                            else
                            {
                                Mobile = Mobile + "," + dataGridRow.Cells[4].Value.ToString();
                            }
                        }
                    }
                    string message = "DEAR CUSTOMER NEXT AUCTION DATE FOR YOUR CHIT " + chitName + ", WILL BE " + Convert.ToDateTime(DtpSMSDate.Text).ToString("dd-MMM-yyyy") + ".";
                    WebClient client = new WebClient();
                    string baseurl = "https://voice.lionsms.com/SendSMS/sendmsg.php?uname=tlcfpl&pass=12345678&send=TLCFPL&dest=" + Mobile + "&msg=" + message + "";
                    //string baseurl = "https://voice.lionsms.com/SendSMS/sendmsg.php?uname=tlcfpl&pass=12345678&send=TLCFPL&dest=9095590900,8072569150&msg=" + message + "";
                    Stream data = client.OpenRead(baseurl);
                    StreamReader reader = new StreamReader(data);
                    string s = reader.ReadToEnd();
                    data.Close();
                    reader.Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            finally
            {
                conn.Close();
            }
        }

        private void BtnSendSMS_Click(object sender, EventArgs e)
        {
            try
            {
                grProgress.Visible = true;
                progressBar.Visible = true;
                Thread thread = new Thread(LoadTable);
                thread.Start();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }

        private void txtChitNameSearch_TextChanged(object sender, EventArgs e)
        {
            //bs.Filter = string.Format("ChitName LIKE '%{0}%'", txtChitNameSearch.Text);
        }

        private void Btngetchits_Click(object sender, EventArgs e)
        {
            try
            {
                SQLDBHelper db = new SQLDBHelper();
                DataTable table = new DataTable();
                if (Internet.FormID == 1)
                {
                    SqlParameter[] sqlParameters = { new SqlParameter("@ttype", "Daily") };
                    table = db.GetDatasWithParameter(CommandType.StoredProcedure, "Proc_GetChitNameforSMS", sqlParameters);
                }
                else
                {
                    SqlParameter[] sqlParameters = { new SqlParameter("@ttype", "Monthly") };
                    table = db.GetDatasWithParameter(CommandType.StoredProcedure, "Proc_GetChitNameforSMS", sqlParameters);
                }
                DataGridChit.Rows.Clear();
                for (int i = 0; i < table.Rows.Count; i++)
                {
                    int Index = DataGridChit.Rows.Add();
                    DataGridViewRow dataGridViewRow = DataGridChit.Rows[Index];
                    dataGridViewRow.Cells[1].Value = table.Rows[i]["chitid"].ToString();
                    dataGridViewRow.Cells[2].Value = table.Rows[i]["chitname"].ToString();
                    dataGridViewRow.Cells[3].Value = table.Rows[i]["AuctionNo"].ToString();
                    dataGridViewRow.Cells[4].Value = Convert.ToDateTime(table.Rows[i]["auctiondate"].ToString()).ToString("dd-MMM-yyyy");
                    dataGridViewRow.Cells[5].Value = Convert.ToDateTime(DtpSMSDate.Text);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }

        protected void LoadData()
        {
            DataGridChit.DataSource = null;
            DataGridChit.AutoGenerateColumns = false;
            DataGridChit.ColumnCount = 5;
            DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn = new DataGridViewCheckBoxColumn();
            dataGridViewCheckBoxColumn.HeaderText = "Chck";
            dataGridViewCheckBoxColumn.Width = 50;
            dataGridViewCheckBoxColumn.Name = "Chck";
            DataGridChit.Columns.Insert(0, dataGridViewCheckBoxColumn);

            DataGridChit.Columns[1].Name = "ChitId";
            DataGridChit.Columns[1].HeaderText = "ChitId";
            DataGridChit.Columns[1].Visible = false;

            DataGridChit.Columns[2].Name = "Chit Name";
            DataGridChit.Columns[2].HeaderText = "Chit Name";

            DataGridChit.Columns[3].Name = "Auction No";
            DataGridChit.Columns[3].HeaderText = "Auction No";

            DataGridChit.Columns[4].Name = "Last Auction Date";
            DataGridChit.Columns[4].HeaderText = "Last Auction Date";
            DataGridChit.Columns[4].Width = 130;
            DataGridChit.Columns[4].DefaultCellStyle.Format = "dd-MMM-yyyy";

            DataGridChit.Columns[5].Name = "Next Auction Date";
            DataGridChit.Columns[5].HeaderText = "Next Auction Date";
            DataGridChit.Columns[5].Width = 130;
            DataGridChit.Columns[5].Visible = false;
        }

        private void FrmSendSMS_Load(object sender, EventArgs e)
        {
            LoadData();
            LoadDatatable();
        }

        private void Button2_Click(object sender, EventArgs e)
        {
            try
            {
                string Chitid = string.Empty;
                if (txtChitName.Text != string.Empty)
                {
                    Chitid = txtChitName.Tag.ToString();
                }
                foreach (DataGridViewRow dataGridRow in DataGridChit.Rows)
                {
                    if (dataGridRow.Cells["Chck"].Value != null && (bool)dataGridRow.Cells["Chck"].Value)
                    {
                        Chitid = dataGridRow.Cells[1].Value.ToString();
                    }
                }
                DataTable data = Getdata(Chitid);
                DataGridCustomer.Rows.Clear();
                for (int i = 0; i < data.Rows.Count; i++)
                {
                    int Index = DataGridCustomer.Rows.Add();
                    DataGridViewRow dataGridViewRow = DataGridCustomer.Rows[Index];
                    dataGridViewRow.Cells[1].Value = data.Rows[i]["ChitName"].ToString();
                    dataGridViewRow.Cells[2].Value = data.Rows[i]["CUSTNAME"].ToString();
                    dataGridViewRow.Cells[3].Value = data.Rows[i]["SLNO"].ToString();
                    dataGridViewRow.Cells[4].Value = data.Rows[i]["mobile"].ToString();
                    dataGridViewRow.Cells[5].Value = data.Rows[i]["DUE"].ToString();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }
        protected void LoadDatatable()
        {
            try
            {
                DataGridCustomer.DataSource = null;
                DataGridCustomer.AutoGenerateColumns = false;
                DataGridCustomer.ColumnCount = 5;

                DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn = new DataGridViewCheckBoxColumn();
                dataGridViewCheckBoxColumn.HeaderText = "Chck";
                dataGridViewCheckBoxColumn.Width = 50;
                dataGridViewCheckBoxColumn.Name = "Chck";
                DataGridCustomer.Columns.Insert(0, dataGridViewCheckBoxColumn);

                DataGridCustomer.Columns[1].Name = "ChitName";
                DataGridCustomer.Columns[1].HeaderText = "ChitName";

                DataGridCustomer.Columns[2].Name = "CustName";
                DataGridCustomer.Columns[2].HeaderText = "CustName";

                DataGridCustomer.Columns[3].Name = "SlNo";
                DataGridCustomer.Columns[3].HeaderText = "SlNo";
                DataGridCustomer.Columns[3].Width = 50;

                DataGridCustomer.Columns[4].Name = "Mobile";
                DataGridCustomer.Columns[4].HeaderText = "Mobile";

                DataGridCustomer.Columns[5].Name = "Due Amount";
                DataGridCustomer.Columns[5].HeaderText = "Due Amount";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }

        private void Button2_Click_1(object sender, EventArgs e)
        {
            string chitName = string.Empty;
            if (DataGridCustomer.Rows.Count > 0)
            {
                for (int i = 0; i < DataGridCustomer.Rows.Count; i++)
                {
                    chitName = DataGridCustomer.Rows[i].Cells[1].Value.ToString();
                }
            }
            string message = "DEAR CUSTOMER NEXT AUCTION DATE FOR YOUR CHIT " + chitName + ", WILL BE " + Convert.ToDateTime(DtpSMSDate.Text).ToString("dd-MMM-yyyy") + ".";
            WebClient client = new WebClient();
            string baseurl = "https://voice.lionsms.com/SendSMS/sendmsg.php?uname=tlcfpl&pass=12345678&send=TLCFPL&dest=9843050500,9946184896&msg=" + message + "";
            Stream data = client.OpenRead(baseurl);
            StreamReader reader = new StreamReader(data);
            string s = reader.ReadToEnd();
            data.Close();
            reader.Close();
            MessageBox.Show("Message Send Successfully", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void txtChitName_Click(object sender, EventArgs e)
        {
            try
            {
                txtChitNameSearch.Text = string.Empty;
                grChitSearch.Visible = true;
                LoadChit();
                txtChitNameSearch.Focus();
            }
            catch (Exception)
            {

                throw;
            }
        }
        protected void LoadChit()
        {
            try
            {
                if (txtChitName.Text == string.Empty)
                {
                    conn.Open();
                    cmd = new SqlCommand
                    {
                        Connection = conn,
                        CommandType = CommandType.StoredProcedure
                    };
                    if (Internet.FormID == 1)
                    {
                      cmd.CommandText = "SP_GetCurrentChit";
                    }
                    else
                    {
                      cmd.CommandText = "SP_GetCurrentChitMonthly";
                    }

                    if (txtChitNameSearch.Text != string.Empty)
                    {
                        cmd.Parameters.AddWithValue("@Tag", 1);
                        cmd.Parameters.AddWithValue("@ChitName", txtChitNameSearch.Text);
                    }
                    else

                    {
                        cmd.Parameters.AddWithValue("@Tag", 0);
                        cmd.Parameters.AddWithValue("@ChitName", txtChitNameSearch.Text);
                    }

                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    DataTable dt = new DataTable();
                    da.Fill(dt);
                    bs.DataSource = dt;
                    DataGridChitSearch.DataSource = null;
                    DataGridChitSearch.AutoGenerateColumns = false;
                    DataGridChitSearch.ColumnCount = 3;

                    DataGridChitSearch.Columns[0].Name = "ChitName";
                    DataGridChitSearch.Columns[0].HeaderText = "ChitName";
                    DataGridChitSearch.Columns[0].DataPropertyName = "ChitName";
                    DataGridChitSearch.Columns[0].Width = 290;
                    DataGridChitSearch.Columns[1].Name = "ChitID";
                    DataGridChitSearch.Columns[1].HeaderText = "ChitID";
                    DataGridChitSearch.Columns[1].DataPropertyName = "ChitID";
                    DataGridChitSearch.Columns[1].Visible = false;
                    DataGridChitSearch.Columns[2].Name = "TenureType";
                    DataGridChitSearch.Columns[2].HeaderText = "TenureType";
                    DataGridChitSearch.Columns[2].DataPropertyName = "TenureType";
                    DataGridChitSearch.Columns[2].Visible = false;
                    DataGridChitSearch.DataSource = bs;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            finally
            {
                conn.Close();
                cmd.Dispose();
            }
        }

        private void TxtChitNameSearch_TextChanged_1(object sender, EventArgs e)
        {
            bs.Filter = string.Format("ChitName LIKE '%{0}%'", txtChitNameSearch.Text);
        }

        private void DataGridChitSearch_CellMouseDoubleClick_1(object sender, DataGridViewCellMouseEventArgs e)
        {
            int index = e.RowIndex;
            txtChitName.Text = DataGridChitSearch.Rows[index].Cells[0].Value.ToString();
            txtChitName.Tag = (int)DataGridChitSearch.Rows[index].Cells[1].Value;
            grChitSearch.Visible = false;
            txtChitNameSearch.Text = string.Empty;
            Button2_Click(sender, e);
        }

        private void ChckSelectAll_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if(ChckSelectAll.Checked == true)
                {
                    for (int i = 0; i < DataGridCustomer.RowCount; i++)
                    {
                        DataGridCustomer[0, i].Value = true;
                    }
                }
                else
                {
                    for (int i = 0; i < DataGridCustomer.RowCount; i++)
                    {
                        DataGridCustomer[0, i].Value = false;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }
    }
}
