﻿using System;
using System.Data;
using System.Windows.Forms;
using System.Configuration;
using System.Data.SqlClient;

namespace ThangamMIS
{
    public partial class FrmLogin : Form
    {
        public FrmLogin()
        {
            InitializeComponent();
            
        }
        public static string UserName;
        public SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnStr"].ConnectionString);
        private void FrmLogin_Load(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void BtnLogin_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtUserName.Text != string.Empty || txtPassword.Text != string.Empty)
                {
                    conn.Open();
                    string Query = "SELECT * FROM UserMaster Where UserName = '" + txtUserName.Text + "' and Password = '" + txtPassword.Text + "'";
                    SqlCommand cmd = new SqlCommand(Query, conn);
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    DataTable dt = new DataTable();
                    da.Fill(dt);
                    if (dt.Rows.Count != 0)
                    {
                        this.Hide();
                        UserName = txtUserName.Text;
                        FrmMain main = new FrmMain();
                        main.Show();
                    }
                }
                else
                {
                    MessageBox.Show("Username and password do not match");
                    txtUserName.Focus();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtUserName.Focus();
                return;
            }
            finally
            {
                conn.Close();
            }
        }
        private void BtnExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void chckUsername_CheckedChanged(object sender, EventArgs e)
        {
            if(chckUsername.Checked == true)
            {
                txtUserName.Text = "bijoy";
                txtPassword.Text = "bijoy";
            }
            else
            {
                txtUserName.Text = string.Empty;
                txtPassword.Text = string.Empty;
            }
        }
    }
}
