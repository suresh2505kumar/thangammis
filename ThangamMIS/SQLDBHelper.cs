﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace ThangamMIS
{
    public class SQLDBHelper
    {
        SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnStr"].ConnectionString);
        public DataSet GetDatasWithPara(CommandType cmdType,string CommandText,SqlParameter[] para)
        {
            DataSet ds = new DataSet();
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = cmdType;
                cmd.CommandText = CommandText;
                cmd.Parameters.AddRange(para);
                cmd.CommandTimeout = 120;
                cmd.Connection = conn;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            catch(Exception ex)
            {
                throw ex;
            }
            return ds;
        }
        public DataSet GetDatasWithPara(CommandType cmdType, string CommandText)
        {
            DataSet ds = new DataSet();
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = cmdType;
                cmd.CommandText = CommandText;                
                cmd.Connection = conn;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ds;
        }
        public int ExecuteQuery(CommandType cmdType, string CommandText, SqlParameter[] para)
        {
            int ds = 0;
            try
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = cmdType;
                cmd.CommandText = CommandText;
                cmd.Parameters.AddRange(para);
                cmd.CommandTimeout = 1800;
                cmd.Connection = conn;
                ds = cmd.ExecuteNonQuery();
                conn.Close();
            }
            catch (Exception ex)
            {
                conn.Close();
                throw ex;
            }
            return ds;
        }

        public DataTable GetDatasWithParameter(CommandType cmdType, string CommandText,SqlParameter[] sqlParameters)
        {
            DataTable ds = new DataTable();
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = cmdType;
                cmd.CommandText = CommandText;
                cmd.Parameters.AddRange(sqlParameters);
                cmd.CommandTimeout = 120;
                cmd.Connection = conn;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ds;
        }
    }
}
