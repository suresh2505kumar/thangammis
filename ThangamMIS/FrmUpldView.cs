﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Threading;
using System.Windows.Forms;

namespace ThangamMIS
{
    public partial class FrmUpldView : Form
    {
        public FrmUpldView()
        {
            InitializeComponent();
        }
        SqlConnection ConnWeb = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnstrWeb"].ConnectionString);
        private void btnShowDetails_Click(object sender, EventArgs e)
        {
            grProgress.Visible = true;
            progressBar.Visible = true;
            Thread thread = new Thread(LoadTable);
            thread.Start();
        }
        protected void LoadTable()
        {
            DataTable dt = GetDatafromWebsrver();
            SetDataToGrid(dt);
        }
        internal delegate void setDataSourceDelegate(DataTable table);
        private void SetDataToGrid(DataTable table)
        {
            if (this.InvokeRequired)
            {
                this.Invoke(new setDataSourceDelegate(SetDataToGrid), table);
            }
            else
            {
                FillDataGrid(table);
                grProgress.Visible = false;
                progressBar.Visible = false;
            }
        }

        private void FillDataGrid(DataTable table)
        {
            DataGridUpldRecords.DataSource = null;
            DataGridUpldRecords.AutoGenerateColumns = false;
            DataGridUpldRecords.ColumnCount = 4;
            DataGridUpldRecords.Columns[0].Name = "AgentId";
            DataGridUpldRecords.Columns[0].HeaderText = "AgentId";
            DataGridUpldRecords.Columns[0].DataPropertyName = "AgentId";
            DataGridUpldRecords.Columns[0].Visible = false;
            DataGridUpldRecords.Columns[1].Name = "AgentCode";
            DataGridUpldRecords.Columns[1].HeaderText = "AgentCode";
            DataGridUpldRecords.Columns[1].DataPropertyName = "AgentCode";
            DataGridUpldRecords.Columns[1].Width = 100;
            DataGridUpldRecords.Columns[2].Name = "AgentName";
            DataGridUpldRecords.Columns[2].HeaderText = "AgentName";
            DataGridUpldRecords.Columns[2].DataPropertyName = "AgentName";
            DataGridUpldRecords.Columns[2].Width =220;
            DataGridUpldRecords.Columns[3].Name = "No of Records";
            DataGridUpldRecords.Columns[3].HeaderText = "No of Records";
            DataGridUpldRecords.Columns[3].DataPropertyName = "NoofRecords";
            DataGridUpldRecords.Columns[3].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;            
            DataGridUpldRecords.DataSource = table;
        }

        private DataTable GetDatafromWebsrver()
        {
            DataTable dt = new DataTable();
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "SP_getUploadDetails";
                cmd.Connection = ConnWeb;
                cmd.Parameters.AddWithValue("@Date", Convert.ToDateTime(DtpCollDate.Text));
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                
            }
            return dt;            
        }

        private void FrmUpldView_Load(object sender, EventArgs e)
        {

        }
    }
}
