﻿namespace ThangamMIS
{
    partial class FrmUpldView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.grProgress = new System.Windows.Forms.GroupBox();
            this.progressBar = new CircularProgressBar.CircularProgressBar();
            this.label1 = new System.Windows.Forms.Label();
            this.DtpCollDate = new System.Windows.Forms.DateTimePicker();
            this.DataGridUpldRecords = new System.Windows.Forms.DataGridView();
            this.btnShowDetails = new System.Windows.Forms.Button();
            this.grProgress.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridUpldRecords)).BeginInit();
            this.SuspendLayout();
            // 
            // grProgress
            // 
            this.grProgress.BackColor = System.Drawing.Color.White;
            this.grProgress.Controls.Add(this.progressBar);
            this.grProgress.Location = new System.Drawing.Point(119, 112);
            this.grProgress.Name = "grProgress";
            this.grProgress.Size = new System.Drawing.Size(191, 198);
            this.grProgress.TabIndex = 34;
            this.grProgress.TabStop = false;
            this.grProgress.Visible = false;
            // 
            // progressBar
            // 
            this.progressBar.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.progressBar.AnimationFunction = WinFormAnimation.KnownAnimationFunctions.Liner;
            this.progressBar.AnimationSpeed = 500;
            this.progressBar.BackColor = System.Drawing.Color.White;
            this.progressBar.Font = new System.Drawing.Font("Arial", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.progressBar.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.progressBar.InnerColor = System.Drawing.Color.White;
            this.progressBar.InnerMargin = 5;
            this.progressBar.InnerWidth = 5;
            this.progressBar.Location = new System.Drawing.Point(6, 12);
            this.progressBar.MarqueeAnimationSpeed = 2000;
            this.progressBar.Name = "progressBar";
            this.progressBar.OuterColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(73)))), ((int)(((byte)(94)))));
            this.progressBar.OuterMargin = -11;
            this.progressBar.OuterWidth = 10;
            this.progressBar.ProgressColor = System.Drawing.Color.FromArgb(((int)(((byte)(194)))), ((int)(((byte)(251)))), ((int)(((byte)(50)))));
            this.progressBar.ProgressWidth = 10;
            this.progressBar.SecondaryFont = new System.Drawing.Font("Microsoft Sans Serif", 4.125F);
            this.progressBar.Size = new System.Drawing.Size(180, 180);
            this.progressBar.StartAngle = 270;
            this.progressBar.Style = System.Windows.Forms.ProgressBarStyle.Marquee;
            this.progressBar.SubscriptColor = System.Drawing.Color.Silver;
            this.progressBar.SubscriptMargin = new System.Windows.Forms.Padding(0);
            this.progressBar.SubscriptText = "";
            this.progressBar.SuperscriptColor = System.Drawing.Color.Silver;
            this.progressBar.SuperscriptMargin = new System.Windows.Forms.Padding(0);
            this.progressBar.SuperscriptText = "";
            this.progressBar.TabIndex = 10;
            this.progressBar.Text = "Please Wait";
            this.progressBar.TextMargin = new System.Windows.Forms.Padding(0, 2, 0, 0);
            this.progressBar.Value = 80;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(28, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(37, 18);
            this.label1.TabIndex = 32;
            this.label1.Text = "Date";
            // 
            // DtpCollDate
            // 
            this.DtpCollDate.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DtpCollDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.DtpCollDate.Location = new System.Drawing.Point(85, 17);
            this.DtpCollDate.Name = "DtpCollDate";
            this.DtpCollDate.Size = new System.Drawing.Size(107, 26);
            this.DtpCollDate.TabIndex = 31;
            // 
            // DataGridUpldRecords
            // 
            this.DataGridUpldRecords.AllowUserToAddRows = false;
            this.DataGridUpldRecords.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DataGridUpldRecords.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.DataGridUpldRecords.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridUpldRecords.Location = new System.Drawing.Point(12, 50);
            this.DataGridUpldRecords.Name = "DataGridUpldRecords";
            this.DataGridUpldRecords.ReadOnly = true;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DataGridUpldRecords.RowHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.DataGridUpldRecords.RowHeadersVisible = false;
            this.DataGridUpldRecords.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DataGridUpldRecords.Size = new System.Drawing.Size(424, 370);
            this.DataGridUpldRecords.TabIndex = 33;
            // 
            // btnShowDetails
            // 
            this.btnShowDetails.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnShowDetails.Location = new System.Drawing.Point(316, 12);
            this.btnShowDetails.Name = "btnShowDetails";
            this.btnShowDetails.Size = new System.Drawing.Size(120, 27);
            this.btnShowDetails.TabIndex = 35;
            this.btnShowDetails.Text = "Show Details";
            this.btnShowDetails.UseVisualStyleBackColor = true;
            this.btnShowDetails.Click += new System.EventHandler(this.btnShowDetails_Click);
            // 
            // FrmUpldView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(451, 426);
            this.Controls.Add(this.btnShowDetails);
            this.Controls.Add(this.grProgress);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.DtpCollDate);
            this.Controls.Add(this.DataGridUpldRecords);
            this.MaximizeBox = false;
            this.Name = "FrmUpldView";
            this.Text = "Upload DataView";
            this.Load += new System.EventHandler(this.FrmUpldView_Load);
            this.grProgress.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DataGridUpldRecords)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox grProgress;
        private CircularProgressBar.CircularProgressBar progressBar;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker DtpCollDate;
        private System.Windows.Forms.DataGridView DataGridUpldRecords;
        private System.Windows.Forms.Button btnShowDetails;
    }
}